//CipherStreamJava/src/main/java/com/mattrixwv/cipherstream/monosubstitution/OneTimePad.java
//Mattrixwv
// Created: 02-23-22
//Modified: 08-11-24
/*
	Copyright (C) 2024  Mattrixwv

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.cipherstream.monosubstitution;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;


/**
 * A class for encoding and decoding strings using the One-Time Pad cipher,
 * which is a special case of the Vigenère cipher where the key is as long as
 * the input message and used only once.
 *
 * <p>
 * The One-Time Pad cipher provides perfect secrecy when the key is truly random,
 * as long as the key length is equal to or greater than the length of the message
 * and the key is used only once.
 * </p>
 */
public class OneTimePad extends Vigenere{
	private static final Logger logger = LoggerFactory.getLogger(OneTimePad.class);

	//?Add some kind of entropy calculator?


	//?Constructor
	/**
	 * Constructs a new {@code OneTimePad} instance with default settings.
	 */
	public OneTimePad(){
		super();
	}
	/**
	 * Constructs a new {@code OneTimePad} instance with specified settings.
	 *
	 * @param preserveCapitals   whether to preserve capitalization in the output
	 * @param preserveWhitespace whether to preserve whitespace in the output
	 * @param preserveSymbols    whether to preserve symbols in the output
	 */
	public OneTimePad(boolean preserveCapitals, boolean preserveWhitespace, boolean preserveSymbols){
		super(preserveCapitals, preserveWhitespace, preserveSymbols);
	}

	/**
	 * Encodes the input string using the One-Time Pad cipher with the provided key.
	 * The key must be at least as long as the input string.
	 *
	 * @param keyword   the key to use for encoding
	 * @param inputString the string to be encoded
	 * @return the encoded string
	 * @throws InvalidKeywordException if the key is shorter than the input string
	 * @throws InvalidInputException   if the input string is invalid
	 */
	@Override
	public String encode(String keyword, String inputString) throws InvalidKeywordException, InvalidInputException{
		if(keyword.length() < inputString.length()){
			throw new InvalidKeywordException("Key must be at least as long as the input");
		}

		logger.debug("Encoding");

		return super.encode(keyword, inputString);
	}
	/**
	 * Decodes the input string using the One-Time Pad cipher with the provided key.
	 * The key must be at least as long as the input string.
	 *
	 * @param keyword   the key to use for decoding
	 * @param inputString the string to be decoded
	 * @return the decoded string
	 * @throws InvalidKeywordException if the key is shorter than the input string
	 * @throws InvalidInputException   if the input string is invalid
	 */
	@Override
	public String decode(String keyword, String inputString) throws InvalidKeywordException, InvalidInputException{
		if(keyword.length() < inputString.length()){
			throw new InvalidKeywordException("Key must be at least as long as the input");
		}

		logger.debug("Decoding");

		return super.decode(keyword, inputString);
	}
}

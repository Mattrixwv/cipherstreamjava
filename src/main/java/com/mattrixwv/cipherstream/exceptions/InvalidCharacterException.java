//CipherStreamJava/src/main/java/com/mattrixwv/CipherStreamJava/Exceptions/InvalidCharacterException.java
//Mattrixwv
// Created: 01-04-22
//Modified: 08-11-24
/*
	Copyright (C) 2024  Mattrixwv

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.cipherstream.exceptions;


/**
 * Thrown to indicate that a problem has occurred related to an invalid character.
 */
public class InvalidCharacterException extends RuntimeException{
	/**
	 * Constructs a new {@code InvalidCharacterException} with {@code null} as its detail message.
	 */
	public InvalidCharacterException(){
		super();
	}
	/**
	 * Constructs a new {@code InvalidCharacterException} with the specified detail message.
	 *
	 * @param message the detail message, which is saved for later retrieval by the {@link #getMessage()} method
	 */
	public InvalidCharacterException(String message){
		super(message);
	}
	/**
	 * Constructs a new {@code InvalidCharacterException} with the specified cause.
	 *
	 * @param error the cause, which is saved for later retrieval by the {@link #getCause()} method
	 */
	public InvalidCharacterException(Throwable error){
		super(error);
	}
	/**
	 * Constructs a new {@code InvalidCharacterException} with the specified detail message and cause.
	 *
	 * @param message the detail message, which is saved for later retrieval by the {@link #getMessage()} method
	 * @param error the cause, which is saved for later retrieval by the {@link #getCause()} method
	 */
	public InvalidCharacterException(String message, Throwable error){
		super(message, error);
	}
}

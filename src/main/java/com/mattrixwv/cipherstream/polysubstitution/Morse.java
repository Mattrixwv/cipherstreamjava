//CipherStreamJava/src/main/java/com/mattrixwv/CipherStreamJava/Morse.java
//Mattrixwv
// Created: 07-28-21
//Modified: 08-11-24
/*
	Copyright (C) 2024  Mattrixwv

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.cipherstream.polysubstitution;


import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mattrixwv.cipherstream.exceptions.InvalidInputException;


/**
 * A class to handle encoding and decoding of Morse code.
 * <p>
 * This class provides methods to encode a string into Morse code and decode Morse code back into a string.
 * It supports alphanumeric characters (A-Z, 0-9) and validates input to ensure it contains valid characters.
 * </p>
 */
public class Morse{
	private static final Logger logger = LoggerFactory.getLogger(Morse.class);
	//?Code representations
	/** Morse code representations for letters A-Z */
	private static final String[] letters = {
		".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..",	//A-L
		"--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."	//M-Z
	};
	/** Morse code representations for digits 0-9 */
	private static final String[] numbers = {
		"-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----."	//0-9
	};
	/** The map to help convert from Morse to letters and numbers */
	private static final Map<String, Character> map;
	/** Initialize the Morse Code map */
	static{
		//Setup the map
		map = new HashMap<>();
		for(int cnt = 0;cnt < letters.length;++cnt){
			String letter = letters[cnt];
			map.put(letter, (char)('A' + cnt));
		}
		for(int cnt = 0;cnt < numbers.length;++cnt){
			String number = numbers[cnt];
			map.put(number, (char)('0' + cnt));
		}
	}
	//?Fields
	/** The string that needs encoded/decoded */
	protected String inputString;
	/** The encoded/decoded message */
	protected String outputString;


	/**
	 * Validates and prepares the input string for encoding.
	 * <p>
	 * This method converts the input string to uppercase, removes all non-alphanumeric characters, and stores the cleaned string.
	 * </p>
	 *
	 * @param inputString the string to be encoded
	 * @throws InvalidInputException if the input string is null or contains invalid characters
	 */
	protected void setInputStringEncode(String inputString){
		logger.debug("Setting input string for encoding '{}'", inputString);

		//Check for null
		if(inputString == null){
			throw new InvalidInputException("Input cannot be null");
		}

		//Convert all letters to uppercase
		logger.debug("Removing case");
		inputString = inputString.toUpperCase();

		//Remove all except alpha-numeric characters
		logger.debug("Removing whitespace and symbols");
		inputString = inputString.replaceAll("[^A-Z0-9]", "");

		logger.debug("Cleaned input string '{}'", inputString);

		//Check for a blank input
		if(inputString.isBlank()){
			throw new InvalidInputException("Input must contain at least 1 letter");
		}

		//Save the input
		this.inputString = inputString;
	}
	/**
	 * Validates and prepares the input string for decoding.
	 * <p>
	 * This method ensures that the input string contains only valid Morse code characters (dots, dashes, and spaces).
	 * </p>
	 *
	 * @param inputString the Morse code string to be decoded
	 * @throws InvalidInputException if the input string is null or contains invalid Morse code characters
	 */
	protected void setInputStringDecode(String inputString){
		logger.debug("Setting input string for decoding '{}'", inputString);

		//Check for null
		if(inputString == null){
			throw new InvalidInputException("Input cannot be null");
		}

		//Remove all non-morse code characters and check if there is a difference
		logger.debug("Checking for invalid characters");
		if(!inputString.equals(inputString.replaceAll("[^ \\.\\-]", ""))){
			throw new InvalidInputException("Invalid Morse characters found");
		}

		//Check for blank input
		if(inputString.isBlank()){
			throw new InvalidInputException("Input must contain at least 1 letter");
		}

		//Save the input
		logger.debug("Saving");
		this.inputString = inputString;
	}
	/**
	 * Encodes the {@code inputString} into Morse code.
	 * <p>
	 * This method converts each alphanumeric character in the input string to its Morse code representation and stores the result.
	 * </p>
	 *
	 * @throws InvalidInputException if the input string contains characters that cannot be encoded
	 */
	protected void encode(){
		logger.debug("Encoding");

		StringJoiner output = new StringJoiner(" ");
		//Loop through every element in the input string and encode it
		for(char letter : inputString.toCharArray()){
			logger.debug("Working character {}", letter);
			//If the character is a letter or a number get the correct code and add it to the output
			if(Character.isUpperCase(letter)){
				logger.debug("Appending letter");

				output.add(letters[letter - 'A']);
			}
			else if(Character.isDigit(letter)){
				logger.debug("Appending number");

				output.add(numbers[letter - '0']);
			}
			//If the character is not a letter or number throw an exception because it cannot be encoded
			else{
				throw new InvalidInputException("Invalid characters found in input");
			}
		}

		//Save the output
		this.outputString = output.toString();
		logger.debug("Saving encoded string '{}'", outputString);
	}
	/**
	 * Decodes the {@code inputString} from Morse code to plain text.
	 * <p>
	 * This method converts each Morse code sequence in the input string back to its corresponding alphanumeric character.
	 * </p>
	 *
	 * @throws InvalidInputException if the input string contains invalid Morse code sequences
	 */
	protected void decode(){
		logger.debug("Decoding");

		StringBuilder output = new StringBuilder();
		//Loop through every element in the input string and encode it
		for(String current : inputString.split(" ")){
			logger.debug("Working letter {}", current);

			//Get the current letter from the map and append it to the output
			if(map.containsKey(current)){
				char letter = map.get(current);
				logger.debug("Decoded letter {}", letter);
				output.append(letter);
			}
			else{
				throw new InvalidInputException("Invalid characters found in input");
			}
		}

		//Save the output
		this.outputString = output.toString();
		logger.debug("Saving decoded string '{}'", outputString);
	}


	//?Constructor
	/**
	 * Resets the class by clearing the input and output strings.
	 */
	public Morse(){
		reset();
	}


	/**
	 * Encodes the given input string into Morse code.
	 * <p>
	 * This method validates the input string, encodes it, and returns the Morse code representation.
	 * </p>
	 *
	 * @param inputString the string to be encoded
	 * @return the Morse code representation of the input string
	 * @throws InvalidInputException if the input string contains invalid characters
	 */
	public String encode(String inputString){
		setInputStringEncode(inputString);
		encode();
		return outputString;
	}
	/**
	 * Decodes the given Morse code string into plain text.
	 * <p>
	 * This method validates the Morse code input, decodes it, and returns the plain text representation.
	 * </p>
	 *
	 * @param inputString the Morse code string to be decoded
	 * @return the decoded plain text representation
	 * @throws InvalidInputException if the input string contains invalid Morse code sequences
	 */
	public String decode(String inputString){
		setInputStringDecode(inputString);
		decode();
		return outputString;
	}


	//?Getters
	/**
	 * Returns the current input string.
	 *
	 * @return the current input string
	 */
	public String getInputString(){
		return inputString;
	}
	/**
	 * Returns the current output string.
	 *
	 * @return the current output string
	 */
	public String getOutputString(){
		return outputString;
	}
	/**
	 * Resets the internal fields to their default values.
	 */
	public void reset(){
		logger.debug("Resetting");

		inputString = "";
		outputString = "";
	}
}

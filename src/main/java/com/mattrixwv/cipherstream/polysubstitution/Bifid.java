//CipherStreamJava/src/main/java/com/mattrixwv/cipherstream/polysubstitution/Bifid.java
//Mattrixwv
// Created: 03-03-22
//Modified: 08-11-24
/*
	Copyright (C) 2024  Mattrixwv

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.cipherstream.polysubstitution;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mattrixwv.cipherstream.exceptions.InvalidCharacterException;
import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;


/**
 * A class for encoding and decoding strings using the Bifid cipher.
 * The Bifid cipher is a polygraphic substitution cipher that uses a Polybius square
 * and applies a columnar transposition to encode or decode messages.
 *
 * <p>
 * The Bifid cipher operates by converting the plaintext message into coordinates based on a
 * Polybius square, then rearranges these coordinates using a transposition before converting
 * them back into text.
 * </p>
 */
public class Bifid{
	private static final Logger logger = LoggerFactory.getLogger(Bifid.class);
	//?Fields
	/** The message that needs to be encoded/decoded */
	protected String inputString;
	/** The encoded/decoded message */
	protected String outputString;
	/** The keyword used to create the grid */
	protected String keyword;
	//?Settings
	/** Persist capitals in the output string */
	protected boolean preserveCapitals;
	/** Persist whitespace in the output string */
	protected boolean preserveWhitespace;
	/** Persist symbols in the output string */
	protected boolean preserveSymbols;
	//?Internal ciphers
	/** Used to encode the message to numbers then back into letters */
	protected PolybiusSquare polybiusSquare;


	/**
	 * Sets the keyword for creating the Polybius square.
	 *
	 * @param keyword the keyword to use for the Polybius square
	 * @throws InvalidKeywordException if the keyword is null
	 */
	protected void setKeyword(String keyword) throws InvalidKeywordException{
		//Ensure the keyword isn't null
		if(keyword == null){
			throw new InvalidKeywordException("Keyword cannot be null");
		}

		logger.debug("Setting keyword '{}'", keyword);

		//Save the key for polybius to deal with
		this.keyword = keyword;
	}
	/**
	 * Validates and preprocesses the input string based on the current settings.
	 *
	 * @param inputString the string to be encoded/decoded
	 * @throws InvalidInputException if the input string is null or blank
	 */
	protected void setInputString(String inputString) throws InvalidInputException{
		//Ensure the input string isn't null
		if(inputString == null){
			throw new InvalidInputException("Input cannot be null");
		}

		logger.debug("Original input string '{}'", inputString);

		//Apply removal options
		if(!preserveCapitals){
			logger.debug("Removing case");

			inputString = inputString.toUpperCase();
		}
		if(!preserveWhitespace){
			logger.debug("Removing whitespace");

			inputString = inputString.replaceAll("\\s", "");
		}
		if(!preserveSymbols){
			logger.debug("Removing symbols");

			inputString = inputString.replaceAll("[^a-zA-Z\\s]", "");
		}

		//Save the string
		logger.debug("Cleaned input string '{}'", inputString);
		this.inputString = inputString;

		//Ensure the string isn't blank
		if(this.inputString.isBlank()){
			throw new InvalidInputException("Input must contain at least 1 letter");
		}
	}
	/**
	 * Formats the encoded/decoded output to match the input string's original format.
	 *
	 * @param outputString the encoded/decoded message to be formatted
	 */
	protected void formatOutput(String outputString){
		logger.debug("Formatting output");
		//Keep track of where you are in the output
		int outputCnt = 0;
		StringBuilder output = new StringBuilder();
		//Check every character in the input and apply the correct rules to the output
		for(char ch : inputString.toCharArray()){
			logger.debug("Current character {}", ch);
			if(Character.isUpperCase(ch)){
				logger.debug("Altering uppercase");

				output.append(Character.toUpperCase(outputString.charAt(outputCnt++)));
			}
			else if(Character.isLowerCase(ch)){
				logger.debug("Altering lowercase");

				output.append(Character.toLowerCase(outputString.charAt(outputCnt++)));
			}
			else{
				logger.debug("Adding symbol");

				output.append(ch);
			}
		}

		//Save the output
		this.outputString = output.toString();
		logger.debug("Formatted output string '{}'", this.outputString);
	}
	/**
	 * Encodes the input string using the Bifid cipher and stores the result in outputString.
	 *
	 * @throws InvalidCharacterException if there are invalid characters in the input
	 * @throws InvalidInputException     if the input string is invalid
	 */
	protected void encode() throws InvalidCharacterException, InvalidInputException{
		logger.debug("Encoding");

		//Get the encoded numbers from a polybius square
		logger.debug("Encoding Polybius");
		String polybiusMessage = polybiusSquare.encode(keyword, inputString).replaceAll("\\s", "");
		keyword = polybiusSquare.getKeyword();	//Save the cleaned keyword

		//Split the numbers into 2 rows and rejoin the rows to create a new string
		StringBuilder row0 = new StringBuilder();
		StringBuilder row1 = new StringBuilder();
		boolean firstNum = true;
		logger.debug("Splitting Polybius Square message");
		for(char ch : polybiusMessage.toCharArray()){
			logger.debug("Current character '{}'", ch);

			if(firstNum){
				row0.append(ch);
			}
			else{
				row1.append(ch);
			}

			firstNum = !firstNum;
		}

		String shuffledResult = row0.toString() + row1.toString();

		//Take the new string and decode the numbers using polybius
		logger.debug("Decoding Polybius Square");
		String letterResult = polybiusSquare.decode(keyword, shuffledResult);

		//Format the output
		formatOutput(letterResult);
	}
	/**
	 * Decodes the input string using the Bifid cipher and stores the result in outputString.
	 *
	 * @throws InvalidCharacterException if there are invalid characters in the input
	 * @throws InvalidInputException     if the input string is invalid
	 */
	protected void decode() throws InvalidCharacterException, InvalidInputException{
		logger.debug("Decoding");

		//Get the decoded number from a polybius square
		logger.debug("Encoding Polybius Square");
		String numberResult = polybiusSquare.encode(keyword, inputString).replaceAll("\\s", "");
		keyword = polybiusSquare.getKeyword();

		//Split the numbers into to rows and rejoin the rows to create a new string
		String row0 = numberResult.substring(0, numberResult.length() / 2);
		String row1 = numberResult.substring(numberResult.length() / 2);
		StringBuilder unshuffledResult = new StringBuilder();
		logger.debug("Splitting Polybius Square message");
		for(int cnt = 0;cnt < row0.length();++cnt){
			logger.debug("Current characters {} {}", row0.charAt(cnt), row1.charAt(cnt));

			unshuffledResult.append(row0.charAt(cnt));
			unshuffledResult.append(row1.charAt(cnt));
		}

		//Take the new string and decode the numbers using polybius
		logger.debug("Decoding Polybius Square");
		String letterResult = polybiusSquare.decode(keyword, unshuffledResult.toString());

		//Format the outputString
		formatOutput(letterResult);
	}


	//?Constructors
	/**
	 * Constructs a new {@code Bifid} instance with default settings.
	 *
	 * @throws InvalidCharacterException if initialization of PolybiusSquare fails
	 */
	public Bifid() throws InvalidCharacterException{
		preserveCapitals = false;
		preserveWhitespace = false;
		preserveSymbols = false;
		polybiusSquare = new PolybiusSquare(false, false);
		reset();
	}
	/**
	 * Constructs a new {@code Bifid} instance with specified settings.
	 *
	 * @param preserveCapitals   whether to preserve capitalization in the output
	 * @param preserveWhitespace whether to preserve whitespace in the output
	 * @param preserveSymbols    whether to preserve symbols in the output
	 * @throws InvalidCharacterException if initialization of PolybiusSquare fails
	 */
	public Bifid(boolean preserveCapitals, boolean preserveWhitespace, boolean preserveSymbols) throws InvalidCharacterException{
		this.preserveCapitals = preserveCapitals;
		this.preserveWhitespace = preserveWhitespace;
		this.preserveSymbols = preserveSymbols;
		polybiusSquare = new PolybiusSquare(false, false);
		reset();
	}

	/**
	 * Encodes the input string using the provided keyword.
	 *
	 * @param keyword      the keyword to use for encoding
	 * @param inputString  the string to be encoded
	 * @return the encoded string
	 * @throws InvalidKeywordException if the keyword is invalid
	 * @throws InvalidInputException   if the input string is invalid
	 * @throws InvalidCharacterException if the input contains invalid characters
	 */
	public String encode(String keyword, String inputString) throws InvalidKeywordException, InvalidInputException, InvalidCharacterException{
		//Set the parameters
		reset();
		setKeyword(keyword);
		setInputString(inputString);

		//Encode and return the message
		encode();
		return outputString;
	}
	/**
	 * Decodes the input string using the provided keyword.
	 *
	 * @param keyword      the keyword to use for decoding
	 * @param inputString  the string to be decoded
	 * @return the decoded string
	 * @throws InvalidKeywordException if the keyword is invalid
	 * @throws InvalidInputException   if the input string is invalid
	 * @throws InvalidCharacterException if the input contains invalid characters
	 */
	public String decode(String keyword, String inputString) throws InvalidKeywordException, InvalidInputException, InvalidCharacterException{
		//Set the parameters
		reset();
		setKeyword(keyword);
		setInputString(inputString);

		//Decode and return the message
		decode();
		return outputString;
	}

	//?Getters
	/**
	 * Gets the current input string.
	 *
	 * @return the input string
	 */
	public String getInputString(){
		return inputString;
	}
	/**
	 * Gets the current output string.
	 *
	 * @return the output string
	 */
	public String getOutputString(){
		return outputString;
	}
	/**
	 * Gets the current keyword.
	 *
	 * @return the keyword
	 */
	public String getKeyword(){
		return keyword;
	}
	/**
	 * Resets all fields to their default values.
	 */
	public void reset(){
		logger.debug("Resetting fields");

		inputString = "";
		outputString = "";
		keyword = "";
	}
}

//MattrixwvWebsite/src/main/java/com/mattrixwv/cipherstream/polysubstitution/Columnar.java
//Mattrixwv
// Created: 01-16-22
//Modified: 08-11-24
/*
	Copyright (C) 2024  Mattrixwv

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.cipherstream.polysubstitution;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mattrixwv.cipherstream.exceptions.InvalidCharacterException;
import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;


/**
 * This class implements the Columnar Transposition cipher for encoding and decoding messages.
 * The Columnar Transposition cipher rearranges the letters of the plaintext based on a keyword,
 * and then reads the message column-wise to produce the encoded output. For decoding, it reverses
 * this process.
 * <p>
 * This class provides methods to configure the cipher, process input strings, and generate output.
 * It also includes options to preserve capitalization, whitespace, symbols, and to handle padding characters.
 * </p>
 */
public class Columnar{
	private static final Logger logger = LoggerFactory.getLogger(Columnar.class);
	//?Fields
	/** The message that needs to be encoded/decoded */
	protected String inputString;
	/** The encoded/decoded message */
	protected String outputString;
	/** The keyword used to create the grid */
	protected String keyword;
	/** The character that is added to the end of a string to bring it to the correct length */
	protected char characterToAdd;
	/** The number of characters that were added to the end of the message */
	protected int charsAdded;
	/** The grid used to encode/decode the message */
	protected ArrayList<ArrayList<Character>> grid;
	//?Settings
	/** Persist capitals in the output string */
	protected boolean preserveCapitals;
	/** Persist whitespace in the output string */
	protected boolean preserveWhitespace;
	/** Persist symbols in the output string */
	protected boolean preserveSymbols;
	/** Remove the padding letters added to the cipher */
	protected boolean removePadding;


	/**
	 * Strip the inputString of all non-letter characters and change them to capitals
	 *
	 * @return The input string in capital letters with all non-letter characters removed
	 */
	protected String getCleanInputString(){
		logger.debug("Cleaning input string");
		return inputString.toUpperCase().replaceAll("[^A-Z]", "");
	}
	/**
	 * Creates the grid used for encoding.
	 */
	protected void createGridEncode(){
		logger.debug("Creating grid for encoding");

		//Add the keyword to the first row in the array
		grid = new ArrayList<>();
		grid.add(new ArrayList<>(Arrays.asList(keyword.chars().mapToObj(c -> (char)c).toArray(Character[]::new))));

		//Create the input that will be used for encoding
		String gridInputString = getCleanInputString();

		//Create arrays from the inputString of keyword.size length and add them each as new rows to the grid
		//?gridRow could be changed to grid.size(), but would it be worth the extra confusion? Probably not unless I really want to min/max
		for(int gridRow = 1;(keyword.length() * gridRow) <= gridInputString.length();++gridRow){
			grid.add(new ArrayList<>(Arrays.asList(
				gridInputString.substring(keyword.length() * (gridRow - 1), (keyword.length() * gridRow))
					.chars().mapToObj(c->(char)c).toArray(Character[]::new)
			)));
		}
	}
	/**
	 * Creates the grid used for decoding.
	 */
	protected void createGridDecode(){
		logger.debug("Creating grid for decoding");

		//Add the keyword to the first row in the array
		grid = new ArrayList<>();
		StringBuilder orderedKeyword = new StringBuilder();
		for(int cnt : getKeywordAlphaLocations()){
			orderedKeyword.append(keyword.charAt(cnt));
		}
		grid.add(new ArrayList<>(Arrays.asList(orderedKeyword.chars().mapToObj(c -> (char)c).toArray(Character[]::new))));

		//Create the input that will be used for encoding
		String gridInputString = getCleanInputString();

		//Make sure the grid has the appropritate number of rows
		int numRows = inputString.length() / keyword.length();
		while(grid.size() <= numRows){
			grid.add(new ArrayList<>());
		}
		//Add each character to the grid
		int rowCnt = 1;
		for(char ch : gridInputString.toCharArray()){
			grid.get(rowCnt++).add(ch);
			if(rowCnt > numRows){
				rowCnt = 1;
			}
		}
	}
	/**
	 * Sets the input string for encoding, applying necessary modifications and padding.
	 *
	 * @param inputString the input string to encode
	 * @throws InvalidInputException if the input string is null or blank
	 */
	protected void setInputStringEncode(String inputString) throws InvalidInputException{
		logger.debug("Setting input string for encoding");

		//Ensure the input isn't null
		if(inputString == null){
			throw new InvalidInputException("Input must not be null");
		}

		logger.debug("Original input string '{}'", inputString);

		//Apply removal options
		if(!preserveCapitals){
			logger.debug("Removing case");

			inputString = inputString.toUpperCase();
		}
		if(!preserveWhitespace){
			logger.debug("Removing whitespace");

			inputString = inputString.replaceAll("\\s", "");
		}
		if(!preserveSymbols){
			logger.debug("Removing symbols");

			inputString = inputString.replaceAll("[^a-zA-Z\\s]", "");
		}

		//Make sure the input is the correct length
		this.inputString = inputString;
		StringBuilder inputStringBuilder = new StringBuilder();
		inputStringBuilder.append(inputString);
		int cleanLength = getCleanInputString().length();
		int charsToAdd = (cleanLength % keyword.length());
		if(charsToAdd != 0){
			charsToAdd = keyword.length() - charsToAdd;

			logger.debug("Appending {} characters", charsToAdd);
		}
		for(int cnt = 0;cnt < charsToAdd;++cnt){
			inputStringBuilder.append(characterToAdd);
		}
		charsAdded = charsToAdd;
		inputString = inputStringBuilder.toString();

		//Save the string
		logger.debug("Cleaned input string '{}'", inputString);
		this.inputString = inputString;

		//Ensure the string isn't blank
		if(this.inputString.isBlank() || getCleanInputString().isBlank()){
			throw new InvalidInputException("Input cannot be blank");
		}
	}
	/**
	 * Sets the input string for decoding, applying necessary adjustments and padding.
	 *
	 * @param inputString the input string to decode
	 * @throws InvalidInputException if the input string is null or blank
	 */
	protected void setInputStringDecode(String inputString) throws InvalidInputException{
		logger.debug("Setting input string for decoding");

		//Ensure the input isn't null
		if(inputString == null){
			throw new InvalidInputException("Input must not be null");
		}

		logger.debug("Original input string '{}'", inputString);

		//Apply removal options
		if(!preserveCapitals){
			logger.debug("Removing case");

			inputString = inputString.toUpperCase();
		}
		if(!preserveWhitespace){
			logger.debug("Removing whitespace");

			inputString = inputString.replaceAll("\\s", "");
		}
		if(!preserveSymbols){
			logger.debug("Removing symbols");

			inputString = inputString.replaceAll("[^a-zA-Z\\s]", "");
		}

		//Make sure the input is the correct length
		charsAdded = 0;
		this.inputString = inputString;
		//Figure out how many rows there will be
		int numRows = getCleanInputString().length() / keyword.length();
		//Get the number of characters the input is over the limit
		int numCharsOver = getCleanInputString().length() % keyword.length();
		if(numCharsOver > 0){
			//Get the first n of the characters in the keyword and their encoded column locations
			ArrayList<Integer> originalLocations = getKeywordOriginalLocations();
			ArrayList<Integer> longColumns = new ArrayList<>();
			for(int cnt = 0;cnt < numCharsOver;++cnt){
				longColumns.add(originalLocations.get(cnt));
			}

			//Add letters where they need to be added to fill out the grid
			StringBuilder input = new StringBuilder();
			int col = 0;
			int row = 0;
			for(char ch : inputString.toCharArray()){
				if(!Character.isAlphabetic(ch)){
					input.append(ch);
					continue;
				}
				if(row < numRows){
					input.append(ch);
					++row;
				}
				else{
					if(longColumns.contains(col)){
						input.append(ch);
						row = 0;
					}
					else{
						input.append(characterToAdd);
						++charsAdded;
						input.append(ch);
						row = 1;
					}
					++col;
				}
			}
			if(!longColumns.contains(col)){
				input.append(characterToAdd);
				++charsAdded;
			}
			inputString = input.toString();
		}

		//Save the input
		logger.debug("Cleaned input string '{}'", inputString);
		this.inputString = inputString;

		//Ensure the string isn't blank
		if(this.inputString.isBlank() || getCleanInputString().isBlank()){
			throw new InvalidInputException("Input cannot be blank");
		}
	}
	/**
	 * Creates the output string by reading the columns of the grid.
	 */
	protected void createOutputStringFromColumns(){
		logger.debug("Creating output string for encoding");

		//Get the current rows of any characters that you added
		logger.debug("Getting added characters");
		ArrayList<Integer> colsAddedTo = new ArrayList<>();
		if(removePadding){
			ArrayList<Integer> cols = getKeywordOriginalLocations();
			Collections.reverse(cols);
			for(int cnt = 0;cnt < charsAdded;++cnt){
				colsAddedTo.add(cols.get(cnt));
			}
		}
		else{
			charsAdded = 0;
		}

		//Turn the grid into a string
		logger.debug("Turning grid into string");
		StringBuilder gridOutput = new StringBuilder();
		for(int col = 0;col < grid.get(0).size();++col){
			for(int row = 1;row < grid.size();++row){
				gridOutput.append(grid.get(row).get(col));
			}
			if(colsAddedTo.contains(col)){
				gridOutput.deleteCharAt(gridOutput.length() - 1);
			}
		}

		//Preserve any remaining symbols in the string
		logger.debug("Formatting output string");
		StringBuilder output = new StringBuilder();
		for(int outputLoc = 0, inputLoc = 0;inputLoc < (inputString.length() - charsAdded);){
			char inputChar = inputString.charAt(inputLoc++);
			if(Character.isUpperCase(inputChar)){
				output.append(Character.toUpperCase(gridOutput.charAt(outputLoc++)));
			}
			else if(Character.isLowerCase(inputChar)){
				output.append(Character.toLowerCase(gridOutput.charAt(outputLoc++)));
			}
			else{
				output.append(inputChar);
			}
		}

		//Save and return the output
		outputString = output.toString();
		logger.debug("Output string '{}'", outputString);
	}
	/**
	 * Creates the output string by reading the rows of the grid.
	 */
	protected void createOutputStringFromRows(){
		logger.debug("Creating output string for decoding");

		//Turn the grid into a string
		logger.debug("Transforming grid to a string");
		StringBuilder gridOutput = new StringBuilder();
		for(int row = 1;row < grid.size();++row){
			for(int col = 0;col < grid.get(row).size();++col){
				gridOutput.append(grid.get(row).get(col));
			}
		}
		//Remove any added characters
		logger.debug("Removing padding");
		if(removePadding){
			for(int cnt = 0;cnt < charsAdded;++cnt){
				gridOutput.deleteCharAt(gridOutput.length() - 1);
			}
		}
		else{
			charsAdded = 0;
		}

		ArrayList<Integer> colsAddedTo = new ArrayList<>();
		if(removePadding){
			ArrayList<Integer> cols = getKeywordOriginalLocations();
			Collections.reverse(cols);
			for(int cnt = 0;cnt < charsAdded;++cnt){
				colsAddedTo.add(cols.get(cnt));
			}
		}
		//Preserve any remaining symbols in the string
		logger.debug("Formatting output string");
		StringBuilder output = new StringBuilder();
		for(int outputLoc = 0, inputLoc = 0, row = 1, col = 0;inputLoc < inputString.length();){
			char inputChar = inputString.charAt(inputLoc++);
			if(((row + 1) == grid.size()) && (colsAddedTo.contains(col)) && (Character.isAlphabetic(inputChar))){
				++col;
				row = 1;
				continue;
			}

			logger.debug("Working character {}", gridOutput.charAt(outputLoc));
			if(Character.isUpperCase(inputChar)){
				logger.debug("Adding upper case");

				output.append(Character.toUpperCase(gridOutput.charAt(outputLoc++)));
				++row;
			}
			else if(Character.isLowerCase(inputChar)){
				logger.debug("Adding lower case");

				output.append(Character.toLowerCase(gridOutput.charAt(outputLoc++)));
				++row;
			}
			else{
				logger.debug("Adding symbol");

				output.append(inputChar);
			}

			if(row == grid.size()){
				++col;
				row = 1;
			}
		}

		//Save and return the output
		outputString = output.toString();
		logger.debug("Decoded output string '{}'", outputString);
	}
	/**
	 * Sets the keyword used for encoding or decoding.
	 *
	 * @param keyword the keyword to use
	 * @throws InvalidKeywordException if the keyword is null, contains less than 2 letters, or contains invalid characters
	 */
	protected void setKeyword(String keyword) throws InvalidKeywordException{
		//Ensure the keyword isn't null
		if(keyword == null){
			throw new InvalidKeywordException("Keyword cannot be null");
		}

		logger.debug("Original keyword {}", keyword);

		//Strip all non-letter characters and change them to uppercase
		keyword = keyword.toUpperCase().replaceAll("[^A-Z]", "");
		this.keyword = keyword;

		logger.debug("Cleaned keyword {}", keyword);

		//Make sure a valid keyword is present
		if(this.keyword.length() < 2){
			throw new InvalidKeywordException("The keyword must contain at least 2 letters");
		}
	}
	/**
	 * Sets the character used for padding.
	 *
	 * @param characterToAdd the padding character
	 * @throws InvalidCharacterException if the padding character is not a letter
	 */
	protected void setCharacterToAdd(char characterToAdd) throws InvalidCharacterException{
		if(!Character.isAlphabetic(characterToAdd)){
			throw new InvalidCharacterException("Character to add must be a letter");
		}

		logger.debug("Setting character to add {}", characterToAdd);

		if(!preserveCapitals){
			characterToAdd = Character.toUpperCase(characterToAdd);
		}

		this.characterToAdd = characterToAdd;

		logger.debug("Character to add for padding {}", characterToAdd);
	}
	/**
	 * Gets the positions of the keyword characters in alphabetical order.
	 *
	 * @return the list of positions of keyword characters in alphabetical order
	 */
	protected ArrayList<Integer> getKeywordAlphaLocations(){
		logger.debug("Creating an array of keyword letter locations");

		ArrayList<Integer> orderedLocations = new ArrayList<>();
		//Go through every letter and check it against the keyword
		for(char ch = 'A';ch <= 'Z';++ch){
			for(int cnt = 0;cnt < keyword.length();++cnt){
				//If the current letter is the same as the letter we are looking for add the location to the array
				if(keyword.charAt(cnt) == ch){
					orderedLocations.add(cnt);
				}
			}
		}

		//Return the alphabetic locations
		logger.debug("Array of keyword letters {}", orderedLocations);
		return orderedLocations;
	}
	/**
	 * Gets the original positions of the keyword characters.
	 *
	 * @return the list of original positions of keyword characters
	 */
	protected ArrayList<Integer> getKeywordOriginalLocations(){
		logger.debug("Creating array of original keyword locations");

		//Figure out the order the columns are in
		ArrayList<Integer> orderedLocations = getKeywordAlphaLocations();
		//Figure out what order the columns need rearanged to
		ArrayList<Integer> originalOrder = new ArrayList<>();
		for(int orgCnt = 0;orgCnt < orderedLocations.size();++orgCnt){
			for(int orderedCnt = 0;orderedCnt < orderedLocations.size();++orderedCnt){
				if(orderedLocations.get(orderedCnt) == orgCnt){
					originalOrder.add(orderedCnt);
					break;
				}
			}
		}

		//Returning the locations
		logger.debug("Array of keyword letters {}", originalOrder);
		return originalOrder;
	}
	/**
	 * Rearranges the grid based on the specified order.
	 *
	 * @param listOrder the order in which columns should be rearranged
	 */
	protected void rearangeGrid(ArrayList<Integer> listOrder){
		logger.debug("Rearanging grid");

		//Create a new grid and make sure it is the same size as the original grid
		int numCol = grid.get(0).size();
		ArrayList<ArrayList<Character>> newGrid = new ArrayList<>(grid.size());
		for(int cnt = 0;cnt < grid.size();++cnt){
			newGrid.add(new ArrayList<>(numCol));
		}

		//Step through the list order, pull out the columns, and add them to the new grid
		for(int col : listOrder){
			for(int row = 0;row < grid.size();++row){
				newGrid.get(row).add(grid.get(row).get(col));
			}
		}

		//Save the new grid
		logger.debug("New grid {}", newGrid);
		grid = newGrid;
	}
	/**
	 * Encodes the input string using the Columnar cipher.
	 */
	protected void encode(){
		logger.debug("Encoding");

		//Create the grid
		createGridEncode();
		//Figure out the new column order
		ArrayList<Integer> orderedLocations = getKeywordAlphaLocations();
		//Rearange the grid to the new order
		rearangeGrid(orderedLocations);
		//Create the output
		createOutputStringFromColumns();
	}
	/**
	 * Decodes the input string using the Columnar cipher.
	 */
	protected void decode(){
		logger.debug("Decoding");

		//Create the grid
		createGridDecode();
		ArrayList<Integer> originalOrder = getKeywordOriginalLocations();
		//Rearange the grid to the original order
		rearangeGrid(originalOrder);
		//Create the output
		createOutputStringFromRows();
	}

	//?Constructors
	/**
	 * Constructs a new Columnar cipher instance with default settings.
	 * The default settings include preserving no capitalization, whitespace, or symbols, and using 'x' as the padding character.
	 *
	 * @throws InvalidCharacterException if the default padding character is invalid
	 */
	public Columnar() throws InvalidCharacterException{
		preserveCapitals = false;
		preserveWhitespace = false;
		preserveSymbols = false;
		removePadding = false;
		setCharacterToAdd('x');
		reset();
	}
	/**
	 * Constructs a new Columnar cipher instance with customizable settings.
	 *
	 * @param preserveCapitals whether to preserve capitalization in the output
	 * @param preserveWhitespace whether to preserve whitespace in the output
	 * @param preserveSymbols whether to preserve symbols in the output
	 * @param removePadding whether to remove padding characters from the output
	 * @throws InvalidCharacterException if the default padding character is invalid
	 */
	public Columnar(boolean preserveCapitals, boolean preserveWhitespace, boolean preserveSymbols, boolean removePadding) throws InvalidCharacterException{
		this.preserveCapitals = preserveCapitals;
		this.preserveWhitespace = preserveWhitespace;
		this.preserveSymbols = preserveSymbols;
		this.removePadding = removePadding;
		setCharacterToAdd('x');
		reset();
	}
	/**
	 * Constructs a new Columnar cipher instance with customizable settings and a specific padding character.
	 *
	 * @param preserveCapitals whether to preserve capitalization in the output
	 * @param preserveWhitespace whether to preserve whitespace in the output
	 * @param preserveSymbols whether to preserve symbols in the output
	 * @param removePadding whether to remove padding characters from the output
	 * @param characterToAdd the character to use for padding
	 * @throws InvalidCharacterException if the padding character is not a letter
	 */
	public Columnar(boolean preserveCapitals, boolean preserveWhitespace, boolean preserveSymbols, boolean removePadding, char characterToAdd) throws InvalidCharacterException{
		this.preserveCapitals = preserveCapitals;
		this.preserveWhitespace = preserveWhitespace;
		this.preserveSymbols = preserveSymbols;
		this.removePadding = removePadding;
		setCharacterToAdd(characterToAdd);
		reset();
	}

	/**
	 * Encodes the given input string using the provided keyword.
	 *
	 * @param keyword the keyword used for encoding
	 * @param inputString the message to encode
	 * @return the encoded message
	 * @throws InvalidKeywordException if the keyword is invalid
	 * @throws InvalidInputException if the input string is invalid
	 */
	public String encode(String keyword, String inputString) throws InvalidKeywordException, InvalidInputException{
		//Set the parameters
		reset();
		setKeyword(keyword);
		setInputStringEncode(inputString);

		//Encode and return the message
		encode();
		return outputString;
	}
	/**
	 * Decodes the given input string using the provided keyword.
	 *
	 * @param keyword the keyword used for decoding
	 * @param inputString the message to decode
	 * @return the decoded message
	 * @throws InvalidKeywordException if the keyword is invalid
	 * @throws InvalidInputException if the input string is invalid
	 */
	public String decode(String keyword, String inputString) throws InvalidKeywordException, InvalidInputException{
		//Set the parameters
		reset();
		setKeyword(keyword);
		setInputStringDecode(inputString);

		//Decode and return the message
		decode();
		return outputString;
	}

	/**
	 * Resets all fields to their default values.
	 */
	public void reset(){
		logger.debug("Resetting fields");

		inputString = "";
		outputString = "";
		keyword = "";
		grid = new ArrayList<>();
		charsAdded = 0;
	}

	//?Getters
	/**
	 * Gets the current input string.
	 *
	 * @return the input string
	 */
	public String getInputString(){
		return inputString;
	}
	/**
	 * Gets the current output string.
	 *
	 * @return the output string
	 */
	public String getOutputString(){
		return outputString;
	}
	/**
	 * Gets the current keyword.
	 *
	 * @return the keyword
	 */
	public String getKeyword(){
		return keyword;
	}
}

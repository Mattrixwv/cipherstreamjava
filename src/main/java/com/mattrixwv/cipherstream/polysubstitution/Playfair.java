//CipherStreamJava/src/main/java/com/mattrixwv/cipherstream/polysubstitution/Playfair.java
//Matthew Ellison
// Created: 07-30-21
//Modified: 08-11-24
/*
	Copyright (C) 2024  Mattrixwv

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package com.mattrixwv.cipherstream.polysubstitution;


import java.util.StringJoiner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mattrixwv.cipherstream.exceptions.InvalidCharacterException;
import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;


/**
 * Represents the Playfair cipher encryption and decryption.
 * The Playfair cipher is a digraph substitution cipher that encrypts pairs of letters.
 */
public class Playfair{
	private static final Logger logger = LoggerFactory.getLogger(Playfair.class);

	/** A class representing the location of a character in the grid */
	protected class CharLocation{
		/** The x location in the grid */
		protected int x;
		/** The y location in the grid */
		protected int y;
		/**
		 * Constructs a CharLocation with the specified x and y coordinates.
		 *
		 * @param x the x-coordinate of the character's location
		 * @param y the y-coordinate of the character's location
		 */
		public CharLocation(int x, int y){
			this.x = x;
			this.y = y;
		}
		/**
		 * Returns the x-coordinate of the character's location.
		 *
		 * @return the x-coordinate
		 */
		public int getX(){
			return x;
		}
		/**
		 * Returns the y-coordinate of the character's location.
		 *
		 * @return the y-coordinate
		 */
		public int getY(){
			return y;
		}
	}

	//?Fields
	/** The message that needs to be encoded/decoded */
	protected String inputString;
	/** The encoded/decoded message */
	protected String outputString;
	/** The keyword used to create the grid */
	protected String keyword;
	/** The grid used to encode/decode the message */
	protected char[][] grid;
	/** The letter that will need to be replaced in the grid and any input string or keyword */
	protected char replaced;
	/** The letter that replaced replaced in the input string or keyword */
	protected char replacer;
	/** The letter that will be placed between double letters in the input string if necessary or to make the string length even */
	protected char doubled;
	//?Settings
	/** Persist capitals in the output string */
	protected boolean preserveCapitals;
	/** Persist whitespace in the output string */
	protected boolean preserveWhitespace;
	/** Persist symbols in the output string */
	protected boolean preserveSymbols;


	/**
	 * Sets the doubled character.
	 *
	 * @param doubled the character to be used as the doubled character
	 * @throws InvalidCharacterException if the character is not a letter or is invalid
	 */
	protected void setDoubled(char doubled) throws InvalidCharacterException{
		logger.debug("Setting doubled");
		logger.debug("Original character {}", doubled);

		//Make sure the character is a letter
		logger.debug("Checking letter");
		if(!Character.isAlphabetic(doubled)){
			throw new InvalidCharacterException("The double letter replacement must be a letter");
		}

		//Make sure the 2 replacers are the same
		logger.debug("Checking same as replacer");
		if(doubled == replacer){
			throw new InvalidCharacterException("The double letter replacement cannot be the same as the regular replacer");
		}

		//Make sure the double letter replacement isn't the same as the letter being replaced
		logger.debug("Checking same as replaced");
		if(doubled == replaced){
			throw new InvalidCharacterException("The double letter replacement cannot be the same as the letter replaced");
		}

		if(!preserveCapitals){
			logger.debug("Removing capitals");

			doubled = Character.toUpperCase(doubled);
		}

		logger.debug("Setting doubled to {}", doubled);
		this.doubled = doubled;
	}
	/**
	 * Sets the replacer character.
	 *
	 * @param replacer the character to replace the replaced character
	 * @throws InvalidCharacterException if the character is not a letter or is invalid
	 */
	protected void setReplacer(char replacer) throws InvalidCharacterException{
		logger.debug("Setting replacer");
		logger.debug("Original character {}", replacer);

		//Make sure the character is a letter
		logger.debug("Checking letter");
		if(!Character.isAlphabetic(replacer)){
			throw new InvalidCharacterException("The replacer must be a letter");
		}

		//Make sure the character isn't the same as what it is supposed to replace
		logger.debug("Checking same as replaced");
		if(replacer == replaced){
			throw new InvalidCharacterException("The replacer cannot be the same letter as what it is replacing");
		}

		//Make sure the replacer isn't the same as the double letter replacer
		logger.debug("Checking same as doubled");
		if(replacer == doubled){
			throw new InvalidCharacterException("The replacer cannot be the same as the double letter replacer");
		}

		if(!preserveCapitals){
			logger.debug("Removing capitals");

			replacer = Character.toUpperCase(replacer);
		}

		logger.debug("Setting replacer to {}", replacer);
		this.replacer = replacer;
	}
	/**
	 * Sets the replaced character.
	 *
	 * @param replaced the character to be replaced
	 * @throws InvalidCharacterException if the character is not a letter or is invalid
	 */
	protected void setReplaced(char replaced) throws InvalidCharacterException{
		logger.debug("Setting replaced");
		logger.debug("Original character {}", replaced);

		//Make sure the character is a letter
		logger.debug("Checking letter");
		if(!Character.isAlphabetic(replaced)){
			throw new InvalidCharacterException("The replaced character must be a letter");
		}

		//Make sure the character isn't the same as what is replacing it
		logger.debug("Checking same as replacer");
		if(replaced == replacer){
			throw new InvalidCharacterException("The replaced letter cannot be the same as the replacing letter");
		}

		//Make sure the replacer isn't the same as the double letter replacer
		logger.debug("Checking same as doubled");
		if(replaced == doubled){
			throw new InvalidCharacterException("The replaced letter cannot be the same as the replacing letter");
		}

		if(!preserveCapitals){
			logger.debug("Removing capitals");

			replaced = Character.toUpperCase(replaced);
		}

		logger.debug("Setting replaced to {}", replaced);
		this.replaced = replaced;
	}
	/**
	 * Creates the grid from the keyword.
	 */
	protected void createGrid(){
		logger.debug("Creating grid from keyword");

		for(int row = 0;row < 5;++row){
			for(int col = 0;col < 5;++col){
				char letter = keyword.charAt((5 * row) + col);
				logger.debug("Letter {} going to position [{}] [{}]", letter, row, col);
				grid[row][col] = letter;
			}
		}

		logger.debug("Grid\n{}", getGrid());
	}
	/**
	 * Strips invalid characters from the string that needs encoding/decoding.
	 *
	 * @param inputString the input string to be cleaned
	 * @param encoding true if encoding, false if decoding
	 * @throws InvalidCharacterException if an invalid character is found
	 * @throws InvalidInputException if the input string is invalid
	 */
	protected void setInputString(String inputString, boolean encoding) throws InvalidCharacterException, InvalidInputException{
		logger.debug("Setting input string");

		//Make sure the input string is not null
		if(inputString == null){
			throw new InvalidInputException("The input string cannot be null");
		}

		logger.debug("Original input string {}", inputString);

		//Set the options
		if(!preserveCapitals){
			logger.debug("Removing case");

			inputString = inputString.toUpperCase();
		}
		if(!preserveWhitespace){
			logger.debug("Removing whitespace");

			inputString = inputString.replaceAll("\\s", "");
		}
		if(!preserveSymbols){
			logger.debug("Removing symbols");

			inputString = inputString.replaceAll("[^a-zA-Z\\s]", "");
		}

		//If there is nothing in the input string throw an exception
		if(inputString.isBlank()){
			throw new InvalidInputException("The input string cannot be blank");
		}

		//If this is encoding parse it and clean up any problems
		if(encoding){
			setInputStringEncode(inputString);
		}
		//If this is decoding just add it without parsing it
		else{
			//Throw an exception if replaced is included
			if(inputString.contains(Character.toString(replaced))){
				throw new InvalidCharacterException("An encoded message cannot contain a letter that needs replaced");
			}

			logger.debug("Clean input string '{}'", inputString);
			this.inputString = inputString;

			if((getPreparedInputString().length() % 2) == 1){
				throw new InvalidInputException("Input length must be even");
			}
		}

		if(getPreparedInputString().isBlank()){
			throw new InvalidInputException("Input must have at least 1 letter");
		}
	}
	/**
	 * Cleans up the input string for encoding.
	 *
	 * @param inputString the input string to be cleaned
	 */
	protected void setInputStringEncode(String inputString){
		logger.debug("Cleaning up input string for encoding");

		//Replace characters that need replaced
		logger.debug("Replacing all {} with {}", replaced, replacer);
		inputString = inputString.replace(Character.toString(replaced), Character.toString(replacer));

		//Check if there are any doubled characters
		StringBuilder cleanInput = new StringBuilder();
		int letterCount = 0;
		for(int cnt = 0;cnt < inputString.length();){
			logger.debug("Starting at character {}", cnt);
			//Advance until you find a letter, saving the input for inclusion
			StringBuilder prepend = new StringBuilder();
			while((cnt < inputString.length()) && !Character.isAlphabetic(inputString.charAt(cnt))){
				prepend.append(inputString.charAt(cnt++));
			}
			//If we have reached the end of the string end the loop
			if(cnt == inputString.length()){
				cleanInput.append(prepend);
				break;
			}
			//Get the next character
			char firstLetter = inputString.charAt(cnt++);
			++letterCount;

			//Advance until you find a letter, saving the input for inclusion
			StringBuilder middle = new StringBuilder();
			while((cnt < inputString.length()) && !Character.isAlphabetic(inputString.charAt(cnt))){
				middle.append(inputString.charAt(cnt++));
			}
			char secondLetter = '\0';
			//If we have not reached the end of the string get the next character
			if(cnt != inputString.length()){
				secondLetter = inputString.charAt(cnt++);
				++letterCount;
			}
			//If the second character is the same as the first character set the pointer back and use the doubled character
			if(secondLetter == firstLetter){
				--cnt;
				secondLetter = doubled;
			}

			//Add all of the gathered input to the cleaned up input
			logger.debug("Adding to clean input: {} {} {} {}", prepend, firstLetter, middle, secondLetter);
			cleanInput.append(prepend);
			cleanInput.append(firstLetter);
			cleanInput.append(middle);
			if(secondLetter != '\0'){
				cleanInput.append(secondLetter);
			}
		}

		//Check if there are an odd number of characters
		logger.debug("Checking odd characters");
		if((letterCount % 2) == 1){
			logger.debug("Adding final character to make even");
			int lastLetterLocation = cleanInput.length() - 1;
			while(!Character.isAlphabetic(cleanInput.charAt(lastLetterLocation))){
				--lastLetterLocation;
			}
			if(cleanInput.charAt(lastLetterLocation) == doubled){
				cleanInput.append(replacer);
			}
			else{
				cleanInput.append(doubled);
			}
		}

		this.inputString = cleanInput.toString();
		logger.debug("Cleaned input string '{}'", this.inputString);
	}
	/**
	 * Returns the input string ready for encoding.
	 *
	 * @return the prepared input string
	 */
	protected String getPreparedInputString(){
		logger.debug("Getting input string ready for encoding");

		String cleanString = inputString.toUpperCase();
		cleanString = cleanString.replaceAll("[^A-Z]", "");

		logger.debug("Prepared string '{}'", cleanString);

		return cleanString;
	}
	/**
	 * Strips invalid characters from the keyword and creates the grid.
	 *
	 * @param keyword the keyword to be processed
	 * @throws InvalidKeywordException if the keyword is invalid
	 */
	protected void setKeyword(String keyword) throws InvalidKeywordException{
		logger.debug("Setting keyword");

		if(keyword == null){
			throw new InvalidKeywordException("Keyword cannot be null");
		}

		logger.debug("Original keyword {}", keyword);

		//Change everything to uppercase
		logger.debug("Removing case");
		keyword = keyword.toUpperCase();

		//Removing everything except capital letters
		logger.debug("Removing all non-letter characters");
		keyword = keyword.replaceAll("[^A-Z]", "");

		//Add all letters in the alphabet to the key
		logger.debug("Appending the alphabet to the keyword");
		keyword += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		//Replace all replaced characters
		logger.debug("Replacing {} with {}", replaced, replacer);
		keyword = keyword.replaceAll(Character.toString(Character.toUpperCase(replaced)), Character.toString(Character.toUpperCase(replacer)));

		//Remove all duplicate chatacters
		logger.debug("Removing duplicate characters");
		StringBuilder uniqueKey = new StringBuilder();
		keyword.chars().distinct().forEach(c -> uniqueKey.append((char)c));
		this.keyword = uniqueKey.toString();
		logger.debug("Cleaned keyword {}", this.keyword);

		//Create the grid from the sanitized keyword
		createGrid();
	}
	/**
	 * Returns the location of the given character in the grid.
	 *
	 * @param letter the character whose location is to be found
	 * @return the location of the character in the grid
	 * @throws InvalidInputException if the character is not found in the grid
	 */
	protected CharLocation findChar(char letter) throws InvalidInputException{
		logger.debug("Finding character in grid {}", letter);

		for(int row = 0;row < grid.length;++row){
			for(int col = 0;col < grid[row].length;++col){
				if(grid[row][col] == letter){
					logger.debug("Found at {}, {}", row, col);

					return new CharLocation(row, col);
				}
			}
		}

		//If it was not found something went wrong
		throw new InvalidInputException("The character '" + letter + "' was not found in the grid");
	}
	/**
	 * Returns the character in the grid at the specified coordinates.
	 *
	 * @param x the x-coordinate
	 * @param y the y-coordinate
	 * @return the character at the specified coordinates
	 */
	protected char getGridChar(int x, int y){
		logger.debug("Getting character from grid[{}][{}]", x, y);

		if(x < 0){
			x += 5;
		}
		if(y < 0){
			y += 5;
		}

		char letter = grid[x % 5][y % 5];
		logger.debug("Character {}", letter);
		return letter;
	}
	/**
	 * Adds characters that aren't letters to the output.
	 *
	 * @param cleanString the cleaned string to be formatted
	 */
	protected void addCharactersToCleanString(String cleanString){
		logger.debug("Formatting output string");

		int outputCnt = 0;
		StringBuilder fullOutput = new StringBuilder();
		for(int inputCnt = 0;inputCnt < inputString.length();++inputCnt){
			logger.debug("Working character {}", inputString.charAt(inputCnt));

			if(Character.isUpperCase(inputString.charAt(inputCnt))){
				logger.debug("Appending uppercase");

				fullOutput.append(cleanString.charAt(outputCnt++));
			}
			else if(Character.isLowerCase(inputString.charAt(inputCnt))){
				logger.debug("Appending lowercase");

				fullOutput.append(Character.toLowerCase(cleanString.charAt(outputCnt++)));
			}
			else{
				logger.debug("Appending symbol");

				fullOutput.append(inputString.charAt(inputCnt));
			}
		}

		outputString = fullOutput.toString();
		logger.debug("Formatted output '{}'", outputString);
	}
	/**
	 * Encodes the input string using the Playfair cipher and stores the result in the output string.
	 *
	 * @throws InvalidInputException if the input string is invalid
	 */
	protected void encode() throws InvalidInputException{
		logger.debug("Encoding");

		StringBuilder output = new StringBuilder();
		int inputCnt = 0;
		String cleanString = getPreparedInputString();
		while(inputCnt < cleanString.length()){
			//Get the next 2 letters to be encoded
			char firstLetter = cleanString.charAt(inputCnt++);
			char secondLetter = cleanString.charAt(inputCnt++);

			logger.debug("Letters {} {}", firstLetter, secondLetter);

			//Find the letters in the grid
			CharLocation firstLocation = findChar(firstLetter);
			CharLocation secondLocation = findChar(secondLetter);

			//Encode the letters
			if(firstLocation.getX() == secondLocation.getX()){
				logger.debug("Row encoding");
				firstLetter = getGridChar(firstLocation.getX(), firstLocation.getY() + 1);
				secondLetter = getGridChar(secondLocation.getX(), secondLocation.getY() + 1);
			}
			else if(firstLocation.getY() == secondLocation.getY()){
				logger.debug("Column encoding");
				firstLetter = getGridChar(firstLocation.getX() + 1, firstLocation.getY());
				secondLetter = getGridChar(secondLocation.getX() + 1, secondLocation.getY());
			}
			else{
				logger.debug("Corner encoding");
				firstLetter = getGridChar(firstLocation.getX(), secondLocation.getY());
				secondLetter = getGridChar(secondLocation.getX(), firstLocation.getY());
			}

			//Add the new letters to the output string
			logger.debug("Encoded letters {} {}", firstLetter, secondLetter);
			output.append(firstLetter);
			output.append(secondLetter);
		}

		//Add other characters to the output string
		addCharactersToCleanString(output.toString());
	}
	/**
	 * Decodes the input string using the Playfair cipher and stores the result in the output string.
	 *
	 * @throws InvalidInputException if the input string is invalid
	 */
	protected void decode() throws InvalidInputException{
		logger.debug("Decoding");

		StringBuilder output = new StringBuilder();
		int inputCnt = 0;
		String cleanString = getPreparedInputString();
		while(inputCnt < cleanString.length()){
			//Get the next 2 letters to be encoded
			char firstLetter = cleanString.charAt(inputCnt++);
			char secondLetter = cleanString.charAt(inputCnt++);

			logger.debug("Letters {} {}", firstLetter, secondLetter);

			//Find the letters in the grid
			CharLocation firstLocation = findChar(firstLetter);
			CharLocation secondLocation = findChar(secondLetter);

			//Decode the letters
			if(firstLocation.getX() == secondLocation.getX()){
				logger.debug("Row decoding");
				firstLetter = getGridChar(firstLocation.getX(), firstLocation.getY() - 1);
				secondLetter = getGridChar(secondLocation.getX(), secondLocation.getY() - 1);
			}
			else if(firstLocation.getY() == secondLocation.getY()){
				logger.debug("Column decoding");
				firstLetter = getGridChar(firstLocation.getX() - 1, firstLocation.getY());
				secondLetter = getGridChar(secondLocation.getX() - 1, secondLocation.getY());
			}
			else{
				logger.debug("Corner decoding");
				firstLetter = getGridChar(firstLocation.getX(), secondLocation.getY());
				secondLetter = getGridChar(secondLocation.getX(), firstLocation.getY());
			}

			//Add the new letters to the output string
			logger.debug("Decoded letters {} {}", firstLetter, secondLetter);
			output.append(firstLetter);
			output.append(secondLetter);
		}

		//Add other characters to the output string
		addCharactersToCleanString(output.toString());
	}

	//?Constructor
	/**
	 * Constructs a Playfair cipher instance with default settings.
	 *
	 * @throws InvalidCharacterException if default characters are invalid
	 */
	public Playfair() throws InvalidCharacterException{
		reset();
		preserveCapitals = false;
		preserveWhitespace = false;
		preserveSymbols = false;
		setReplaced('j');
		setReplacer('i');
		setDoubled('x');
	}
	/**
	 * Constructs a Playfair cipher instance with specified settings.
	 *
	 * @param preserveCapitals whether to preserve capital letters
	 * @param preserveWhitespace whether to preserve whitespace
	 * @param preserveSymbols whether to preserve symbols
	 * @throws InvalidCharacterException if default characters are invalid
	 */
	public Playfair(boolean preserveCapitals, boolean preserveWhitespace, boolean preserveSymbols) throws InvalidCharacterException{
		reset();
		this.preserveCapitals = preserveCapitals;
		this.preserveWhitespace = preserveWhitespace;
		this.preserveSymbols = preserveSymbols;
		setReplaced('j');
		setReplacer('i');
		setDoubled('x');
	}
	/**
	 * Constructs a Playfair cipher instance with specified settings and characters.
	 *
	 * @param preserveCapitals whether to preserve capital letters
	 * @param preserveWhitespace whether to preserve whitespace
	 * @param preserveSymbols whether to preserve symbols
	 * @param replaced the character to be replaced
	 * @param replacer the character to replace the replaced character
	 * @param doubled the character to use for doubled letters
	 * @throws InvalidCharacterException if any character is invalid
	 */
	public Playfair(boolean preserveCapitals, boolean preserveWhitespace, boolean preserveSymbols, char replaced, char replacer, char doubled) throws InvalidCharacterException{
		reset();
		this.preserveCapitals = preserveCapitals;
		this.preserveWhitespace = preserveWhitespace;
		this.preserveSymbols = preserveSymbols;
		setReplaced(replaced);
		setReplacer(replacer);
		setDoubled(doubled);
	}

	/**
	 * Sets the keyword and input string and encodes the message.
	 *
	 * @param keyword the keyword for the cipher
	 * @param input the message to encode
	 * @return the encoded message
	 * @throws InvalidCharacterException if any character is invalid
	 * @throws InvalidInputException if the input string is invalid
	 */
	public String encode(String keyword, String input) throws InvalidCharacterException, InvalidInputException{
		reset();
		setKeyword(keyword);
		setInputString(input, true);
		encode();
		return outputString;
	}
	/**
	 * Sets the keyword and input string and decodes the message.
	 *
	 * @param keyword the keyword for the cipher
	 * @param input the encoded message to decode
	 * @return the decoded message
	 * @throws InvalidCharacterException if any character is invalid
	 * @throws InvalidInputException if the input string is invalid
	 */
	public String decode(String keyword, String input) throws InvalidCharacterException, InvalidInputException{
		reset();
		setKeyword(keyword);
		setInputString(input, false);
		decode();
		return outputString;
	}

	/**
	 * Resets all variables to their default values.
	 */
	public void reset(){
		logger.debug("Resetting fields");

		grid = new char[5][5];
		inputString = "";
		outputString = "";
		keyword = "";
	}
	//?Getters
	/**
	 * Returns the replaced character.
	 *
	 * @return the replaced character
	 */
	public char getReplaced(){
		return replaced;
	}
	/**
	 * Returns the replacer character.
	 *
	 * @return the replacer character
	 */
	public char getReplacer(){
		return replacer;
	}
	/**
	 * Returns the doubled character.
	 *
	 * @return the doubled character
	 */
	public char getDoubled(){
		return doubled;
	}
	/**
	 * Returns the keyword used in the cipher.
	 *
	 * @return the keyword
	 */
	public String getKeyword(){
		return keyword;
	}
	/**
	 * Returns the input string that was set for encoding/decoding.
	 *
	 * @return the input string
	 */
	public String getInputString(){
		return inputString;
	}
	/**
	 * Returns the output string after encoding/decoding.
	 *
	 * @return the output string
	 */
	public String getOutputString(){
		return outputString;
	}
	/**
	 * Return the grid used for encoding/decoding.
	 *
	 * @return the grid as a string
	 */
	public String getGrid(){
		logger.debug("Creating string from grid");

		StringJoiner gridString = new StringJoiner("\n");
		for(char[] row : grid){
			StringJoiner rowString = new StringJoiner(" ", "[", "]");
			for(char col : row){
				rowString.add(Character.toString(col));
			}
			gridString.add(rowString.toString());
		}

		return gridString.toString();
	}
}

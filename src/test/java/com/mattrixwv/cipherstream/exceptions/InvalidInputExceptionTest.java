//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/exception/TestInvalidInputException.java
//Mattrixwv
// Created: 04-14-23
//Modified: 04-19-24
package com.mattrixwv.cipherstream.exceptions;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


public class InvalidInputExceptionTest{
	private String message = "message";
	private Throwable cause = new Exception();


	@Test
	public void testConstructor_default(){
		InvalidInputException exception = new InvalidInputException();
		assertNull(exception.getMessage());
		assertNull(exception.getCause());
	}

	@Test
	public void testConstructor_message(){
		InvalidInputException exception = new InvalidInputException(message);
		assertEquals(message, exception.getMessage());
		assertNull(exception.getCause());
	}

	@Test
	public void testConstructor_cause(){
		InvalidInputException exception = new InvalidInputException(cause);
		assertEquals(cause.toString(), exception.getMessage());
		assertEquals(cause, exception.getCause());
	}

	@Test
	public void testConstructor_messageAndCause(){
		InvalidInputException exception = new InvalidInputException(message, cause);
		assertEquals(message, exception.getMessage());
		assertEquals(cause, exception.getCause());
	}
}

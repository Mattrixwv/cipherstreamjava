//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/exceptions/TestInvalidKeyException.java
//Mattrixwv
// Created: 04-14-23
//Modified: 04-19-24
package com.mattrixwv.cipherstream.exceptions;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


public class InvalidKeyExceptionTest{
	public String message = "message";
	public Throwable cause = new Exception();


	@Test
	public void testConstructor_default(){
		InvalidKeyException exception = new InvalidKeyException();
		assertNull(exception.getMessage());
		assertNull(exception.getCause());
	}

	@Test
	public void testConstructor_message(){
		InvalidKeyException exception = new InvalidKeyException(message);
		assertEquals(message, exception.getMessage());
		assertNull(exception.getCause());
	}

	@Test
	public void testConstructor_cause(){
		InvalidKeyException exception = new InvalidKeyException(cause);
		assertEquals(cause.toString(), exception.getMessage());
		assertEquals(cause, exception.getCause());
	}

	@Test
	public void testConstructor_messageAndCause(){
		InvalidKeyException exception = new InvalidKeyException(message, cause);
		assertEquals(message, exception.getMessage());
		assertEquals(cause, exception.getCause());
	}
}

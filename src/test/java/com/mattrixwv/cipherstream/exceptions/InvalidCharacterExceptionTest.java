//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/exceptions/TestInvalidcharacterException.java
//Mattrixwv
// Created: 04-14-23
//Modified: 04-19-24
package com.mattrixwv.cipherstream.exceptions;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


public class InvalidCharacterExceptionTest{
	private String message = "message";
	private Throwable cause = new Exception();


	@Test
	public void testConstructor_default(){
		InvalidCharacterException exception = new InvalidCharacterException();
		assertNull(exception.getMessage());
		assertNull(exception.getCause());
	}

	@Test
	public void testConstructor_message(){
		InvalidCharacterException exception = new InvalidCharacterException(message);
		assertEquals(message, exception.getMessage());
		assertNull(exception.getCause());
	}

	@Test
	public void testConstructor_cause(){
		InvalidCharacterException exception = new InvalidCharacterException(cause);
		assertEquals(cause.toString(), exception.getMessage());
		assertEquals(cause, exception.getCause());
	}

	@Test
	public void testConstructor_messageAndCause(){
		InvalidCharacterException exception = new InvalidCharacterException(message, cause);
		assertEquals(message, exception.getMessage());
		assertEquals(cause, exception.getCause());
	}
}

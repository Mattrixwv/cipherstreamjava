//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/exceptions/TestInvalidBaseException.java
//Mattrixwv
// Created: 04-14-23
//Modified: 04-19-24
package com.mattrixwv.cipherstream.exceptions;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


public class InvalidBaseExceptionTest{
	private static final String message = "message";
	private static final Throwable cause = new Exception();


	@Test
	public void testConstructor_default(){
		InvalidBaseException exception = new InvalidBaseException();
		assertNull(exception.getMessage());
		assertNull(exception.getCause());
	}

	@Test
	public void testConstructor_message(){
		InvalidBaseException exception = new InvalidBaseException(message);
		assertEquals(message, exception.getMessage());
		assertNull(exception.getCause());
	}

	@Test
	public void testConstructor_cause(){
		InvalidBaseException exception = new InvalidBaseException(cause);
		assertEquals(cause.toString(), exception.getMessage());
		assertEquals(cause, exception.getCause());
	}

	@Test
	public void testConstructor_messageAndCause(){
		InvalidBaseException exception = new InvalidBaseException(message, cause);
		assertEquals(message, exception.getMessage());
		assertEquals(cause, exception.getCause());
	}
}

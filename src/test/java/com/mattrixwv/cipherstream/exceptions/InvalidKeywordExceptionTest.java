//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/exceptions/TestInvalidKeywordException.java
//Mattrixwv
// Created: 04-14-23
//Modified: 04-19-24
package com.mattrixwv.cipherstream.exceptions;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


public class InvalidKeywordExceptionTest{
	private String message = "message";
	private Throwable cause = new Exception();


	@Test
	public void testConstructor_default(){
		InvalidKeywordException exception = new InvalidKeywordException();
		assertNull(exception.getMessage());
		assertNull(exception.getCause());
	}

	@Test
	public void testConstructor_message(){
		InvalidKeywordException exception = new InvalidKeywordException(message);
		assertEquals(message, exception.getMessage());
		assertNull(exception.getCause());
	}

	@Test
	public void testConstructor_cause(){
		InvalidKeywordException exception = new InvalidKeywordException(cause);
		assertEquals(cause.toString(), exception.getMessage());
		assertEquals(cause, exception.getCause());
	}

	@Test
	public void testConstructor_messageAndCause(){
		InvalidKeywordException exception = new InvalidKeywordException(message, cause);
		assertEquals(message, exception.getMessage());
		assertEquals(cause, exception.getCause());
	}
}

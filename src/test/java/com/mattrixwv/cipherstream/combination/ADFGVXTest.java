//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/combination/TestADFGVX.java
//Mattrixwv
// Created: 01-26-22
//Modified: 04-19-24
package com.mattrixwv.cipherstream.combination;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;
import com.mattrixwv.cipherstream.polysubstitution.Columnar;
import com.mattrixwv.cipherstream.polysubstitution.LargePolybiusSquare;


@ExtendWith(MockitoExtension.class)
public class ADFGVXTest{
	@InjectMocks
	private ADFGVX cipher;
	@Mock
	private Logger logger;
	//Variables
	private static final String decodedString = "Message to^encode";
	private static final String decodedStringClean = "MESSAGETOENCODE";
	private static final String encodedString = "AXgvdavfxgagfa afag^aaxdxfgdagda";
	private static final String encodedStringClean = "AXGVDAVFXGAGFAAFAGAAXDXFGDAGDA";
	private static final String keyword = "keyword";
	private static final String squareKeyword = "SquareKeyword";


	@Test
	public void testConstructor_default(){
		cipher = new ADFGVX();

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.squareKeyword);
		assertEquals("", cipher.squareKeyword);
		assertNotNull(cipher.largePolybiusSquare);
		assertNotNull(cipher.columnar);
	}

	@Test
	public void testConstructor_preservesCapitals(){
		cipher = new ADFGVX(true, false, false);

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveSymbols);
		assertFalse(cipher.preserveWhitespace);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.squareKeyword);
		assertEquals("", cipher.squareKeyword);
		assertNotNull(cipher.largePolybiusSquare);
		assertNotNull(cipher.columnar);
	}

	@Test
	public void testConstructor_preservesWhitespace(){
		cipher = new ADFGVX(false, true, false);

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveSymbols);
		assertTrue(cipher.preserveWhitespace);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.squareKeyword);
		assertEquals("", cipher.squareKeyword);
		assertNotNull(cipher.largePolybiusSquare);
		assertNotNull(cipher.columnar);
	}

	@Test
	public void testConstructor_preservesSymbols(){
		cipher = new ADFGVX(false, false, true);

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveSymbols);
		assertFalse(cipher.preserveWhitespace);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.squareKeyword);
		assertEquals("", cipher.squareKeyword);
		assertNotNull(cipher.largePolybiusSquare);
		assertNotNull(cipher.columnar);
	}

	@Test
	public void testSetSquareKeyword(){
		cipher.setSquareKeyword(squareKeyword);

		assertEquals(squareKeyword, cipher.squareKeyword);
		verify(logger, times(1)).debug("squareKeyword '{}'", squareKeyword);
	}

	@Test
	public void testSetSquareKeyword_null(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setSquareKeyword(null);
		});

		assertEquals("", cipher.squareKeyword);
		verify(logger, never()).debug(eq("squareKeyword '{}'"), anyString());
	}

	@Test
	public void testSetKeyword(){
		cipher.setKeyword(keyword);

		assertEquals(keyword, cipher.keyword);
		verify(logger, times(1)).debug("keyword '{}'", keyword);
	}

	@Test
	public void testSetKeyword_null(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword(null);
		});

		assertEquals("", cipher.keyword);
		verify(logger, never()).debug(eq("keyword '{}'"), anyString());
	}

	@Test
	public void testSetInputString(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString, cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString);
	}

	@Test
	public void testSetInputString_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, times(1)).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.toUpperCase());
	}

	@Test
	public void testSetInputString_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing capitals");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputString_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("[^a-zA-Z\\s]", ""));
	}

	@Test
	public void testSetInputString_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputString_null(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Original input string '{}'"), anyString());
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testFormateOutputStringEncode(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedStringClean;

		cipher.formatOutputStringEncode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Formatting output string to match input string");
		verify(logger, times(17)).debug(eq("Input character {}"), anyChar());
		verify(logger, times(1)).debug("Converting output to uppercase");
		verify(logger, times(14)).debug("Converting output to lowercase");
		verify(logger, times(2)).debug("Appending symbol to output");
		verify(logger, times(1)).debug("Saving output string '{}'", encodedString);
	}

	@Test
	public void testFormatOutputStringDecode(){
		cipher.outputString = decodedStringClean;
		cipher.inputString = encodedString;

		cipher.formatOutputStringDecode();

		assertEquals(decodedString, cipher.outputString);
		verify(logger, times(1)).debug("Formatting output string to match input string");
		verify(logger, times(17)).debug(eq("Input character {}"), anyChar());
		verify(logger, times(1)).debug("Converting output to uppercase");
		verify(logger, times(14)).debug("Converting output to lowercase");
		verify(logger, times(2)).debug("Appending symbol to output");
		verify(logger, times(1)).debug("Saving output string '{}'", decodedString);
	}

	@Test
	public void testEncode(){
		cipher.inputString = decodedString;
		cipher.keyword = keyword;
		cipher.squareKeyword = squareKeyword;

		cipher.encode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding using Polybius Square");
		verify(logger, times(1)).debug("Replacing coordinates with letters");
		verify(logger, times(1)).debug("Encoding using columnar");
	}

	@Test
	public void testDecode(){
		cipher.inputString = encodedString;
		cipher.keyword = keyword;
		cipher.squareKeyword = squareKeyword;

		cipher.decode();

		assertEquals(decodedString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding using columnar");
		verify(logger, times(1)).debug("Replacing letters with coordinates");
		verify(logger, times(1)).debug("Decoding using Polybius Square");
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.squareKeyword = squareKeyword;
		cipher.keyword = keyword;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(encodedString, cipher.getOutputString());
		assertEquals(squareKeyword, cipher.getSquareKeyword());
		assertEquals(keyword, cipher.getKeyword());
	}

	@Test
	public void testReset(){
		LargePolybiusSquare ps = cipher.largePolybiusSquare;
		Columnar columnar = cipher.columnar;
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.squareKeyword = squareKeyword;
		cipher.keyword = keyword;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.squareKeyword);
		assertEquals("", cipher.keyword);
		assertNotSame(ps, cipher.largePolybiusSquare);
		assertNotSame(columnar, cipher.columnar);
		verify(logger, times(1)).debug("Resetting fields");
	}

	@Test
	public void testPracticalEncoding(){
		cipher = new ADFGVX(true, true, true);

		String output = cipher.encode(squareKeyword, keyword, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(keyword, cipher.keyword);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalEncoding_clean(){
		cipher = new ADFGVX(false, false, false);

		String output = cipher.encode(squareKeyword, keyword, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(keyword, cipher.keyword);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
	}

	@Test
	public void testPracticalDecoding(){
		cipher = new ADFGVX(true, true, true);

		String output = cipher.decode(squareKeyword, keyword, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(keyword, cipher.keyword);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testPracticalDecoding_clean(){
		cipher = new ADFGVX(false, false, false);

		String output = cipher.decode(squareKeyword, keyword, encodedString);

		assertEquals(encodedStringClean, cipher.inputString);
		assertEquals(keyword, cipher.keyword);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}
}

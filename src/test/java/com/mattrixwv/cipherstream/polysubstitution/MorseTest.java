//CipherStreamJava/src/main/java/com/mattrixwv/CipherStreamJava/MoresTest.java
//Matthew Ellison
// Created: 07-28-21
//Modified: 04-19-24
package com.mattrixwv.cipherstream.polysubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidInputException;


@ExtendWith(MockitoExtension.class)
public class MorseTest{
	@InjectMocks
	private Morse cipher;
	@Mock
	private Logger logger;
	//Fields
	private static final String inputString = "Message to^encode123";
	private static final String inputStringClean = "MESSAGETOENCODE123";
	private static final String outputString = "-- . ... ... .- --. . - --- . -. -.-. --- -.. . .---- ..--- ...--";


	@Test
	public void testConstructor(){
		cipher = new Morse();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testSetInputStringEncode(){
		cipher.setInputStringEncode(inputString);

		assertEquals(inputStringClean, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", inputString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace and symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", inputStringClean);
	}

	@Test
	public void testSetInputStringEncode_blank(){
		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", "");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace and symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputStringEncode_blankClean(){
		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode("*&^");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", "*&^");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace and symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputStringEncode_null(){
		String nullString = null;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", nullString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace and symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputStringDecode(){
		cipher.setInputStringDecode(outputString);

		assertEquals(outputString, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", outputString);
		verify(logger, times(1)).debug("Checking for invalid characters");
		verify(logger, times(1)).debug("Saving");
	}

	@Test
	public void testSetInputStringDecode_invalid(){
		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode("*");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", "*");
		verify(logger, times(1)).debug("Checking for invalid characters");
		verify(logger, never()).debug("Saving");
	}

	@Test
	public void testSetInputStringDecode_blank(){
		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", "");
		verify(logger, times(1)).debug("Checking for invalid characters");
		verify(logger, never()).debug("Saving");
	}

	@Test
	public void testSetInputStringDecode_null(){
		String nullString = null;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", nullString);
		verify(logger, never()).debug("Checking for invalid characters");
		verify(logger, never()).debug("Saving");
	}

	@Test
	public void testEncode(){
		cipher.inputString = inputStringClean;

		cipher.encode();

		assertEquals(outputString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(18)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(15)).debug("Appending letter");
		verify(logger, times(3)).debug("Appending number");
		verify(logger, times(1)).debug("Saving encoded string '{}'", outputString);
	}

	@Test
	public void testEncode_invalid(){
		cipher.inputString = "*";

		assertThrows(InvalidInputException.class, () -> {
			cipher.encode();
		});

		assertEquals("", cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(1)).debug(eq("Working character {}"), anyChar());
		verify(logger, never()).debug("Appending letter");
		verify(logger, never()).debug("Appending number");
		verify(logger, never()).debug("Saving encoded string '{}'", outputString);
	}

	@Test
	public void testDecode(){
		cipher.inputString = outputString;

		cipher.decode();

		assertEquals(inputStringClean, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(18)).debug(eq("Working letter {}"), anyString());
		verify(logger, times(18)).debug(eq("Decoded letter {}"), anyChar());
		verify(logger, times(1)).debug("Saving decoded string '{}'", inputStringClean);
	}

	@Test
	public void testDecode_invalid(){
		cipher.inputString = "A";

		assertThrows(InvalidInputException.class, () -> {
			cipher.decode();
		});

		assertEquals("", cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(1)).debug(eq("Working letter {}"), anyString());
		verify(logger, never()).debug(eq("Decoded letter {}"), anyChar());
		verify(logger, never()).debug(eq("Saving decoded string '{}'"), anyString());
	}

	@Test
	public void testGetters(){
		cipher.inputString = inputString;
		cipher.outputString = outputString;

		assertEquals(inputString, cipher.getInputString());
		assertEquals(outputString, cipher.getOutputString());
	}

	@Test
	public void testReset(){
		cipher.inputString = inputString;
		cipher.outputString = outputString;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		verify(logger, times(1)).debug("Resetting");
	}

	@Test
	public void testPracticalEncode(){
		String output = cipher.encode(inputString);

		assertEquals(inputStringClean, cipher.inputString);
		assertEquals(outputString, cipher.outputString);
		assertEquals(outputString, output);
	}

	@Test
	public void testPracticalDecode(){
		String output = cipher.decode(outputString);

		assertEquals(outputString, cipher.inputString);
		assertEquals(inputStringClean, cipher.outputString);
		assertEquals(inputStringClean, output);
	}
}

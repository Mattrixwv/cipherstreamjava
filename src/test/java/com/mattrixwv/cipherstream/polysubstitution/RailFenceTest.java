//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/polysubstitution/RailFenceTest.java
//Mattrixwv
// Created: 03-21-22
//Modified: 04-19-24
package com.mattrixwv.cipherstream.polysubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidBaseException;
import com.mattrixwv.cipherstream.exceptions.InvalidInputException;


@ExtendWith(MockitoExtension.class)
public class RailFenceTest{
	@InjectMocks
	private RailFence cipher;
	@Mock
	private Logger logger;
	//Fields
	private static final String decodedString = "Message to^encode";
	private static final String decodedStringClean = "MESSAGETOENCODE";
	private static final String encodedString3 = "Maooesg te^cdsene";
	private static final String encodedString3Clean = "MAOOESGTECDSENE";
	private static final String encodedString5 = "Moetese ne^sgcdao";
	private static final String encodedString5Clean = "MOETESENESGCDAO";
	private static final StringBuilder[] fence3 = new StringBuilder[]{
		new StringBuilder("Maoo"),
		new StringBuilder("esgtecd"),
		new StringBuilder("sene")
	};
	private static final StringBuilder[] fence5 = new StringBuilder[]{
		new StringBuilder("Mo"),
		new StringBuilder("ete"),
		new StringBuilder("sene"),
		new StringBuilder("sgcd"),
		new StringBuilder("ao")
	};


	@Test
	public void testConstructor_default(){
		cipher = new RailFence();

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertNull(cipher.fence);
	}

	@Test
	public void testConstructor_noCapitals(){
		cipher = new RailFence(false, true, true);

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertNull(cipher.fence);
	}

	@Test
	public void testConstructor_noWhitespace(){
		cipher = new RailFence(true, false, true);

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertNull(cipher.fence);
	}

	@Test
	public void testConstructor_noSymbols(){
		cipher = new RailFence(true, true, false);

		assertTrue(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertNull(cipher.fence);
	}

	@Test
	public void testSetInputString(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString, cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Clean input string '{}'", decodedString);
	}

	@Test
	public void testSetInputString_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Clean input string '{}'", decodedString.toUpperCase());
	}

	@Test
	public void testSetInputString_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Clean input string '{}'", decodedString.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputString_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Clean input string '{}'", decodedString.replaceAll("[^a-zA-Z\\s]", ""));
	}

	@Test
	public void testSetInputString_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Clean input string '{}'", "");
	}

	@Test
	public void testSetInputString_null(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Original input string '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Clean input string '{}'"), anyString());
	}

	@Test
	public void testSetNumRails(){
		cipher.setNumRails(3);

		assertEquals(3, cipher.fence.length);
		verify(logger, times(1)).debug("Creating {} rails", 3);
	}

	@Test
	public void testSetNumRails_short(){
		assertThrows(InvalidBaseException.class, () -> {
			cipher.setNumRails(1);
		});

		assertNull(cipher.fence);
		verify(logger, never()).debug(eq("Creating {} rails"), anyInt());
	}

	@Test
	public void testGetCleanInputString(){
		cipher.inputString = decodedString;

		String output = cipher.getCleanInputString();

		assertEquals(decodedString.replaceAll("[^a-zA-Z]", ""), output);
		verify(logger, times(1)).debug("Getting input string for encoding");
	}

	@Test
	public void testFormatOutput(){
		cipher.inputString = decodedString;

		cipher.formatOutput(encodedString3Clean);

		assertEquals(encodedString3, cipher.outputString);
		verify(logger, times(1)).debug("Formatting output string");
		verify(logger, times(17)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(1)).debug("Formatting uppercase");
		verify(logger, times(14)).debug("Formatting lowercase");
		verify(logger, times(2)).debug("Inserting symbol");
		verify(logger, times(1)).debug("Formatted output '{}'", encodedString3);
	}

	@Test
	public void testGetDecodedStringFromFence(){
		cipher.fence = fence3;

		String output = cipher.getDecodedStringFromFence();

		assertEquals(decodedString.replaceAll("[^a-zA-Z]", ""), output);
		verify(logger, times(1)).debug("Getting decoded string from the fence");
		verify(logger, times(1)).debug(eq("Fence output '{}'"), any(StringBuilder.class));
	}

	@Test
	public void testEncode(){
		cipher.inputString = decodedString;
		cipher.fence = new StringBuilder[]{
			new StringBuilder(),
			new StringBuilder(),
			new StringBuilder()
		};

		cipher.encode();

		assertEquals(encodedString3, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(15)).debug(eq("Working character '{}'"), anyChar());
		verify(logger, times(9)).debug("Moving up");
		verify(logger, times(6)).debug("Moving down");
		verify(logger, times(4)).debug("Swapping to down");
		verify(logger, times(3)).debug("Swapping to up");
		verify(logger, times(1)).debug("Appending rows from the fence");
	}

	@Test
	public void testDecode(){
		cipher.inputString = encodedString3;
		cipher.fence = new StringBuilder[]{
			new StringBuilder(),
			new StringBuilder(),
			new StringBuilder()
		};

		cipher.decode();

		assertEquals(decodedString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(1)).debug("Number of characters in the top rail {}", 4);
		verify(logger, times(1)).debug("Number of characters in the middle rails {}", 8);
		verify(logger, times(1)).debug("Number of characters in the bottom rail {}", 4);
		verify(logger, times(1)).debug("Adding characters to the rails");
		verify(logger, times(1)).debug("Appending the bottom rail");
		verify(logger, times(1)).debug("Fence output '{}'", decodedString.replaceAll("[^a-zA-Z]", ""));
	}

	@Test
	public void testDecode_length(){
		cipher.inputString = "aa";
		cipher.fence = new StringBuilder[]{
			new StringBuilder(),
			new StringBuilder(),
			new StringBuilder()
		};

		cipher.decode();

		assertEquals("aa", cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(1)).debug("Number of characters in the top rail {}", 1);
		verify(logger, times(1)).debug("Number of characters in the middle rails {}", 1);
		verify(logger, times(1)).debug("Number of characters in the bottom rail {}", 0);
		verify(logger, times(1)).debug("Adding characters to the rails");
		verify(logger, times(1)).debug("Appending the bottom rail");
		verify(logger, times(1)).debug("Fence output '{}'", "aa");
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString3;
		cipher.fence = fence3;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(encodedString3, cipher.getOutputString());
		assertEquals(fence3.length, cipher.getNumRails());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString3;
		cipher.fence = fence3;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertNull(cipher.fence);
	}

	@Test
	public void testPracticalEncoding_3(){
		cipher = new RailFence(true, true, true);

		String output = cipher.encode(3, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(fence3.length, cipher.fence.length);
		assertEquals(fence3[0].toString(), cipher.fence[0].toString());
		assertEquals(fence3[1].toString(), cipher.fence[1].toString());
		assertEquals(fence3[2].toString(), cipher.fence[2].toString());
		assertEquals(encodedString3, cipher.outputString);
		assertEquals(encodedString3, output);
	}

	@Test
	public void testPracticalEncoding_3Clean(){
		cipher = new RailFence(false, false, false);

		String output = cipher.encode(3, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(fence3.length, cipher.fence.length);
		assertEquals(fence3[0].toString().toUpperCase(), cipher.fence[0].toString());
		assertEquals(fence3[1].toString().toUpperCase(), cipher.fence[1].toString());
		assertEquals(fence3[2].toString().toUpperCase(), cipher.fence[2].toString());
		assertEquals(encodedString3Clean, cipher.outputString);
		assertEquals(encodedString3Clean, output);
	}

	@Test
	public void testPracticalEncoding_5(){
		cipher = new RailFence(true, true, true);

		String output = cipher.encode(5, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(fence5.length, cipher.fence.length);
		assertEquals(fence5[0].toString(), cipher.fence[0].toString());
		assertEquals(fence5[1].toString(), cipher.fence[1].toString());
		assertEquals(fence5[2].toString(), cipher.fence[2].toString());
		assertEquals(fence5[3].toString(), cipher.fence[3].toString());
		assertEquals(fence5[4].toString(), cipher.fence[4].toString());
		assertEquals(encodedString5, cipher.outputString);
		assertEquals(encodedString5, output);
	}

	@Test
	public void testPracticalEncoding_5Clean(){
		cipher = new RailFence(false, false, false);

		String output = cipher.encode(5, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(fence5.length, cipher.fence.length);
		assertEquals(fence5[0].toString().toUpperCase(), cipher.fence[0].toString());
		assertEquals(fence5[1].toString().toUpperCase(), cipher.fence[1].toString());
		assertEquals(fence5[2].toString().toUpperCase(), cipher.fence[2].toString());
		assertEquals(fence5[3].toString().toUpperCase(), cipher.fence[3].toString());
		assertEquals(fence5[4].toString().toUpperCase(), cipher.fence[4].toString());
		assertEquals(encodedString5Clean, cipher.outputString);
		assertEquals(encodedString5Clean, output);
	}

	@Test
	public void testPracticalDecoding_3(){
		cipher = new RailFence(true, true, true);

		String output = cipher.decode(3, encodedString3);

		assertEquals(encodedString3, cipher.inputString);
		assertEquals(fence3.length, cipher.fence.length);
		assertEquals(fence3[0].toString(), cipher.fence[0].toString());
		assertEquals(fence3[1].toString(), cipher.fence[1].toString());
		assertEquals(fence3[2].toString(), cipher.fence[2].toString());
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testPracticalDecoding_3Clean(){
		cipher = new RailFence(false, false, false);

		String output = cipher.decode(3, encodedString3);

		assertEquals(encodedString3Clean, cipher.inputString);
		assertEquals(fence3.length, cipher.fence.length);
		assertEquals(fence3[0].toString().toUpperCase(), cipher.fence[0].toString());
		assertEquals(fence3[1].toString().toUpperCase(), cipher.fence[1].toString());
		assertEquals(fence3[2].toString().toUpperCase(), cipher.fence[2].toString());
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}

	@Test
	public void testPracticalDecoding_5(){
		cipher = new RailFence(true, true, true);

		String output = cipher.decode(5, encodedString5);

		assertEquals(encodedString5, cipher.inputString);
		assertEquals(fence5.length, cipher.fence.length);
		assertEquals(fence5[0].toString(), cipher.fence[0].toString());
		assertEquals(fence5[1].toString(), cipher.fence[1].toString());
		assertEquals(fence5[2].toString(), cipher.fence[2].toString());
		assertEquals(fence5[3].toString(), cipher.fence[3].toString());
		assertEquals(fence5[4].toString(), cipher.fence[4].toString());
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testPracticalDecoding_5Clean(){
		cipher = new RailFence(false, false, false);

		String output = cipher.decode(5, encodedString5);

		assertEquals(encodedString5Clean, cipher.inputString);
		assertEquals(fence5.length, cipher.fence.length);
		assertEquals(fence5[0].toString().toUpperCase(), cipher.fence[0].toString());
		assertEquals(fence5[1].toString().toUpperCase(), cipher.fence[1].toString());
		assertEquals(fence5[2].toString().toUpperCase(), cipher.fence[2].toString());
		assertEquals(fence5[3].toString().toUpperCase(), cipher.fence[3].toString());
		assertEquals(fence5[4].toString().toUpperCase(), cipher.fence[4].toString());
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}
}

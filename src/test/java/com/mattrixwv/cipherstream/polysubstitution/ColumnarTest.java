//Mattrixwv/src/test/java/com/mattrixwv/cipherstream/polysubstitution/ColumnarTest.java
//Mattrixwv
// Created: 01-16-22
//Modified: 04-19-24
package com.mattrixwv.cipherstream.polysubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidCharacterException;
import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;


@ExtendWith(MockitoExtension.class)
public class ColumnarTest{
	@InjectMocks
	private Columnar cipher;
	@Mock
	private Logger logger;
	//Variables
	private static final String decodedString = "Message to*encode";
	private static final String decodedStringPadded = "Message to*encodexxxxxx";
	private static final String decodedStringClean = "MESSAGETOENCODEXXXXXX";
	private static final String encodedString = "Edeomte ac*gosnse";
	//TODO: This is a bug that needs fixed
	private static final String encodedStringPadingAdded = "Edxeoxmte ac*xgoxsnxsex";	//When padding is added to outputString for decoding
	private static final String encodedStringPadded = "Edxeoxm te*acxgoxsnxsex";	//When padding is left in outputString
	private static final String encodedStringClean = "EDXEOXMTEACXGOXSNXSEX";
	private static final String keyword = "ke yw*ord";
	private static final String keywordClean = "KEYWORD";
	private static final ArrayList<ArrayList<Character>> encodeGrid = new ArrayList<>(
		List.of(
			new ArrayList<>(
				List.of('K', 'E', 'Y', 'W', 'O', 'R', 'D')
			),
			new ArrayList<>(
				List.of('M', 'E', 'S', 'S', 'A', 'G', 'E')
			),
			new ArrayList<>(
				List.of('T', 'O', 'E', 'N', 'C', 'O', 'D')
			),
			new ArrayList<>(
				List.of('E', 'X', 'X', 'X', 'X', 'X', 'X')
			)
		)
	);
	private static final ArrayList<ArrayList<Character>> decodeGrid = new ArrayList<>(
		List.of(
			new ArrayList<>(
				List.of('D', 'E', 'K', 'O', 'R', 'W', 'Y')
			),
			new ArrayList<>(
				List.of('E', 'E', 'M', 'A', 'G', 'S', 'S')
			),
			new ArrayList<>(
				List.of('D', 'O', 'T', 'C', 'O', 'N', 'E')
			),
			new ArrayList<>(
				List.of('X', 'X', 'E', 'X', 'X', 'X', 'X')
			)
		)
	);


	@Test
	public void testConstructor_default(){
		cipher = new Columnar();

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertFalse(cipher.removePadding);
		assertEquals('X', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.grid);
		assertEquals(0, cipher.charsAdded);
	}

	@Test
	public void testConstructorPadding_noCapitals(){
		cipher = new Columnar(true, false, false, false);

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertFalse(cipher.removePadding);
		assertEquals('x', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.grid);
		assertEquals(0, cipher.charsAdded);
	}

	@Test
	public void testConstructorPadding_noWhitespace(){
		cipher = new Columnar(false, true, false, false);

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertFalse(cipher.removePadding);
		assertEquals('X', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.grid);
		assertEquals(0, cipher.charsAdded);
	}

	@Test
	public void testCosntructorPadding_noSymbols(){
		cipher = new Columnar(false, false, true, false);

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertFalse(cipher.removePadding);
		assertEquals('X', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.grid);
		assertEquals(0, cipher.charsAdded);
	}

	@Test
	public void testConstructorPadding_noPadding(){
		cipher = new Columnar(false, false, false, true);

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertTrue(cipher.removePadding);
		assertEquals('X', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.grid);
		assertEquals(0, cipher.charsAdded);
	}

	@Test
	public void testConstructorCharacter_noCapitals(){
		cipher = new Columnar(true, false, false, false, 'y');

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertFalse(cipher.removePadding);
		assertEquals('y', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.grid);
		assertEquals(0, cipher.charsAdded);
	}

	@Test
	public void testConstructorCharacter_noWhitespace(){
		cipher = new Columnar(false, true, false, false, 'y');

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertFalse(cipher.removePadding);
		assertEquals('Y', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.grid);
		assertEquals(0, cipher.charsAdded);
	}

	@Test
	public void testConstructorCharacter_noSymbols(){
		cipher = new Columnar(false, false, true, false, 'y');

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertFalse(cipher.removePadding);
		assertEquals('Y', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.grid);
		assertEquals(0, cipher.charsAdded);
	}

	@Test
	public void testConstructorCharacter_noPadding(){
		cipher = new Columnar(false, false, false, true, 'y');

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertTrue(cipher.removePadding);
		assertEquals('Y', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.grid);
		assertEquals(0, cipher.charsAdded);
	}

	@Test
	public void testConstructorCharacter_differentCharacter(){
		cipher = new Columnar(false, false, false, false, 'z');

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertFalse(cipher.removePadding);
		assertEquals('Z', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.grid);
		assertEquals(0, cipher.charsAdded);
	}

	@Test
	public void testGetCleanInputString(){
		cipher.inputString = decodedString;

		String output = cipher.getCleanInputString();

		assertEquals(decodedString.toUpperCase().replaceAll("[^A-Z]", ""), output);
	}

	@Test
	public void testCreateGridEncode(){
		cipher.inputString = decodedStringPadded;
		cipher.keyword = keywordClean;

		cipher.createGridEncode();

		assertEquals(encodeGrid, cipher.grid);
		verify(logger, times(1)).debug("Creating grid for encoding");
	}

	@Test
	public void testCreateGridDecode(){
		cipher.inputString = encodedStringPadingAdded;
		cipher.keyword = keywordClean;

		cipher.createGridDecode();

		assertEquals(decodeGrid, cipher.grid);
		verify(logger, times(1)).debug("Creating grid for decoding");
	}

	@Test
	public void testSetInputStringEncode(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.characterToAdd = 'x';

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedStringPadded, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Appending {} characters", 6);
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringPadded);
	}

	@Test
	public void testSetInputStringEncode_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.characterToAdd = 'X';

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedStringPadded.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Appending {} characters", 6);
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringPadded.toUpperCase());
	}

	@Test
	public void testSetInputStringEncode_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.characterToAdd = 'x';

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedStringPadded.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Appending {} characters", 6);
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringPadded.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputStringEncode_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;
		cipher.keyword = keywordClean;
		cipher.characterToAdd = 'x';

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedStringPadded.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Appending {} characters", 6);
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringPadded.replaceAll("[^a-zA-Z\\s]", ""));
	}

	@Test
	public void testSetInputStringEncode_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keyword;
		cipher.characterToAdd = 'x';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Appending {} characters"), anyInt());
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputStringEncode_blankClean(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keyword;
		cipher.characterToAdd = 'x';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode("*");
		});

		assertEquals("*", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, times(1)).debug("Original input string '{}'", "*");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Appending {} characters"), anyInt());
		verify(logger, times(1)).debug("Cleaned input string '{}'", "*");
	}

	@Test
	public void testSetInputStringEncode_null(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keyword;
		cipher.characterToAdd = 'x';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, never()).debug(eq("Original input string '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Appending {} characters"), anyInt());
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputStringDecode(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.characterToAdd = 'x';

		cipher.setInputStringDecode(encodedString);

		assertEquals(encodedStringPadingAdded, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", encodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedStringPadingAdded);
	}

	@Test
	public void testSetInputStringDecode_perfectLength(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.characterToAdd = 'x';

		cipher.setInputStringDecode("Message to encod");

		assertEquals("Message to encod", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", "Message to encod");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "Message to encod");
	}

	@Test
	public void testSetInputStringDecode_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.characterToAdd = 'X';

		cipher.setInputStringDecode(encodedString);

		assertEquals(encodedStringPadingAdded.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", encodedString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedStringPadingAdded.toUpperCase());
	}

	@Test
	public void testSetInputStringDecode_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.characterToAdd = 'x';

		cipher.setInputStringDecode(encodedString);

		assertEquals(encodedStringPadingAdded.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", encodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedStringPadingAdded.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputStringDecode_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;
		cipher.keyword = keywordClean;
		cipher.characterToAdd = 'x';

		cipher.setInputStringDecode(encodedString);

		assertEquals(encodedStringPadingAdded.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", encodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedStringPadingAdded.replaceAll("[^a-zA-Z\\s]", ""));
	}

	@Test
	public void testSetInputStringDecode_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.characterToAdd = 'x';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputStringDecode_blankClean(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.characterToAdd = 'x';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode("*");
		});

		assertEquals("*", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", "*");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "*");
	}

	@Test
	public void testSetInputStringDecode_null(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.characterToAdd = 'x';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, never()).debug(eq("Original input string '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testCreateOutputStringFromColumns(){
		cipher.removePadding = true;
		cipher.inputString = decodedStringPadded;
		cipher.keyword = keywordClean;
		cipher.grid = decodeGrid;
		cipher.charsAdded = 6;
		cipher.characterToAdd = 'x';

		cipher.createOutputStringFromColumns();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Creating output string for encoding");
		verify(logger, times(1)).debug("Getting added characters");
		verify(logger, times(1)).debug("Turning grid into string");
		verify(logger, times(1)).debug("Formatting output string");
		verify(logger, times(1)).debug("Output string '{}'", encodedString);
	}

	@Test
	public void testCreateOutputStringFromColumns_padding(){
		cipher.removePadding = false;
		cipher.inputString = decodedStringPadded;
		cipher.keyword = keywordClean;
		cipher.grid = decodeGrid;
		cipher.charsAdded = 6;
		cipher.characterToAdd = 'x';

		cipher.createOutputStringFromColumns();

		assertEquals(encodedStringPadded, cipher.outputString);
		verify(logger, times(1)).debug("Creating output string for encoding");
		verify(logger, times(1)).debug("Getting added characters");
		verify(logger, times(1)).debug("Turning grid into string");
		verify(logger, times(1)).debug("Formatting output string");
		verify(logger, times(1)).debug("Output string '{}'", encodedStringPadded);
	}

	@Test
	public void testCreateOutputStringFromRows(){
		cipher.removePadding = false;
		cipher.inputString = encodedStringPadded;
		cipher.keyword = keywordClean;
		cipher.grid = encodeGrid;
		cipher.charsAdded = 6;
		cipher.characterToAdd = 'x';

		cipher.createOutputStringFromRows();

		assertEquals(decodedStringPadded, cipher.outputString);
		verify(logger, times(1)).debug("Creating output string for decoding");
		verify(logger, times(1)).debug("Transforming grid to a string");
		verify(logger, times(1)).debug("Removing padding");
		verify(logger, times(1)).debug("Formatting output string");
		verify(logger, times(23)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(1)).debug("Adding upper case");
		verify(logger, times(20)).debug("Adding lower case");
		verify(logger, times(2)).debug("Adding symbol");
		verify(logger, times(1)).debug("Decoded output string '{}'", decodedStringPadded);
	}

	@Test
	public void testCreateOuputStringFromRows_removePadding(){
		cipher.removePadding = true;
		cipher.inputString = encodedStringPadingAdded;
		cipher.keyword = keywordClean;
		cipher.grid = encodeGrid;
		cipher.charsAdded = 6;
		cipher.characterToAdd = 'x';

		cipher.createOutputStringFromRows();

		assertEquals(decodedString, cipher.outputString);
		verify(logger, times(1)).debug("Creating output string for decoding");
		verify(logger, times(1)).debug("Transforming grid to a string");
		verify(logger, times(1)).debug("Removing padding");
		verify(logger, times(1)).debug("Formatting output string");
		verify(logger, times(17)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(1)).debug("Adding upper case");
		verify(logger, times(14)).debug("Adding lower case");
		verify(logger, times(2)).debug("Adding symbol");
		verify(logger, times(1)).debug("Decoded output string '{}'", decodedString);
	}

	@Test
	public void testSetKeyword(){
		cipher.setKeyword(keyword);

		assertEquals(keywordClean, cipher.keyword);
		verify(logger, times(1)).debug("Original keyword {}", keyword);
		verify(logger, times(1)).debug("Cleaned keyword {}", keywordClean);
	}

	@Test
	public void testSetKeyword_blank(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword("");
		});

		assertEquals("", cipher.keyword);
		verify(logger, times(1)).debug("Original keyword {}", "");
		verify(logger, times(1)).debug("Cleaned keyword {}", "");
	}

	@Test
	public void testSetKeyword_null(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword(null);
		});

		assertEquals("", cipher.keyword);
		verify(logger, never()).debug(eq("Original keyword {}"), anyString());
		verify(logger, never()).debug(eq("Cleaned keyword {}"), anyString());
	}

	@Test
	public void testSetCharacterToAdd(){
		cipher.preserveCapitals = false;

		cipher.setCharacterToAdd('a');

		assertEquals('A', cipher.characterToAdd);
		verify(logger, times(1)).debug("Setting character to add {}", 'a');
		verify(logger, times(1)).debug("Character to add for padding {}", 'A');
	}

	@Test
	public void testSetCharacterToAdd_preserveCapitals(){
		cipher.preserveCapitals = true;

		cipher.setCharacterToAdd('a');

		assertEquals('a', cipher.characterToAdd);
		verify(logger, times(1)).debug("Setting character to add {}", 'a');
		verify(logger, times(1)).debug("Character to add for padding {}", 'a');
	}

	@Test
	public void testSetCharacterToAdd_symbol(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setCharacterToAdd('*');
		});

		assertEquals('X', cipher.characterToAdd);
		verify(logger, never()).debug(eq("Setting character to add {}"), anyChar());
		verify(logger, never()).debug(eq("Character to add for padding {}"), anyChar());
	}

	@Test
	public void testGetKeywordAlphaLocations(){
		ArrayList<Integer> alphaLocations = new ArrayList<>(List.of(6, 1, 0, 4, 5, 3, 2));

		cipher.keyword = keywordClean;

		ArrayList<Integer> returnedLocations = cipher.getKeywordAlphaLocations();

		assertEquals(alphaLocations, returnedLocations);
		verify(logger, times(1)).debug("Creating an array of keyword letter locations");
		verify(logger, times(1)).debug("Array of keyword letters {}", alphaLocations);
	}

	@Test
	public void testGetKeywordOriginalLocations(){
		ArrayList<Integer> orderedLocations = new ArrayList<>(List.of(2, 1, 6, 5, 3, 4, 0));

		cipher.keyword = keywordClean;

		ArrayList<Integer> returnedLocations = cipher.getKeywordOriginalLocations();

		assertEquals(orderedLocations, returnedLocations);
		verify(logger, times(1)).debug("Creating array of original keyword locations");
		verify(logger, times(1)).debug("Array of keyword letters {}", orderedLocations);
	}

	@Test
	public void testRearangeGrid_encode(){
		ArrayList<Integer> listOrder = new ArrayList<>(List.of(6, 1, 0, 4, 5, 3, 2));

		cipher.grid = encodeGrid;

		cipher.rearangeGrid(listOrder);

		assertEquals(decodeGrid, cipher.grid);
		verify(logger, times(1)).debug("Rearanging grid");
		verify(logger, times(1)).debug("New grid {}", decodeGrid);
	}

	@Test
	public void testRearangeGrid_decode(){
		ArrayList<Integer> listOrder = new ArrayList<>(List.of(2, 1, 6, 5, 3, 4, 0));

		cipher.grid = decodeGrid;

		cipher.rearangeGrid(listOrder);

		assertEquals(encodeGrid, cipher.grid);
		verify(logger, times(1)).debug("Rearanging grid");
		verify(logger, times(1)).debug("New grid {}", encodeGrid);
	}

	@Test
	public void testEncode(){
		cipher.preserveCapitals = true;
		cipher.removePadding = true;
		cipher.inputString = decodedStringPadded;
		cipher.keyword = keywordClean;
		cipher.charsAdded = 6;
		cipher.characterToAdd = 'x';

		cipher.encode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(1)).debug("Creating grid for encoding");
		verify(logger, times(2)).debug("Creating an array of keyword letter locations");
		verify(logger, times(1)).debug("Rearanging grid");
		verify(logger, times(1)).debug("Creating output string for encoding");
	}

	@Test
	public void testEncode_padding(){
		cipher.preserveCapitals = true;
		cipher.removePadding = false;
		cipher.inputString = decodedStringPadded;
		cipher.keyword = keywordClean;
		cipher.charsAdded = 6;
		cipher.characterToAdd = 'x';

		cipher.encode();

		assertEquals(encodedStringPadded, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(1)).debug("Creating grid for encoding");
		verify(logger, times(1)).debug("Creating an array of keyword letter locations");
		verify(logger, times(1)).debug("Rearanging grid");
		verify(logger, times(1)).debug("Creating output string for encoding");
	}

	@Test
	public void testDecode(){
		cipher.preserveCapitals = true;
		cipher.removePadding = true;
		cipher.inputString = encodedStringPadingAdded;
		cipher.keyword = keywordClean;
		cipher.charsAdded = 6;
		cipher.characterToAdd = 'x';

		cipher.decode();

		assertEquals(decodedString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(1)).debug("Creating grid for decoding");
		verify(logger, times(2)).debug("Creating array of original keyword locations");
		verify(logger, times(1)).debug("Rearanging grid");
		verify(logger, times(1)).debug("Creating output string for decoding");
	}

	@Test
	public void testDecode_padding(){
		cipher.preserveCapitals = true;
		cipher.removePadding = false;
		cipher.inputString = encodedStringPadded;
		cipher.keyword = keywordClean;
		cipher.charsAdded = 6;
		cipher.characterToAdd = 'x';

		cipher.decode();

		assertEquals(decodedStringPadded, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(1)).debug("Creating grid for decoding");
		verify(logger, times(1)).debug("Creating array of original keyword locations");
		verify(logger, times(1)).debug("Rearanging grid");
		verify(logger, times(1)).debug("Creating output string for decoding");
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.keyword = keyword;
		cipher.outputString = encodedString;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(keyword, cipher.getKeyword());
		assertEquals(encodedString, cipher.getOutputString());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.keyword = keyword;
		cipher.outputString = encodedString;
		cipher.grid = encodeGrid;
		cipher.charsAdded = 5;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		assertEquals(new ArrayList<>(), cipher.grid);
		assertEquals(0, cipher.charsAdded);
	}

	@Test
	public void testPracticalEncode(){
		cipher = new Columnar(true, true, true, true);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedStringPadded, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalEncode_noPadding(){
		cipher = new Columnar(true, true, true, false);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedStringPadded, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(encodedStringPadded, cipher.outputString);
		assertEquals(encodedStringPadded, output);
	}

	@Test
	public void testPracticalEncode_clean(){
		cipher = new Columnar(false, false, false, false);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
	}

	@Test
	public void testPracticalDecode(){
		cipher = new Columnar(true, true, true, true);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedStringPadingAdded, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testPracticalDecode_noPadding(){
		cipher = new Columnar(true, true, true, false);

		String output = cipher.decode(keyword, encodedStringPadded);

		assertEquals(encodedStringPadded, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(decodedStringPadded, cipher.outputString);
		assertEquals(decodedStringPadded, output);
	}

	@Test
	public void testPracticalDecode_clean(){
		cipher = new Columnar(false, false, false, false);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedStringClean, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}
}

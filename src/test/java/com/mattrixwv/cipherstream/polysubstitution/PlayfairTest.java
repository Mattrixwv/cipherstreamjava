//CipherStreamJava/src/main/java/com/mattrixwv/CipherStreamJava/PlayfairTest.java
//Matthew Ellison
// Created: 07-30-21
//Modified: 04-19-24
package com.mattrixwv.cipherstream.polysubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidCharacterException;
import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;
import com.mattrixwv.cipherstream.polysubstitution.Playfair.CharLocation;


@ExtendWith(MockitoExtension.class)
public class PlayfairTest{
	@InjectMocks
	private Playfair cipher;
	@Mock
	private Logger logger;
	//Fields
	private static final String decodedString = "Hide the gold in - the@tree+stump";
	private static final String decodedStringPadded = "Hide the gold in - the@trexe+stump";
	private static final String decodedStringClean = "HIDETHEGOLDINTHETREXESTUMP";
	private static final String encodedString = "Bmod zbx dnab ek - udm@uixmm+ouvif";
	private static final String encodedStringClean = "BMODZBXDNABEKUDMUIXMMOUVIF";
	private static final String keyword = "Play-fair@Exam ple";
	private static final String keywordClean = "PLAYFIREXMBCDGHKNOQSTUVWZ";
	private static final char[][] grid = new char[][]{
		{'P', 'L', 'A', 'Y', 'F'},
		{'I', 'R', 'E', 'X', 'M'},
		{'B', 'C', 'D', 'G', 'H'},
		{'K', 'N', 'O', 'Q', 'S'},
		{'T', 'U', 'V', 'W', 'Z'}
	};


	@Test
	public void testConstructor_default(){
		cipher = new Playfair();

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[5][5], cipher.grid);
		assertEquals('J', cipher.replaced);
		assertEquals('I', cipher.replacer);
		assertEquals('X', cipher.doubled);
	}

	@Test
	public void testConstructor_noCapitals(){
		cipher = new Playfair(false, true, true);

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[5][5], cipher.grid);
		assertEquals('J', cipher.replaced);
		assertEquals('I', cipher.replacer);
		assertEquals('X', cipher.doubled);
	}

	@Test
	public void testConstructor_noWhitespace(){
		cipher = new Playfair(true, false, true);

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[5][5], cipher.grid);
		assertEquals('j', cipher.replaced);
		assertEquals('i', cipher.replacer);
		assertEquals('x', cipher.doubled);
	}

	@Test
	public void testConstructor_noSymbols(){
		cipher = new Playfair(true, true, false);

		assertTrue(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[5][5], cipher.grid);
		assertEquals('j', cipher.replaced);
		assertEquals('i', cipher.replacer);
		assertEquals('x', cipher.doubled);
	}

	@Test
	public void testConstructor_letters_noCapitals(){
		cipher = new Playfair(false, true, true, 'a', 'b', 'c');

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[5][5], cipher.grid);
		assertEquals('A', cipher.replaced);
		assertEquals('B', cipher.replacer);
		assertEquals('C', cipher.doubled);
	}

	@Test
	public void testConstructor_letters_noWhitespace(){
		cipher = new Playfair(true, false, true, 'a', 'b', 'c');

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[5][5], cipher.grid);
		assertEquals('a', cipher.replaced);
		assertEquals('b', cipher.replacer);
		assertEquals('c', cipher.doubled);
	}

	@Test
	public void testConstructor_letters_noSymbols(){
		cipher = new Playfair(true, true, false, 'a', 'b', 'c');

		assertTrue(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[5][5], cipher.grid);
		assertEquals('a', cipher.replaced);
		assertEquals('b', cipher.replacer);
		assertEquals('c', cipher.doubled);
	}

	@Test
	public void testSetDoubled(){
		cipher.setDoubled('a');

		assertEquals('A', cipher.doubled);
		verify(logger, times(1)).debug("Setting doubled");
		verify(logger, times(1)).debug("Original character {}", 'a');
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, times(1)).debug("Checking same as replacer");
		verify(logger, times(1)).debug("Checking same as replaced");
		verify(logger, times(1)).debug("Removing capitals");
		verify(logger, times(1)).debug("Setting doubled to {}", 'A');
	}

	@Test
	public void testSetDoubled_capital(){
		cipher.preserveCapitals = true;

		cipher.setDoubled('a');

		assertEquals('a', cipher.doubled);
		verify(logger, times(1)).debug("Setting doubled");
		verify(logger, times(1)).debug("Original character {}", 'a');
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, times(1)).debug("Checking same as replacer");
		verify(logger, times(1)).debug("Checking same as replaced");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, times(1)).debug("Setting doubled to {}", 'a');
	}

	@Test
	public void testSetDoubled_digit(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setDoubled('1');
		});

		assertEquals('X', cipher.doubled);
		verify(logger, times(1)).debug("Setting doubled");
		verify(logger, times(1)).debug("Original character {}", '1');
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, never()).debug("Checking same as replacer");
		verify(logger, never()).debug("Checking same as replaced");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug(eq("Setting doubled to {}"), anyChar());
	}

	@Test
	public void testSetDoubled_replacer(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setDoubled(cipher.replacer);
		});

		assertEquals('X', cipher.doubled);
		verify(logger, times(1)).debug("Setting doubled");
		verify(logger, times(1)).debug("Original character {}", cipher.replacer);
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, times(1)).debug("Checking same as replacer");
		verify(logger, never()).debug("Checking same as replaced");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Setting doubled to {}", 'A');
	}

	@Test
	public void testSetDoubled_replaced(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setDoubled(cipher.replaced);
		});

		assertEquals('X', cipher.doubled);
		verify(logger, times(1)).debug("Setting doubled");
		verify(logger, times(1)).debug("Original character {}", cipher.replaced);
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, times(1)).debug("Checking same as replacer");
		verify(logger, times(1)).debug("Checking same as replaced");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Setting doubled to {}", 'A');
	}

	@Test
	public void testSetReplacer(){
		cipher.setReplacer('a');

		assertEquals('A', cipher.replacer);
		verify(logger, times(1)).debug("Setting replacer");
		verify(logger, times(1)).debug("Original character {}", 'a');
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, times(1)).debug("Checking same as replaced");
		verify(logger, times(1)).debug("Checking same as doubled");
		verify(logger, times(1)).debug("Removing capitals");
		verify(logger, times(1)).debug("Setting replacer to {}", 'A');
	}

	@Test
	public void testSetReplacer_capital(){
		cipher.preserveCapitals = true;

		cipher.setReplacer('a');

		assertEquals('a', cipher.replacer);
		verify(logger, times(1)).debug("Setting replacer");
		verify(logger, times(1)).debug("Original character {}", 'a');
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, times(1)).debug("Checking same as replaced");
		verify(logger, times(1)).debug("Checking same as doubled");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, times(1)).debug("Setting replacer to {}", 'a');
	}

	@Test
	public void testSetReplacer_digital(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setReplacer('1');
		});

		assertEquals('I', cipher.replacer);
		verify(logger, times(1)).debug("Setting replacer");
		verify(logger, times(1)).debug("Original character {}", '1');
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, never()).debug("Checking same as replaced");
		verify(logger, never()).debug("Checking same as doubled");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug(eq("Setting replacer to {}"), anyChar());
	}

	@Test
	public void testSetReplacer_replaced(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setReplacer(cipher.replaced);
		});

		assertEquals('I', cipher.replacer);
		verify(logger, times(1)).debug("Setting replacer");
		verify(logger, times(1)).debug("Original character {}", cipher.replaced);
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, times(1)).debug("Checking same as replaced");
		verify(logger, never()).debug("Checking same as doubled");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug(eq("Setting replacer to {}"), anyChar());
	}

	@Test
	public void testSetReplacer_doubled(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setReplacer(cipher.doubled);
		});

		assertEquals('I', cipher.replacer);
		verify(logger, times(1)).debug("Setting replacer");
		verify(logger, times(1)).debug("Original character {}", cipher.doubled);
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, times(1)).debug("Checking same as replaced");
		verify(logger, times(1)).debug("Checking same as doubled");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug(eq("Setting replacer to {}"), anyChar());
	}

	@Test
	public void testSetReplaced(){
		cipher.setReplaced('a');

		assertEquals('A', cipher.replaced);
		verify(logger, times(1)).debug("Setting replaced");
		verify(logger, times(1)).debug("Original character {}", 'a');
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, times(1)).debug("Checking same as replacer");
		verify(logger, times(1)).debug("Checking same as doubled");
		verify(logger, times(1)).debug("Removing capitals");
		verify(logger, times(1)).debug("Setting replaced to {}", 'A');
	}

	@Test
	public void testSetReplaced_capital(){
		cipher.preserveCapitals = true;

		cipher.setReplaced('a');

		assertEquals('a', cipher.replaced);
		verify(logger, times(1)).debug("Setting replaced");
		verify(logger, times(1)).debug("Original character {}", 'a');
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, times(1)).debug("Checking same as replacer");
		verify(logger, times(1)).debug("Checking same as doubled");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, times(1)).debug("Setting replaced to {}", 'a');
	}

	@Test
	public void testSetReplaced_digital(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setReplaced('1');
		});

		assertEquals('J', cipher.replaced);
		verify(logger, times(1)).debug("Setting replaced");
		verify(logger, times(1)).debug("Original character {}", '1');
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, never()).debug("Checking same as replacer");
		verify(logger, never()).debug("Checking same as doubled");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug(eq("Setting replaced to {}"), anyChar());
	}

	@Test
	public void testSetReplaced_replacer(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setReplaced(cipher.replacer);
		});

		assertEquals('J', cipher.replaced);
		verify(logger, times(1)).debug("Setting replaced");
		verify(logger, times(1)).debug("Original character {}", cipher.replacer);
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, times(1)).debug("Checking same as replacer");
		verify(logger, never()).debug("Checking same as doubled");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug(eq("Setting replaced to {}"), anyChar());
	}

	@Test
	public void testSetReplaced_doubled(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setReplaced(cipher.doubled);
		});

		assertEquals('J', cipher.replaced);
		verify(logger, times(1)).debug("Setting replaced");
		verify(logger, times(1)).debug("Original character {}", cipher.doubled);
		verify(logger, times(1)).debug("Checking letter");
		verify(logger, times(1)).debug("Checking same as replacer");
		verify(logger, times(1)).debug("Checking same as doubled");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug(eq("Setting replaced to {}"), anyChar());
	}

	@Test
	public void testCreateGrid(){
		String expectedGridString = "[P L A Y F]\n[I R E X M]\n[B C D G H]\n[K N O Q S]\n[T U V W Z]";
		cipher.keyword = keywordClean;

		cipher.createGrid();

		assertArrayEquals(grid, cipher.grid);
		verify(logger, times(1)).debug("Creating grid from keyword");
		verify(logger, times(25)).debug(eq("Letter {} going to position [{}] [{}]"), anyChar(), anyInt(), anyInt());
		verify(logger, times(1)).debug("Grid\n{}", expectedGridString);
	}

	@Test
	public void testSetInputString(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.doubled = 'x';

		cipher.setInputString(decodedString, true);

		assertEquals(decodedStringPadded, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string");
		verify(logger, times(1)).debug("Original input string {}", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
	}

	@Test
	public void testSetInputString_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.doubled = 'X';

		cipher.setInputString(decodedString, true);

		assertEquals(decodedStringPadded.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string");
		verify(logger, times(1)).debug("Original input string {}", decodedString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
	}

	@Test
	public void testSetInputString_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;
		cipher.doubled = 'x';

		cipher.setInputString(decodedString, true);

		assertEquals(decodedStringPadded.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string");
		verify(logger, times(1)).debug("Original input string {}", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
	}

	@Test
	public void testSetInputString_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;
		cipher.doubled = 'x';

		cipher.setInputString(decodedString, true);

		assertEquals(decodedStringPadded.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string");
		verify(logger, times(1)).debug("Original input string {}", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
	}

	@Test
	public void testSetInputString_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.doubled = 'x';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString("", true);
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string");
		verify(logger, times(1)).debug("Original input string {}", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
	}

	@Test
	public void testSetInputString_decoding(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.doubled = 'x';

		cipher.setInputString(encodedString, false);

		assertEquals(encodedString, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string");
		verify(logger, times(1)).debug("Original input string {}", encodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Clean input string '{}'", encodedString);
	}

	@Test
	public void testSetInputString_decodingContainsReplaced(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.doubled = 'x';

		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setInputString(encodedString + cipher.replaced, false);
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string");
		verify(logger, times(1)).debug("Original input string {}", encodedString + Character.toString(cipher.replaced));
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug("Clean input string '{}'", encodedString);
	}

	@Test
	public void testSetInputString_decodingOddLength(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.doubled = 'x';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString(encodedString + "a", false);
		});

		assertEquals(encodedString + "a", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string");
		verify(logger, times(1)).debug("Original input string {}", encodedString + "a");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Clean input string '{}'", encodedString + "a");
	}

	@Test
	public void testSetInputString_blankClean(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.doubled = 'x';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString("**", true);
		});

		assertEquals("**", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string");
		verify(logger, times(1)).debug("Original input string {}", "**");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug("Clean input string '{}'", "**");
	}

	@Test
	public void testSetInputString_null(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.doubled = 'x';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString(null, true);
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string");
		verify(logger, never()).debug(eq("Original input string {}"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Clean input string '{}'"), anyString());
	}

	@Test
	public void testSetInputStringEncode(){
		cipher.doubled = 'x';

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedStringPadded, cipher.inputString);
		verify(logger, times(1)).debug("Cleaning up input string for encoding");
		verify(logger, times(1)).debug("Replacing all {} with {}", cipher.replaced, cipher.replacer);
		verify(logger, times(13)).debug(eq("Starting at character {}"), anyInt());
		verify(logger, times(13)).debug(eq("Adding to clean input: {} {} {} {}"), any(StringBuilder.class), anyChar(), any(StringBuilder.class), anyChar());
		verify(logger, times(1)).debug("Checking odd characters");
		verify(logger, never()).debug("Adding final character to make even");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringPadded);
	}

	@Test
	public void testSetInputStringEncode_odd(){
		cipher.doubled = 'x';

		cipher.setInputStringEncode(decodedStringPadded + 'a');

		assertEquals(decodedStringPadded + "ax", cipher.inputString);
		verify(logger, times(1)).debug("Cleaning up input string for encoding");
		verify(logger, times(1)).debug("Replacing all {} with {}", cipher.replaced, cipher.replacer);
		verify(logger, times(14)).debug(eq("Starting at character {}"), anyInt());
		verify(logger, times(14)).debug(eq("Adding to clean input: {} {} {} {}"), any(StringBuilder.class), anyChar(), any(StringBuilder.class), anyChar());
		verify(logger, times(1)).debug("Checking odd characters");
		verify(logger, times(1)).debug("Adding final character to make even");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringPadded + "ax");
	}

	@Test
	public void testSetInputString_oddEndSymbol(){
		cipher.doubled = 'x';

		cipher.setInputStringEncode(decodedStringPadded + "a*");

		assertEquals(decodedStringPadded + "a*x", cipher.inputString);
		verify(logger, times(1)).debug("Cleaning up input string for encoding");
		verify(logger, times(1)).debug("Replacing all {} with {}", cipher.replaced, cipher.replacer);
		verify(logger, times(14)).debug(eq("Starting at character {}"), anyInt());
		verify(logger, times(14)).debug(eq("Adding to clean input: {} {} {} {}"), any(StringBuilder.class), anyChar(), any(StringBuilder.class), anyChar());
		verify(logger, times(1)).debug("Checking odd characters");
		verify(logger, times(1)).debug("Adding final character to make even");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringPadded + "a*x");
	}

	@Test
	public void testSetInputString_oddEndDoubled(){
		cipher.doubled = 'x';
		cipher.replacer = 'i';

		cipher.setInputStringEncode(decodedStringPadded + 'x');

		assertEquals(decodedStringPadded + "xi", cipher.inputString);
		verify(logger, times(1)).debug("Cleaning up input string for encoding");
		verify(logger, times(1)).debug("Replacing all {} with {}", cipher.replaced, cipher.replacer);
		verify(logger, times(14)).debug(eq("Starting at character {}"), anyInt());
		verify(logger, times(14)).debug(eq("Adding to clean input: {} {} {} {}"), any(StringBuilder.class), anyChar(), any(StringBuilder.class), anyChar());
		verify(logger, times(1)).debug("Checking odd characters");
		verify(logger, times(1)).debug("Adding final character to make even");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringPadded + "xi");
	}

	@Test
	public void testGetPreparedInputString(){
		cipher.inputString = decodedStringPadded;

		String output = cipher.getPreparedInputString();
		assertEquals(decodedStringClean, output);
		verify(logger, times(1)).debug("Getting input string ready for encoding");
		verify(logger, times(1)).debug("Prepared string '{}'", decodedStringClean);
	}

	@Test
	public void testSetKeyword(){
		cipher.setKeyword(keyword);

		assertEquals(keywordClean, cipher.keyword);
		verify(logger, times(1)).debug("Setting keyword");
		verify(logger, times(1)).debug("Original keyword {}", keyword);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all non-letter characters");
		verify(logger, times(1)).debug("Appending the alphabet to the keyword");
		verify(logger, times(1)).debug("Replacing {} with {}", cipher.replaced, cipher.replacer);
		verify(logger, times(1)).debug("Removing duplicate characters");
		verify(logger, times(1)).debug("Cleaned keyword {}", keywordClean);
	}

	@Test
	public void testSetKeyword_null(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword(null);
		});

		assertEquals("", cipher.keyword);
		verify(logger, times(1)).debug("Setting keyword");
		verify(logger, never()).debug(eq("Original keyword {}"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing all non-letter characters");
		verify(logger, never()).debug("Appending the alphabet to the keyword");
		verify(logger, never()).debug(eq("Replacing {} with {}"), anyChar(), anyChar());
		verify(logger, never()).debug("Removing duplicate characters");
		verify(logger, never()).debug(eq("Cleaned keyword {}"), anyString());
	}

	@Test
	public void testFindChar(){
		cipher.grid = grid;

		CharLocation returnedLocation = cipher.findChar('E');

		assertEquals(1, returnedLocation.getX());
		assertEquals(2, returnedLocation.getY());
		verify(logger, times(1)).debug("Finding character in grid {}", 'E');
		verify(logger, times(1)).debug("Found at {}, {}", 1, 2);
	}

	@Test
	public void testFindChar_invalid(){
		cipher.grid = grid;

		assertThrows(InvalidInputException.class, () -> {
			cipher.findChar('J');
		});

		verify(logger, times(1)).debug("Finding character in grid {}", 'J');
		verify(logger, never()).debug(eq("Found at {}, {}"), anyInt(), anyInt());
	}

	@Test
	public void testGetGridChar(){
		cipher.grid = grid;

		char output = cipher.getGridChar(1, 1);

		assertEquals(grid[1][1], output);
		verify(logger, times(1)).debug("Getting character from grid[{}][{}]", 1, 1);
		verify(logger, times(1)).debug("Character {}", grid[1][1]);
	}

	@Test
	public void testGetGridChar_large(){
		cipher.grid = grid;

		char output = cipher.getGridChar(-4, -4);

		assertEquals(grid[1][1], output);
		verify(logger, times(1)).debug("Getting character from grid[{}][{}]", -4, -4);
		verify(logger, times(1)).debug("Character {}", grid[1][1]);
	}

	@Test
	public void testGetGridChar_negative(){
		cipher.grid = grid;

		char output = cipher.getGridChar(6, 6);

		assertEquals(grid[1][1], output);
		verify(logger, times(1)).debug("Getting character from grid[{}][{}]", 6, 6);
		verify(logger, times(1)).debug("Character {}", grid[1][1]);
	}

	@Test
	public void testAddCharactersToCleanString(){
		cipher.inputString = decodedStringPadded;

		cipher.addCharactersToCleanString(encodedStringClean);

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Formatting output string");
		verify(logger, times(34)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(1)).debug("Appending uppercase");
		verify(logger, times(25)).debug("Appending lowercase");
		verify(logger, times(8)).debug("Appending symbol");
		verify(logger, times(1)).debug("Formatted output '{}'", encodedString);
	}

	@Test
	public void testEncode(){
		cipher.inputString = decodedStringPadded;
		cipher.keyword = keywordClean;
		cipher.grid = grid;

		cipher.encode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(13)).debug(eq("Letters {} {}"), anyChar(), anyChar());
		verify(logger, times(2)).debug("Row encoding");
		verify(logger, times(1)).debug("Column encoding");
		verify(logger, times(10)).debug("Corner encoding");
		verify(logger, times(13)).debug(eq("Encoded letters {} {}"), anyChar(), anyChar());
	}

	@Test
	public void testDecode(){
		cipher.inputString = encodedString;
		cipher.keyword = keywordClean;
		cipher.grid = grid;

		cipher.decode();

		assertEquals(decodedStringPadded, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(13)).debug(eq("Letters {} {}"), anyChar(), anyChar());
		verify(logger, times(2)).debug("Row decoding");
		verify(logger, times(1)).debug("Column decoding");
		verify(logger, times(10)).debug("Corner decoding");
		verify(logger, times(13)).debug(eq("Decoded letters {} {}"), anyChar(), anyChar());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.keyword = keyword;
		cipher.grid = grid;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[5][5], cipher.grid);
		verify(logger, times(1)).debug("Resetting fields");
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.keyword = keyword;
		cipher.grid = grid;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(encodedString, cipher.getOutputString());
		assertEquals(keyword, cipher.getKeyword());
		assertEquals("[P L A Y F]\n[I R E X M]\n[B C D G H]\n[K N O Q S]\n[T U V W Z]", cipher.getGrid());
		assertEquals('J', cipher.getReplaced());
		assertEquals('I', cipher.getReplacer());
		assertEquals('X', cipher.getDoubled());
	}


	@Test
	public void testPracticalEncode(){
		cipher = new Playfair(true, true, true);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedStringPadded, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalEncode_clean(){
		cipher = new Playfair(false, false, false);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalDecode(){
		cipher = new Playfair(true, true, true);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(decodedStringPadded, cipher.outputString);
		assertEquals(decodedStringPadded, output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalDecode_clean(){
		cipher = new Playfair(false, false, false);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedStringClean, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
		assertArrayEquals(grid, cipher.grid);
	}
}

//CipherStreamJava/src/test/java/com/mattrixwv/CipherStreamJava/PolybiusSquareTest.java
//Mattrixwv
// Created: 01-04-22
//Modified: 04-19-24
package com.mattrixwv.cipherstream.polysubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidCharacterException;
import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;
import com.mattrixwv.cipherstream.polysubstitution.PolybiusSquare.CharLocation;


@ExtendWith(MockitoExtension.class)
public class PolybiusSquareTest{
	@InjectMocks
	private PolybiusSquare cipher;
	@Mock
	private Logger logger;
	//Fields
	private static final String decodedString = "Message to^encode";
	private static final String decodedStringClean = "M E S S A G E T O E N C O D E";
	private static final String encodedString = "41124545233212 5115^124225152212";
	private static final String encodedStringClean = "41 12 45 45 23 32 12 51 15 12 42 25 15 22 12";
	private static final String keyword = "ke yw*ord";
	private static final String keywordClean = "KEYWORDABCFGHILMNPQSTUVXZ";
	private static final char[][] grid = new char[][]{
		{'K', 'E', 'Y', 'W', 'O'},
		{'R', 'D', 'A', 'B', 'C'},
		{'F', 'G', 'H', 'I', 'L'},
		{'M', 'N', 'P', 'Q', 'S'},
		{'T', 'U', 'V', 'X', 'Z'}
	};
	private static final String gridString = "[K E Y W O]\n[R D A B C]\n[F G H I L]\n[M N P Q S]\n[T U V X Z]";


	@Test
	public void testConstructor_default(){
		cipher = new PolybiusSquare();

		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		assertArrayEquals(new char[5][5], cipher.grid);
		assertEquals('J', cipher.replaced);
		assertEquals('I', cipher.replacer);
	}

	@Test
	public void testConstructor_noWhitespace(){
		cipher = new PolybiusSquare(false, true);

		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		assertArrayEquals(new char[5][5], cipher.grid);
		assertEquals('J', cipher.replaced);
		assertEquals('I', cipher.replacer);
	}

	@Test
	public void testConstructor_noSymbols(){
		cipher = new PolybiusSquare(true, false);

		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		assertArrayEquals(new char[5][5], cipher.grid);
		assertEquals('J', cipher.replaced);
		assertEquals('I', cipher.replacer);
	}

	@Test
	public void testConstructor_newChars_noWhitespace(){
		cipher = new PolybiusSquare(false, true, 'A', 'B');

		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		assertArrayEquals(new char[5][5], cipher.grid);
		assertEquals('A', cipher.replaced);
		assertEquals('B', cipher.replacer);
	}

	@Test
	public void testConstructor_newChars_noSymbols(){
		cipher = new PolybiusSquare(true, false, 'A', 'B');

		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		assertArrayEquals(new char[5][5], cipher.grid);
		assertEquals('A', cipher.replaced);
		assertEquals('B', cipher.replacer);
	}

	@Test
	public void testSetReplaced(){
		cipher.setReplaced('a');

		assertEquals('A', cipher.replaced);
		verify(logger, times(1)).debug("Setting replaced");
		verify(logger, times(1)).debug("Original character {}", 'a');
		verify(logger, times(1)).debug("Checking replacer");
		verify(logger, times(1)).debug("Cleaned character {}", 'A');
	}

	@Test
	public void testSetReplaced_digital(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setReplaced('1');
		});

		assertEquals('J', cipher.replaced);
		verify(logger, times(1)).debug("Setting replaced");
		verify(logger, times(1)).debug("Original character {}", '1');
		verify(logger, never()).debug("Checking replacer");
		verify(logger, never()).debug(eq("Cleaned character {}"), anyChar());
	}

	@Test
	public void testSetReplaced_replacer(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setReplaced(cipher.replacer);
		});

		assertEquals('J', cipher.replaced);
		verify(logger, times(1)).debug("Setting replaced");
		verify(logger, times(1)).debug("Original character {}", cipher.replacer);
		verify(logger, times(1)).debug("Checking replacer");
		verify(logger, never()).debug(eq("Cleaned character {}"), anyChar());
	}

	@Test
	public void testSetReplacer(){
		cipher.setReplacer('a');

		assertEquals('A', cipher.replacer);
		verify(logger, times(1)).debug("Setting replacer");
		verify(logger, times(1)).debug("Original character {}", 'a');
		verify(logger, times(1)).debug("Checking replaced");
		verify(logger, times(1)).debug("Cleaned character {}", 'A');
	}

	@Test
	public void testSetReplacer_digital(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setReplacer('1');
		});

		assertEquals('I', cipher.replacer);
		verify(logger, times(1)).debug("Setting replacer");
		verify(logger, times(1)).debug("Original character {}", '1');
		verify(logger, never()).debug("Checking replaced");
		verify(logger, never()).debug(eq("Cleaned character {}"), anyChar());
	}

	@Test
	public void testSetReplacer_replaced(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setReplacer(cipher.replaced);
		});

		assertEquals('I', cipher.replacer);
		verify(logger, times(1)).debug("Setting replacer");
		verify(logger, times(1)).debug("Original character {}", cipher.replaced);
		verify(logger, times(1)).debug("Checking replaced");
		verify(logger, never()).debug(eq("Cleaned character {}"), anyChar());
	}

	@Test
	public void testCreateGrid(){
		cipher.keyword = keywordClean;

		cipher.createGrid();

		assertArrayEquals(grid, cipher.grid);
		verify(logger, times(1)).debug("Creating grid from keyword");
		verify(logger, times(1)).debug("Created grid\n{}", gridString);
	}

	@Test
	public void testSetInputStringEncode(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.replaced = 'J';
		cipher.replacer = 'I';

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedString.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", decodedString);
		verify(logger, times(1)).debug("Checking for digits");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Replacing {} with {}", cipher.replaced, cipher.replacer);
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.toUpperCase());
	}

	@Test
	public void testSetInputStringEncode_noWhitespace(){
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;
		cipher.replaced = 'J';
		cipher.replacer = 'I';

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedString.toUpperCase().replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", decodedString);
		verify(logger, times(1)).debug("Checking for digits");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Replacing {} with {}", cipher.replaced, cipher.replacer);
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.toUpperCase().replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputStringEncode_noSymbols(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;
		cipher.replaced = 'J';
		cipher.replacer = 'I';

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedString.toUpperCase().replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", decodedString);
		verify(logger, times(1)).debug("Checking for digits");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Replacing {} with {}", cipher.replaced, cipher.replacer);
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.toUpperCase().replaceAll("[^a-zA-Z\\s]", ""));
	}

	@Test
	public void testSetInputStringEncode_noWhitespaceSymbols(){
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = false;
		cipher.replaced = 'J';
		cipher.replacer = 'I';

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", decodedString);
		verify(logger, times(1)).debug("Checking for digits");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Replacing {} with {}", cipher.replaced, cipher.replacer);
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringClean);
	}

	@Test
	public void testSetInputStringEncode_digits(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.replaced = 'J';
		cipher.replacer = 'I';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode("1");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", "1");
		verify(logger, times(1)).debug("Checking for digits");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Replacing {} with {}"), anyChar(), anyChar());
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputStringEncode_blank(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.replaced = 'J';
		cipher.replacer = 'I';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", "");
		verify(logger, times(1)).debug("Checking for digits");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Replacing {} with {}", cipher.replaced, cipher.replacer);
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputStringEncode_blankClean(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.replaced = 'J';
		cipher.replacer = 'I';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode("*");
		});

		assertEquals("*", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", "*");
		verify(logger, times(1)).debug("Checking for digits");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Replacing {} with {}", cipher.replaced, cipher.replacer);
		verify(logger, times(1)).debug("Cleaned input string '{}'", "*");
	}

	@Test
	public void testSetInputStringEncode_null(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.replaced = 'J';
		cipher.replacer = 'I';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Setting input string for encoding '{}'"), anyString());
		verify(logger, never()).debug("Checking for digits");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Replacing {} with {}"), anyChar(), anyChar());
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputStringDecode(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputStringDecode(encodedString);

		assertEquals(encodedString, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", encodedString);
		verify(logger, times(1)).debug("Checking for letters");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedString);
	}

	@Test
	public void testSetInputStringDecode_noWhitespace(){
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;

		cipher.setInputStringDecode(encodedString);

		assertEquals(encodedString.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", encodedString);
		verify(logger, times(1)).debug("Checking for letters");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedString.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputStringDecode_noSymbol(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;

		cipher.setInputStringDecode(encodedString);

		assertEquals(encodedString.replaceAll("[^0-9\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", encodedString);
		verify(logger, times(1)).debug("Checking for letters");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedString.replaceAll("[^0-9\\s]", ""));
	}

	@Test
	public void testSetInputStringDecode_alpha(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode(encodedString + "a");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", encodedString + "a");
		verify(logger, times(1)).debug("Checking for letters");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputStringDecode_odd(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode(encodedString + "0");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", encodedString + "0");
		verify(logger, times(1)).debug("Checking for letters");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputStringDecode_blank(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", "");
		verify(logger, times(1)).debug("Checking for letters");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputStringDecode_cleanBlank(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode("*");
		});

		assertEquals("*", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", "*");
		verify(logger, times(1)).debug("Checking for letters");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "*");
	}

	@Test
	public void testSetInputStringDecode_null(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Setting input string for decoding '{}'"), anyString());
		verify(logger, never()).debug("Checking for letters");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testGetPreparedInputStringEncode(){
		cipher.inputString = decodedString;

		String output = cipher.getPreparedInputStringEncode();

		assertEquals(decodedStringClean.replaceAll("\\s", ""), output);
		verify(logger, times(1)).debug("Preparing input string for encoding");
		verify(logger, times(1)).debug("Prepared string '{}'", decodedStringClean.replaceAll("\\s", ""));
	}

	@Test
	public void testGetPreparedInputStringDecode(){
		cipher.inputString = encodedString;

		String output = cipher.getPreparedInputStringDecode();

		assertEquals(encodedStringClean.replaceAll("\\s", ""), output);
		verify(logger, times(1)).debug("Preparing input string for decoding");
		verify(logger, times(1)).debug("Prepared string '{}'", encodedStringClean.replaceAll("\\s", ""));
	}

	@Test
	public void testSetKeyword(){
		cipher.setKeyword(keyword);

		assertEquals(keywordClean, cipher.keyword);
		assertArrayEquals(grid, cipher.grid);
		verify(logger, times(1)).debug("Original keyword {}", keyword);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all non-letter characters");
		verify(logger, times(1)).debug("Appending entire alphabet");
		verify(logger, times(1)).debug("Replacing {} with {}", cipher.replaced, cipher.replacer);
		verify(logger, times(1)).debug("Cleaned keyword {}", keywordClean);
	}

	@Test
	public void testSetKeyword_blank(){
		char[][] gridBlank = new char[][]{
			{'A', 'B', 'C', 'D', 'E'},
			{'F', 'G', 'H', 'I', 'K'},
			{'L', 'M', 'N', 'O', 'P'},
			{'Q', 'R', 'S', 'T', 'U'},
			{'V', 'W', 'X', 'Y', 'Z'}
		};

		cipher.setKeyword("");

		assertEquals("ABCDEFGHIKLMNOPQRSTUVWXYZ", cipher.keyword);
		assertArrayEquals(gridBlank, cipher.grid);
		verify(logger, times(1)).debug("Original keyword {}", "");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all non-letter characters");
		verify(logger, times(1)).debug("Appending entire alphabet");
		verify(logger, times(1)).debug("Replacing {} with {}", cipher.replaced, cipher.replacer);
		verify(logger, times(1)).debug("Cleaned keyword {}", "ABCDEFGHIKLMNOPQRSTUVWXYZ");
	}

	@Test
	public void testSetKeyword_null(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword(null);
		});

		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[5][5], cipher.grid);
		verify(logger, never()).debug(eq("Original keyword {}"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing all non-letter characters");
		verify(logger, never()).debug("Appending entire alphabet");
		verify(logger, never()).debug(eq("Replacing {} with {}"), anyChar(), anyChar());
		verify(logger, never()).debug(eq("Cleaned keyword {}"), anyString());
	}

	@Test
	public void testFindChar(){
		cipher.grid = grid;

		CharLocation returnedLocation = cipher.findChar('A');

		assertEquals(1, returnedLocation.getX());
		assertEquals(2, returnedLocation.getY());
		verify(logger, times(1)).debug("Finding {} in grid", 'A');
		verify(logger, times(1)).debug("Found at {}, {}", 1, 2);
	}

	@Test
	public void testFindChar_invalid(){
		cipher.grid = grid;

		assertThrows(InvalidInputException.class, () -> {
			cipher.findChar(cipher.replaced);
		});

		verify(logger, times(1)).debug("Finding {} in grid", cipher.replaced);
		verify(logger, never()).debug(eq("Found at {}, {}"), anyInt(), anyInt());
	}

	@Test
	public void testAddCharactersToCleanStringEncode(){
		cipher.inputString = decodedString;

		cipher.addCharactersToCleanStringEncode(encodedStringClean.replaceAll("\\D", ""));

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Formatting output string for encoding");
		verify(logger, times(17)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(15)).debug("Adding encoded characters");
		verify(logger, times(2)).debug("Adding symbols");
		verify(logger, times(1)).debug("Formatted output '{}'", encodedString);
	}

	@Test
	public void testAddCharactersToCleanStringDecode(){
		cipher.inputString = encodedString;

		cipher.addCharactersToCleanStringDecode(decodedStringClean.replaceAll("\\s", ""));

		assertEquals(decodedString.toUpperCase(), cipher.outputString);
		verify(logger, times(1)).debug("Formatting output string for decoding");
		verify(logger, times(17)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(15)).debug("Adding decoded characters");
		verify(logger, times(2)).debug("Adding symbols");
		verify(logger, times(1)).debug("Formatted output '{}'", decodedString.toUpperCase());
	}

	@Test
	public void testEncode(){
		cipher.inputString = decodedString.toUpperCase();
		cipher.keyword = keywordClean;
		cipher.grid = grid;

		cipher.encode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(15)).debug(eq("Current working character {}"), anyChar());
		verify(logger, times(15)).debug(eq("Location {}, {}"), anyInt(), anyInt());
	}

	@Test
	public void testDecode(){
		cipher.inputString = encodedString;
		cipher.keyword = keywordClean;
		cipher.grid = grid;

		cipher.decode();

		assertEquals(decodedString.toUpperCase(), cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(15)).debug(eq("Digits to decode {} {}"), anyChar(), anyChar());
		verify(logger, times(15)).debug(eq("Decoded letter {}"), anyChar());
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.keyword = keywordClean;
		cipher.outputString = encodedString;
		cipher.grid = grid;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(keywordClean, cipher.getKeyword());
		assertEquals(encodedString, cipher.getOutputString());
		assertEquals("[K E Y W O]\n[R D A B C]\n[F G H I L]\n[M N P Q S]\n[T U V X Z]", cipher.getGrid());
		assertEquals('J', cipher.getReplaced());
		assertEquals('I', cipher.getReplacer());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.keyword = keyword;
		cipher.outputString = encodedString;
		cipher.grid = grid;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		assertArrayEquals(new char[5][5], cipher.grid);
		verify(logger, times(1)).debug("Resetting fields");
	}


	@Test
	public void testPracticalEncode(){
		cipher = new PolybiusSquare(true, true);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedString.toUpperCase(), cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalEncode_clean(){
		cipher = new PolybiusSquare(false, false);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalDecode(){
		cipher = new PolybiusSquare(true, true);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(decodedString.toUpperCase(), cipher.outputString);
		assertEquals(decodedString.toUpperCase(), output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalDecode_clean(){
		cipher = new PolybiusSquare(false, false);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedString.replaceAll("\\s", "").replaceAll("[^0-9]", ""), cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(decodedStringClean.replaceAll("\\s", ""), cipher.outputString);
		assertEquals(decodedStringClean.replaceAll("\\s", ""), output);
		assertArrayEquals(grid, cipher.grid);
	}
}

//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/polysubstitution/TrifidTest.java
//Mattrixwv
// Created: 03-03-22
//Modified: 04-19-24
package com.mattrixwv.cipherstream.polysubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidBaseException;
import com.mattrixwv.cipherstream.exceptions.InvalidCharacterException;
import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;
import com.mattrixwv.cipherstream.polysubstitution.Trifid.CharLocation;


@ExtendWith(MockitoExtension.class)
public class TrifidTest{
	@InjectMocks
	private Trifid cipher;
	@Mock
	private Logger logger;
	//Fields
	private static final String decodedString = "Message to^encode+";
	private static final String decodedStringClean = "MESSAGETOENCODE+";
	private static final String decodedStringCleanAlt = "MESSAGETOENCODE";
	private static final String encodedString = "Gqdokxy eg^ranmoqr";
	private static final String encodedStringAlt = "Gqdokpd od^ljvflf+";
	private static final String encodedString3 = "Gpjqdvd of^odlklf+";
	private static final String encodedStringClean = "GQDOKXYEGRANMOQR";
	private static final String encodedStringCleanAlt = "GQDOKPDODLJVFLF";
	private static final String encodedStringClean3 = "GPJQDVDOFODLKLF+";
	private static final String keyword = "ke yw*ord";
	private static final String keywordClean = "KEYWORDABCFGHIJLMNPQSTUVXZ+";
	private static final String keywordCleanAlt = "KEYWORDABCFGHIJLMNPQSTUVXZ=";
	private static final char[][][] grid = new char[][][]{
		{
			{'K', 'E', 'Y'},
			{'W', 'O', 'R'},
			{'D', 'A', 'B'}
		},
		{
			{'C', 'F', 'G'},
			{'H', 'I', 'J'},
			{'L', 'M', 'N'}
		},
		{
			{'P', 'Q', 'S'},
			{'T', 'U', 'V'},
			{'X', 'Z', '+'}
		}
	};
	private static final String gridString = "[K E Y]\n[W O R]\n[D A B]\n\n[C F G]\n[H I J]\n[L M N]\n\n[P Q S]\n[T U V]\n[X Z +]";
	private static final char[][][] gridAlt = new char[][][]{
		{
			{'K', 'E', 'Y'},
			{'W', 'O', 'R'},
			{'D', 'A', 'B'}
		},
		{
			{'C', 'F', 'G'},
			{'H', 'I', 'J'},
			{'L', 'M', 'N'}
		},
		{
			{'P', 'Q', 'S'},
			{'T', 'U', 'V'},
			{'X', 'Z', '='}
		}
	};


	@Test
	public void testConstructor_default(){
		cipher = new Trifid();

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals('+', cipher.fillIn);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(Integer.MAX_VALUE, cipher.groupSize);
		assertArrayEquals(new char[3][3][3], cipher.grid);
	}

	@Test
	public void testconstructor_noCapitals(){
		cipher = new Trifid(false, true, true);

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals('+', cipher.fillIn);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(Integer.MAX_VALUE, cipher.groupSize);
		assertArrayEquals(new char[3][3][3], cipher.grid);
	}

	@Test
	public void testconstructor_noWhitespace(){
		cipher = new Trifid(true, false, true);

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals('+', cipher.fillIn);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(Integer.MAX_VALUE, cipher.groupSize);
		assertArrayEquals(new char[3][3][3], cipher.grid);
	}

	@Test
	public void testconstructor_noSymbols(){
		cipher = new Trifid(true, true, false);

		assertTrue(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals('+', cipher.fillIn);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(Integer.MAX_VALUE, cipher.groupSize);
		assertArrayEquals(new char[3][3][3], cipher.grid);
	}

	@Test
	public void testconstructor_fillNoCapitals(){
		cipher = new Trifid(false, true, true, '=');

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals('=', cipher.fillIn);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(Integer.MAX_VALUE, cipher.groupSize);
		assertArrayEquals(new char[3][3][3], cipher.grid);
	}

	@Test
	public void testconstructor_fillNoWhitespace(){
		cipher = new Trifid(true, false, true, '=');

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals('=', cipher.fillIn);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(Integer.MAX_VALUE, cipher.groupSize);
		assertArrayEquals(new char[3][3][3], cipher.grid);
	}

	@Test
	public void testconstructor_fillNoSymbols(){
		cipher = new Trifid(true, true, false, '=');

		assertTrue(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals('=', cipher.fillIn);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(Integer.MAX_VALUE, cipher.groupSize);
		assertArrayEquals(new char[3][3][3], cipher.grid);
	}

	@Test
	public void testSetFillIn(){
		cipher.setFillIn('=');

		assertEquals('=', cipher.fillIn);
		verify(logger, times(1)).debug("Setting fill in {}", '=');
	}

	@Test
	public void testSetFillIn_nonPrintingLow(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setFillIn('\0');
		});

		assertEquals('+', cipher.fillIn);
		verify(logger, never()).debug(eq("Setting fill in {}"), anyChar());
	}

	@Test
	public void testSetFill_nonPrintingHigh(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setFillIn((char)127);
		});

		assertEquals('+', cipher.fillIn);
		verify(logger, never()).debug(eq("Setting fill in {}"), anyChar());
	}

	@Test
	public void testSetFillIn_letter(){
		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setFillIn('a');
		});

		assertEquals('+', cipher.fillIn);
		verify(logger, never()).debug(eq("Setting fill in {}"), anyChar());
	}

	@Test
	public void testSetKeyword(){
		cipher.setKeyword(keyword);

		assertEquals(keywordClean, cipher.keyword);
		assertArrayEquals(grid, cipher.grid);
		verify(logger, times(1)).debug("Original keyword {}", keyword);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all invalid characters");
		verify(logger, times(1)).debug("Appending entire alphabet to keyword");
		verify(logger, times(1)).debug("Removing duplicate characters");
		verify(logger, times(1)).debug("Cleaned keyword {}", keywordClean);
	}

	@Test
	public void testSetKeyword_blank(){
		char[][][] blankGrid = new char[][][]{
			{
				{'A', 'B', 'C'},
				{'D', 'E', 'F'},
				{'G', 'H', 'I'}
			},
			{
				{'J', 'K', 'L'},
				{'M', 'N', 'O'},
				{'P', 'Q', 'R'}
			},
			{
				{'S', 'T', 'U'},
				{'V', 'W', 'X'},
				{'Y', 'Z', '+'}
			}
		};
		cipher.setKeyword("");

		assertEquals("ABCDEFGHIJKLMNOPQRSTUVWXYZ+", cipher.keyword);
		assertArrayEquals(blankGrid, cipher.grid);
		verify(logger, times(1)).debug("Original keyword {}", "");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all invalid characters");
		verify(logger, times(1)).debug("Appending entire alphabet to keyword");
		verify(logger, times(1)).debug("Removing duplicate characters");
		verify(logger, times(1)).debug("Cleaned keyword {}", "ABCDEFGHIJKLMNOPQRSTUVWXYZ+");
	}
	
	@Test
	public void testSetKeyword_null(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword(null);
		});

		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[3][3][3], cipher.grid);
		verify(logger, never()).debug(eq("Original keyword {}"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing all invalid characters");
		verify(logger, never()).debug("Appending entire alphabet to keyword");
		verify(logger, never()).debug("Removing duplicate characters");
		verify(logger, never()).debug(eq("Cleaned keyword {}"), anyString());
	}

	@Test
	public void setCreateGrid(){
		cipher.keyword = keywordClean;

		cipher.createGrid();

		assertArrayEquals(grid, cipher.grid);
		verify(logger, times(1)).debug("Creating grid from keyword");
		verify(logger, times(1)).debug("Completed grid\n{}", gridString);
	}

	@Test
	public void testSetGroupSize(){
		cipher.setGroupSize(1);

		assertEquals(1, cipher.groupSize);
		verify(logger, times(1)).debug("Setting group size");
	}

	@Test
	public void testSetGroupSize_negative(){
		assertThrows(InvalidBaseException.class, () -> {
			cipher.setGroupSize(-1);
		});

		assertEquals(Integer.MAX_VALUE, cipher.groupSize);
		verify(logger, never()).debug("Setting group size");
	}

	@Test
	public void testSetInputString(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString, cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString);
	}

	@Test
	public void testSetInputString_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.toUpperCase());
	}

	@Test
	public void testSetInputString_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputString_noWhitespaceFill(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;
		cipher.fillIn = ' ';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString(decodedString);
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputString_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("[^a-zA-Z\\s+]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("[^a-zA-Z\\s+]", ""));
	}

	@Test
	public void testSetInputString_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputString_null(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Original input string '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testGetCleanInputString(){
		cipher.inputString = decodedString;

		String returnedInput = cipher.getCleanInputString();

		assertEquals(decodedStringClean, returnedInput);
		verify(logger, times(1)).debug("Cleaning input string for encoding");
	}

	@Test
	public void testFindChar(){
		cipher.grid = grid;

		CharLocation returnedLocation = cipher.findChar('G');

		assertEquals(cipher.new CharLocation(0, 2, 1).toString(), returnedLocation.toString());
		verify(logger, times(1)).debug("Finding character {} in grid", 'G');
		verify(logger, times(1)).debug("Found at {} {} {}", 1, 0, 2);
	}

	@Test
	public void testFindChar_invalid(){
		cipher.grid = grid;

		assertThrows(InvalidCharacterException.class, () -> {
			cipher.findChar('=');
		});

		verify(logger, times(1)).debug("Finding character {} in grid", '=');
		verify(logger, never()).debug(eq("Found at {} {} {}"), anyInt(), anyInt(), anyInt());
	}

	@Test
	public void testGetChar(){
		cipher.grid = grid;

		char returnedChar = cipher.getChar(cipher.new CharLocation(0, 1, 2));

		assertEquals('Q', returnedChar);
		verify(logger, times(1)).debug("Getting character at {} {} {}", 2, 0, 1);
	}

	@ParameterizedTest
	@CsvSource({
		"3, 1, 2",
		"0, 3, 2",
		"0, 1, 3"
	})
	public void testGetChar_invalidLocation(int x, int y, int z){
		cipher.grid = grid;
		CharLocation location = cipher.new CharLocation(x, y, z);

		assertThrows(InvalidCharacterException.class, () -> {
			cipher.getChar(location);
		});

		verify(logger, never()).debug(eq("Getting character at {} {} {}"), anyInt(), anyInt(), anyInt());
	}

	@Test
	public void testFormatOutput(){
		cipher.preserveCapitals = true;
		cipher.inputString = decodedString;

		cipher.formatOutput(encodedStringClean);

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Formatting output");
		verify(logger, times(18)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(1)).debug("Formatting uppercase");
		verify(logger, times(14)).debug("Formatting lowercase");
		verify(logger, times(1)).debug("Adding fillIn");
		verify(logger, times(2)).debug("Appending symbol");
		verify(logger, times(1)).debug("Formatted output '{}'", encodedString);
	}

	@Test
	public void testFormatOutput_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.inputString = decodedString.toUpperCase();

		cipher.formatOutput(encodedStringClean);

		assertEquals(encodedString.toUpperCase(), cipher.outputString);
		verify(logger, times(1)).debug("Formatting output");
		verify(logger, times(18)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(15)).debug("Formatting uppercase");
		verify(logger, never()).debug("Formatting lowercase");
		verify(logger, times(1)).debug("Adding fillIn");
		verify(logger, times(2)).debug("Appending symbol");
		verify(logger, times(1)).debug("Formatted output '{}'", encodedString.toUpperCase());
	}

	@Test
	public void testEncode(){
		cipher.preserveCapitals = true;
		cipher.inputString = decodedString;
		cipher.keyword = keywordClean;
		cipher.grid = grid;

		cipher.encode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(1)).debug("Converting letters to coordinates");
		verify(logger, times(1)).debug("Splitting locations into groups");
		verify(logger, times(1)).debug("Splitting groups into rows");
		verify(logger, times(1)).debug("Converting split locations into new locations");
		verify(logger, times(1)).debug("Converting new locations into characters");
	}

	@Test
	public void testEncode_3(){
		cipher.preserveCapitals = true;
		cipher.inputString = decodedString;
		cipher.keyword = keywordClean;
		cipher.grid = grid;
		cipher.groupSize = 3;

		cipher.encode();

		assertEquals(encodedString3, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(1)).debug("Converting letters to coordinates");
		verify(logger, times(1)).debug("Splitting locations into groups");
		verify(logger, times(1)).debug("Splitting groups into rows");
		verify(logger, times(1)).debug("Converting split locations into new locations");
		verify(logger, times(1)).debug("Converting new locations into characters");
	}

	@Test
	public void testDecode(){
		cipher.preserveCapitals = true;
		cipher.inputString = encodedString;
		cipher.keyword = keywordClean;
		cipher.grid = grid;

		cipher.decode();

		assertEquals(decodedString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(1)).debug("Converting letters to coordinates");
		verify(logger, times(1)).debug("Splitting locations into groups");
		verify(logger, times(1)).debug("Putting locations into rows");
		verify(logger, times(1)).debug("Converting locations into new locations");
		verify(logger, times(1)).debug("Converting new locations into letters");
	}

	@Test
	public void testDecode_3(){
		cipher.preserveCapitals = true;
		cipher.inputString = encodedString3;
		cipher.keyword = keywordClean;
		cipher.grid = grid;
		cipher.groupSize = 3;

		cipher.decode();

		assertEquals(decodedString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(1)).debug("Converting letters to coordinates");
		verify(logger, times(1)).debug("Splitting locations into groups");
		verify(logger, times(1)).debug("Putting locations into rows");
		verify(logger, times(6)).debug("Converting locations into new locations");
		verify(logger, times(1)).debug("Converting new locations into letters");
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.keyword = keywordClean;
		cipher.groupSize = 3;
		cipher.fillIn = '=';
		cipher.grid = grid;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(encodedString, cipher.getOutputString());
		assertEquals(keywordClean, cipher.getKeyword());
		assertEquals(3, cipher.getGroupSize());
		assertEquals('=', cipher.getFillIn());
		assertEquals(gridString, cipher.getGrid());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.keyword = keyword;
		cipher.groupSize = 3;
		cipher.grid = grid;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(Integer.MAX_VALUE, cipher.groupSize);
		assertArrayEquals(new char[3][3][3], cipher.grid);
	}

	@Test
	public void testPracticalEncoding(){
		cipher = new Trifid(true, true, true);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalEncoding_3(){
		cipher = new Trifid(true, true, true);

		String output = cipher.encode(keyword, 3, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(encodedString3, cipher.outputString);
		assertEquals(encodedString3, output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalEncoding_fill(){
		cipher = new Trifid(true, true, true, '=');

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(keywordCleanAlt, cipher.keyword);
		assertEquals(encodedStringAlt, cipher.outputString);
		assertEquals(encodedStringAlt, output);
		assertArrayEquals(gridAlt, cipher.grid);
	}

	@Test
	public void testPracticalEncoding_clean(){
		cipher = new Trifid(false, false, false);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalEncoding_clean3(){
		cipher = new Trifid(false, false, false);

		String output = cipher.encode(keyword, 3, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(encodedStringClean3, cipher.outputString);
		assertEquals(encodedStringClean3, output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalEncoding_cleanFill(){
		cipher = new Trifid(false, false, false, '=');

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedStringCleanAlt, cipher.inputString);
		assertEquals(keywordCleanAlt, cipher.keyword);
		assertEquals(encodedStringCleanAlt, cipher.outputString);
		assertEquals(encodedStringCleanAlt, output);
		assertArrayEquals(gridAlt, cipher.grid);
	}

	@Test
	public void testPracticalDecoding(){
		cipher = new Trifid(true, true, true);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalDecoding_3(){
		cipher = new Trifid(true, true, true);

		String output = cipher.decode(keyword, 3, encodedString3);

		assertEquals(encodedString3, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalDecoding_fill(){
		cipher = new Trifid(true, true, true, '=');

		String output = cipher.decode(keyword, encodedStringAlt);

		assertEquals(encodedStringAlt, cipher.inputString);
		assertEquals(keywordCleanAlt, cipher.keyword);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
		assertArrayEquals(gridAlt, cipher.grid);
	}

	@Test
	public void testpracticalDecoding_clean(){
		cipher = new Trifid(false, false, false);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedStringClean, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalDecoding_clean3(){
		cipher = new Trifid(false, false, false);

		String output = cipher.decode(keyword, 3, encodedString3);

		assertEquals(encodedStringClean3, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
		assertArrayEquals(grid, cipher.grid);
	}

	@Test
	public void testPracticalDecoding_cleanFill(){
		cipher = new Trifid(false, false, false, '=');

		String output = cipher.decode(keyword, encodedStringAlt);

		assertEquals(encodedStringCleanAlt, cipher.inputString);
		assertEquals(keywordCleanAlt, cipher.keyword);
		assertEquals(decodedStringCleanAlt, cipher.outputString);
		assertEquals(decodedStringCleanAlt, output);
		assertArrayEquals(gridAlt, cipher.grid);
	}
}

//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/polysubstitution/HillTest.java
//Mattrixwv
// Created: 01-31-22
//Modified: 04-19-24
package com.mattrixwv.cipherstream.polysubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidCharacterException;
import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeyException;
import com.mattrixwv.matrix.ModMatrix;


@ExtendWith(MockitoExtension.class)
public class HillTest{
	@InjectMocks
	private Hill cipher;
	@Mock
	private Logger logger;
	//Fields
	private static final String decodedString = "Message to^encoded";
	private static final String decodedStringPadded = "Message to^encodedxx";
	private static final String decodedStringClean = "MESSAGETOENCODEDXX";
	private static final String encodedString = "Mgkeqge ul^ikhisplrd";
	private static final String encodedStringClean = "MGKEQGEULIKHISPLRD";
	private static final int[][] keyArray = new int[][]{{1, 4, 2}, {2, 4, 1}, {4, 1, 2}};
	private static final ModMatrix key = new ModMatrix(keyArray, 26);


	@Test
	public void testConstructor_default(){
		cipher = new Hill();

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals('X', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals(new ModMatrix(26), cipher.key);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testConstructorShort_noCapitals(){
		cipher = new Hill(false, true, true);

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals('X', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals(new ModMatrix(26), cipher.key);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testConstructorShort_noWhitespace(){
		cipher = new Hill(true, false, true);

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals('x', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals(new ModMatrix(26), cipher.key);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testConstructorShort_noSymbols(){
		cipher = new Hill(true, true, false);

		assertTrue(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals('x', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals(new ModMatrix(26), cipher.key);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testConstructorLong_noCapitals(){
		cipher = new Hill(false, true, true, 'j');

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals('J', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals(new ModMatrix(26), cipher.key);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testConstructorLong_noWhitespace(){
		cipher = new Hill(true, false, true, 'j');

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals('j', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals(new ModMatrix(26), cipher.key);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testConstructorLong_noSymbols(){
		cipher = new Hill(true, true, false, 'j');

		assertTrue(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals('j', cipher.characterToAdd);
		assertEquals("", cipher.inputString);
		assertEquals(new ModMatrix(26), cipher.key);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testSetKey(){
		cipher.setKey(key);

		assertEquals(key, cipher.key);
		verify(logger, times(1)).debug("Setting key");
		verify(logger, times(1)).debug("Testing mod");
		verify(logger, times(1)).debug("Testing square");
		verify(logger, times(1)).debug("Testing invertable");
		verify(logger, times(1)).debug("key\n{}", key);
	}

	@Test
	public void testSetKey_invalidMod(){
		ModMatrix matrix = new ModMatrix(keyArray, 30);

		assertThrows(InvalidKeyException.class, () -> {
			cipher.setKey(matrix);
		});

		assertEquals(new ModMatrix(26), cipher.key);
		verify(logger, times(1)).debug("Setting key");
		verify(logger, times(1)).debug("Testing mod");
		verify(logger, never()).debug("Testing square");
		verify(logger, never()).debug("Testing invertable");
		verify(logger, never()).debug(eq("key\n{}"), any(ModMatrix.class));
	}

	@Test
	public void testSetKey_NotSquare(){
		ModMatrix matrix = new ModMatrix(new int[][]{{10, 11}}, 26);

		assertThrows(InvalidKeyException.class, () -> {
			cipher.setKey(matrix);
		});

		assertEquals(new ModMatrix(26), cipher.key);
		verify(logger, times(1)).debug("Setting key");
		verify(logger, times(1)).debug("Testing mod");
		verify(logger, times(1)).debug("Testing square");
		verify(logger, never()).debug("Testing invertable");
		verify(logger, never()).debug(eq("key\n{}"), any(ModMatrix.class));
	}

	@Test
	public void testSetKey_notInvertable(){
		ModMatrix matrix = new ModMatrix(new int[][]{{10, 11}, {12, 13}}, 26);

		assertThrows(InvalidKeyException.class, () -> {
			cipher.setKey(matrix);
		});

		assertEquals(new ModMatrix(26), cipher.key);
		verify(logger, times(1)).debug("Setting key");
		verify(logger, times(1)).debug("Testing mod");
		verify(logger, times(1)).debug("Testing square");
		verify(logger, times(1)).debug("Testing invertable");
		verify(logger, never()).debug(eq("key\n{}"), any(ModMatrix.class));
	}

	@Test
	public void testSetInputStringEncode(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.key = key;
		cipher.characterToAdd = 'x';

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedStringPadded, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Checking length");
		verify(logger, times(1)).debug("Adding {} characters", 2);
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringPadded);
	}

	@Test
	public void testSetInputStringEncode_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.key = key;
		cipher.characterToAdd = 'X';

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedStringPadded.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, times(1)).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Checking length");
		verify(logger, times(1)).debug("Adding {} characters", 2);
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringPadded.toUpperCase());
	}

	@Test
	public void testSetInputStringEncode_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;
		cipher.key = key;
		cipher.characterToAdd = 'x';

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedStringPadded.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing capitals");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Checking length");
		verify(logger, times(1)).debug("Adding {} characters", 2);
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringPadded.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputStringEncode_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;
		cipher.key = key;
		cipher.characterToAdd = 'x';

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedStringPadded.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Checking length");
		verify(logger, times(1)).debug("Adding {} characters", 2);
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringPadded.replaceAll("[^a-zA-Z\\s]", ""));
	}

	@Test
	public void testSetInputStringEncode_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.key = key;
		cipher.characterToAdd = 'x';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Checking length");
		verify(logger, times(1)).debug("Adding {} characters", 0);
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputStringEncode_blankClean(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.key = key;
		cipher.characterToAdd = 'x';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode("*");
		});

		assertEquals("*", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, times(1)).debug("Original input string '{}'", "*");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Checking length");
		verify(logger, times(1)).debug("Adding {} characters", 0);
		verify(logger, times(1)).debug("Cleaned input string '{}'", "*");
	}

	@Test
	public void testSetInputStringEncode_null(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.key = key;
		cipher.characterToAdd = 'x';

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding");
		verify(logger, never()).debug(eq("Original input string '{}'"), anyString());
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug("Checking length");
		verify(logger, never()).debug(eq("Adding {} characters"), anyInt());
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputStringDecode(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.key = key;

		cipher.setInputStringDecode(encodedString);

		assertEquals(encodedString, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", encodedString);
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedString);
		verify(logger, times(1)).debug("Checking length");
	}

	@Test
	public void testSetInputStringDecode_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.key = key;

		cipher.setInputStringDecode(encodedString);

		assertEquals(encodedString.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", encodedString);
		verify(logger, times(1)).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedString.toUpperCase());
		verify(logger, times(1)).debug("Checking length");
	}

	@Test
	public void testSetInputStringDecode_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;
		cipher.key = key;

		cipher.setInputStringDecode(encodedString);

		assertEquals(encodedString.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", encodedString);
		verify(logger, never()).debug("Removing capitals");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedString.replaceAll("\\s", ""));
		verify(logger, times(1)).debug("Checking length");
	}

	@Test
	public void testSetInputStringDecode_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;
		cipher.key = key;

		cipher.setInputStringDecode(encodedString);

		assertEquals(encodedString.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", encodedString);
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedString.replaceAll("[^a-zA-Z\\s]", ""));
		verify(logger, times(1)).debug("Checking length");
	}

	@Test
	public void testSetInputStringDecode_notMultiple(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.key = key;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode(encodedString + "a");
		});

		assertEquals(encodedString + "a", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", encodedString + "a");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedString + "a");
		verify(logger, times(1)).debug("Checking length");
	}

	@Test
	public void testSetInputStringDecode_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.key = key;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
		verify(logger, times(1)).debug("Checking length");
	}

	@Test
	public void testSetInputStringDecode_blankClean(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.key = key;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode("*^&");
		});

		assertEquals("*^&", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, times(1)).debug("Original input string '{}'", "*^&");
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "*^&");
		verify(logger, times(1)).debug("Checking length");
	}

	@Test
	public void testSetInputStringDecode_null(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.key = key;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding");
		verify(logger, never()).debug(eq("Original input string '{}'"), anyString());
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
		verify(logger, never()).debug("Checking length");
	}

	@Test
	public void testGetCleanInputString(){
		cipher.inputString = decodedStringPadded;

		String output = cipher.getCleanInputString();

		assertEquals(decodedStringClean, output);
		verify(logger, times(1)).debug("Cleaning inputString");
		verify(logger, times(1)).debug("Clean input string '{}'", decodedStringClean);
	}

	@Test
	public void testSetCharacterToAdd(){
		cipher.preserveCapitals = true;

		cipher.setCharacterToAdd('j');

		assertEquals('j', cipher.characterToAdd);
		verify(logger, times(1)).debug("Setting character to add {}", 'j');
		verify(logger, never()).debug("Removing capitals");
		verify(logger, times(1)).debug("Cleaned character {}", 'j');
	}

	@Test
	public void testSetCharacterToAdd_noCapitals(){
		cipher.preserveCapitals = false;

		cipher.setCharacterToAdd('j');

		assertEquals('J', cipher.characterToAdd);
		verify(logger, times(1)).debug("Setting character to add {}", 'j');
		verify(logger, times(1)).debug("Removing capitals");
		verify(logger, times(1)).debug("Cleaned character {}", 'J');
	}

	@Test
	public void testSetCharacterToAdd_nonAlpha(){
		cipher.preserveCapitals = true;

		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setCharacterToAdd('1');
		});

		assertEquals('X', cipher.characterToAdd);
		verify(logger, times(1)).debug("Setting character to add {}", '1');
		verify(logger, never()).debug("Removing capitals");
		verify(logger, never()).debug(eq("Cleaned character {}"), anyChar());
	}

	@Test
	public void testPolishOutputString(){
		cipher.inputString = decodedStringPadded;
		cipher.outputString = encodedStringClean;

		String output = cipher.polishOutputString();

		assertEquals(encodedString, output);
		verify(logger, times(1)).debug("Polishing output string");
		verify(logger, times(20)).debug(eq("Current char {}"), anyChar());
		verify(logger, times(1)).debug("Uppercase");
		verify(logger, times(17)).debug("Lowercase");
		verify(logger, times(2)).debug("Symbol");
		verify(logger, times(1)).debug("Polished string '{}'", encodedString);
	}

	@Test
	public void testGetInputVectors(){
		ArrayList<ModMatrix> expectedVectors = new ArrayList<>();
		expectedVectors.add(new ModMatrix(new int[][]{{12}, { 4}, {18}}, 26));
		expectedVectors.add(new ModMatrix(new int[][]{{18}, { 0}, { 6}}, 26));
		expectedVectors.add(new ModMatrix(new int[][]{{ 4}, {19}, {14}}, 26));
		expectedVectors.add(new ModMatrix(new int[][]{{ 4}, {13}, { 2}}, 26));
		expectedVectors.add(new ModMatrix(new int[][]{{14}, { 3}, { 4}}, 26));
		expectedVectors.add(new ModMatrix(new int[][]{{ 3}, {23}, {23}}, 26));

		cipher.inputString = decodedStringPadded;
		cipher.key = key;

		ArrayList<ModMatrix> returnedVectors = cipher.getInputVectors();

		assertEquals(expectedVectors, returnedVectors);
		verify(logger, times(1)).debug("Generating input vectors");
		verify(logger, times(6)).debug(eq("Current substring '{}'"), anyString());
		verify(logger, times(6)).debug(eq("Current vector {}"), any(ModMatrix.class));
	}

	@Test
	public void testGetOutputFromVectors(){
		ArrayList<ModMatrix> outputVectors = new ArrayList<>();
		outputVectors.add(key.multiply(new ModMatrix(new int[][]{{12}, { 4}, {18}}, 26)));
		outputVectors.add(key.multiply(new ModMatrix(new int[][]{{18}, { 0}, { 6}}, 26)));
		outputVectors.add(key.multiply(new ModMatrix(new int[][]{{ 4}, {19}, {14}}, 26)));
		outputVectors.add(key.multiply(new ModMatrix(new int[][]{{ 4}, {13}, { 2}}, 26)));
		outputVectors.add(key.multiply(new ModMatrix(new int[][]{{14}, { 3}, { 4}}, 26)));
		outputVectors.add(key.multiply(new ModMatrix(new int[][]{{ 3}, {23}, {23}}, 26)));

		String returnedString = cipher.getOutputFromVectors(outputVectors);

		assertEquals(encodedStringClean, returnedString);
		verify(logger, times(1)).debug("Turning vectors into a string");
		verify(logger, times(6)).debug(eq("Current vector {}"), any(ModMatrix.class));
		verify(logger, times(1)).debug("Converted string '{}'", encodedStringClean);
	}

	@Test
	public void testEncode(){
		cipher.inputString = decodedStringPadded;
		cipher.key = key;

		cipher.encode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(1)).debug("Multiplying vectors");
		verify(logger, times(6)).debug(eq("Current input vector {}"), any(ModMatrix.class));
		verify(logger, times(6)).debug(eq("Multiplied vector {}"), any(ModMatrix.class));
	}

	@Test
	public void testDecode(){
		cipher.inputString = encodedString;
		cipher.key = key;

		cipher.decode();

		assertEquals(decodedStringPadded, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(1)).debug("Getting inverse of key");
		verify(logger, times(1)).debug("Inverse of key {}", key.inverse());
		verify(logger, times(6)).debug(eq("Current input vector {}"), any(ModMatrix.class));
		verify(logger, times(6)).debug(eq("Multiplied vector {}"), any(ModMatrix.class));
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.key = key;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(encodedString, cipher.getOutputString());
		assertEquals(key, cipher.getKey());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.key = key;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals(new ModMatrix(26), cipher.key);
		verify(logger, times(1)).debug("Resetting fields");
	}

	@Test
	public void testPracticalEncode(){
		cipher = new Hill(true, true, true);

		String output = cipher.encode(key, decodedString);

		assertEquals(decodedStringPadded, cipher.inputString);
		assertEquals(key, cipher.key);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalEncode_clean(){
		cipher = new Hill(false, false, false);

		String output = cipher.encode(key, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(key, cipher.key);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
	}

	@Test
	public void testPracticalEncode_array(){
		cipher = new Hill(true, true, true);

		String output = cipher.encode(keyArray, decodedString);

		assertEquals(decodedStringPadded, cipher.inputString);
		assertEquals(key, cipher.key);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalEncode_padding(){
		cipher = new Hill(true, true, true, 'j');

		String output = cipher.encode(key, decodedString);

		assertEquals(decodedString + "jj", cipher.inputString);
		assertEquals(key, cipher.key);
		assertEquals("Mgkeqge ul^ikhispfzn", cipher.outputString);
		assertEquals("Mgkeqge ul^ikhispfzn", output);
	}

	@Test
	public void testPracticalDecode(){
		cipher = new Hill(true, true, true);

		String output = cipher.decode(key, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(key, cipher.key);
		assertEquals(decodedStringPadded, cipher.outputString);
		assertEquals(decodedStringPadded, output);
	}

	@Test
	public void testPracticalDecode_clean(){
		cipher = new Hill(false, false, false);

		String output = cipher.decode(key, encodedString);

		assertEquals(encodedStringClean, cipher.inputString);
		assertEquals(key, cipher.key);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}

	@Test
	public void testPracticalDecode_array(){
		cipher = new Hill(true, true, true);

		String output = cipher.decode(keyArray, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(key, cipher.key);
		assertEquals(decodedStringPadded, cipher.outputString);
		assertEquals(decodedStringPadded, output);
	}

	@Test
	public void testPracticalDecode_padding(){
		cipher = new Hill(true, true, true, 'j');

		String output = cipher.decode(key, "Mgkeqge ul^ikhispfzn");

		assertEquals("Mgkeqge ul^ikhispfzn", cipher.inputString);
		assertEquals(key, cipher.key);
		assertEquals(decodedString + "jj", cipher.outputString);
		assertEquals(decodedString + "jj", output);
	}
}

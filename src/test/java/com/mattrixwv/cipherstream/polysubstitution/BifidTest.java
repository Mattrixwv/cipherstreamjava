//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/polysubstitution/BifidTest.java
//Mattrixwv
// Created: 03-03-22
//Modified: 04-19-24
package com.mattrixwv.cipherstream.polysubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;


@ExtendWith(MockitoExtension.class)
public class BifidTest{
	@InjectMocks
	private Bifid cipher;
	@Mock
	private Logger logger;
	//Test
	private static final String inputString = "Message to^encode";
	private static final String inputStringClean = "MESSAGETOENCODE";
	private static final String outputString = "Mqaokne kc^vdodzd";
	private static final String outputStringClean = "MQAOKNEKCVDODZD";
	private static final String keyword = "keyword";
	private static final String keywordClean = "KEYWORDABCFGHILMNPQSTUVXZ";


	@Test
	public void testConstructor_default(){
		cipher = new Bifid();

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertNotNull(cipher.polybiusSquare);
		assertFalse(cipher.polybiusSquare.preserveWhitespace);
		assertFalse(cipher.polybiusSquare.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testConstructor_preservesCapitals(){
		cipher = new Bifid(true, false, false);

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertNotNull(cipher.polybiusSquare);
		assertFalse(cipher.polybiusSquare.preserveWhitespace);
		assertFalse(cipher.polybiusSquare.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testConstructor_preservesWhitespace(){
		cipher = new Bifid(false, true, false);

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertNotNull(cipher.polybiusSquare);
		assertFalse(cipher.polybiusSquare.preserveWhitespace);
		assertFalse(cipher.polybiusSquare.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testConstructor_preservesSymbols(){
		cipher = new Bifid(false, false, true);

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertNotNull(cipher.polybiusSquare);
		assertFalse(cipher.polybiusSquare.preserveWhitespace);
		assertFalse(cipher.polybiusSquare.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testSetKeyword(){
		cipher.setKeyword(keyword);

		assertEquals(keyword, cipher.keyword);
		verify(logger, times(1)).debug("Setting keyword '{}'", keyword);
	}

	@Test
	public void testSetKeyword_null(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword(null);
		});

		verify(logger, never()).debug(eq("Setting keyword '{}'"), anyString());
	}

	@Test
	public void testSetInputString(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(inputString);

		assertEquals(inputString, cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", inputString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", inputString);
	}

	@Test
	public void testSetInputString_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(inputString);

		assertEquals(inputString.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", inputString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", inputString.toUpperCase());
	}

	@Test
	public void testSetInputString_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;

		cipher.setInputString(inputString);

		assertEquals(inputString.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", inputString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", inputString.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputString_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;

		cipher.setInputString(inputString);

		assertEquals(inputString.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", inputString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", inputString.replaceAll("[^a-zA-Z\\s]", ""));
	}

	@Test
	public void testSetInputString_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputString_null(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Original input string '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testFormatOutput(){
		cipher.inputString = inputString;

		cipher.formatOutput(outputStringClean);

		assertEquals(outputString, cipher.outputString);
		verify(logger, times(1)).debug("Formatting output");
		verify(logger, times(17)).debug(eq("Current character {}"), anyChar());
		verify(logger, times(1)).debug("Altering uppercase");
		verify(logger, times(14)).debug("Altering lowercase");
		verify(logger, times(2)).debug("Adding symbol");
		verify(logger, times(1)).debug("Formatted output string '{}'", outputString);
	}

	@Test
	public void testEncode(){
		cipher.inputString = inputString;
		cipher.keyword = keyword;

		cipher.encode();

		assertEquals(inputString, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(outputString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(1)).debug("Encoding Polybius");
		verify(logger, times(1)).debug("Splitting Polybius Square message");
		verify(logger, times(30)).debug(eq("Current character '{}'"), anyChar());
		verify(logger, times(1)).debug("Decoding Polybius Square");
	}

	@Test
	public void testDecode(){
		cipher.inputString = outputString;
		cipher.keyword = keyword;

		cipher.decode();

		assertEquals(outputString, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(inputString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(1)).debug("Encoding Polybius Square");
		verify(logger, times(1)).debug("Splitting Polybius Square message");
		verify(logger, times(15)).debug(eq("Current characters {} {}"), anyChar(), anyChar());
		verify(logger, times(1)).debug("Decoding Polybius Square");
	}

	@Test
	public void testGetters(){
		cipher.inputString = inputString;
		cipher.keyword = keyword;
		cipher.outputString = outputString;

		assertEquals(inputString, cipher.getInputString());
		assertEquals(keyword, cipher.getKeyword());
		assertEquals(outputString, cipher.getOutputString());
	}

	@Test
	public void testReset(){
		cipher.inputString = inputString;
		cipher.keyword = keyword;
		cipher.outputString = outputString;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		verify(logger, times(1)).debug("Resetting fields");
	}

	@Test
	public void testPracticalEncoding(){
		cipher = new Bifid(true, true, true);

		String output = cipher.encode(keyword, inputString);

		assertEquals(inputString, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(outputString, cipher.outputString);
		assertEquals(output, cipher.outputString);
	}

	@Test
	public void testPracticalEncoding_clean(){
		cipher = new Bifid(false, false, false);

		String output = cipher.encode(keyword, inputString);

		assertEquals(inputStringClean, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(outputStringClean, cipher.outputString);
		assertEquals(outputStringClean, output);
	}

	@Test
	public void testPracticalDecoding(){
		cipher = new Bifid(true, true, true);

		String output = cipher.decode(keyword, outputString);

		assertEquals(outputString, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(inputString, cipher.outputString);
		assertEquals(inputString, output);
	}

	@Test
	public void testPracticalDecoding_clean(){
		cipher = new Bifid(false, false, false);

		String output = cipher.decode(keyword, outputString);

		assertEquals(outputStringClean, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(inputStringClean, cipher.outputString);
		assertEquals(inputStringClean, output);
	}
}

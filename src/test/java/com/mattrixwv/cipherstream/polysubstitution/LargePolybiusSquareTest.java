//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/polysubstitution/LargePolybiusSquareTest.java
//Mattrixwv
// Created: 04-21-23
//Modified: 04-19-24
package com.mattrixwv.cipherstream.polysubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;


@ExtendWith(MockitoExtension.class)
public class LargePolybiusSquareTest{
	@InjectMocks
	private LargePolybiusSquare cipher;
	@Mock(name = "com.mattrixwv.cipherstream.polysubstitution.LargePolybiusSquare")
	private Logger logger;
	//Variables
	private static final String decodedString = "Message to^encode";
	private static final String decodedStringClean = "MESSAGETOENCODE";
	private static final String encodedString = "35124343222612 4415^123624152112";
	private static final String encodedStringClean = "31 15 41 41 11 21 15 42 33 15 32 13 33 14 15";
	private static final String keyword = "ke yw*ord";
	private static final String keywordClean = "KEYWORDABCFGHIJLMNPQSTUVXZ0123456789";
	private static final String keywordBlank = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	private static final char[][] grid = {
		{'K', 'E', 'Y', 'W', 'O', 'R'},
		{'D', 'A', 'B', 'C', 'F', 'G'},
		{'H', 'I', 'J', 'L', 'M', 'N'},
		{'P', 'Q', 'S', 'T', 'U', 'V'},
		{'X', 'Z', '0', '1', '2', '3'},
		{'4', '5', '6', '7', '8', '9'}
	};
	private static final char[][] gridClean = {
		{'A', 'B', 'C', 'D', 'E', 'F'},
		{'G', 'H', 'I', 'J', 'K', 'L'},
		{'M', 'N', 'O', 'P', 'Q', 'R'},
		{'S', 'T', 'U', 'V', 'W', 'X'},
		{'Y', 'Z', '0', '1', '2', '3'},
		{'4', '5', '6', '7', '8', '9'},
	};


	@Test
	public void testConstructor_default(){
		cipher = new LargePolybiusSquare();

		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[6][6], cipher.grid);
	}

	@Test
	public void testConstructor_preserveWhitespace(){
		cipher = new LargePolybiusSquare(true, false);

		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[6][6], cipher.grid);
	}

	@Test
	public void testConstructor_preserveSymbols(){
		cipher = new LargePolybiusSquare(false, true);

		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[6][6], cipher.grid);
	}

	@Test
	public void testCreateGrid(){
		cipher.keyword = keywordClean;

		cipher.createGrid();

		assertArrayEquals(grid, cipher.grid);
		verify(logger, times(1)).debug("Creating grid");
	}

	@Test
	public void testSetInputStringEncoding(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedString.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.toUpperCase());
	}

	@Test
	public void testSetInputStringEncoding_noWhitespace(){
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedString.toUpperCase().replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.toUpperCase().replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputStringEncoding_noSymbols(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedString.toUpperCase().replaceAll("[^a-zA-Z0-9\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.toUpperCase().replaceAll("[^a-zA-Z0-9\\s]", ""));
	}

	@Test
	public void testSetInputStringEncoding_noWhitespaceNoSymbols(){
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = false;

		cipher.setInputStringEncode(decodedString);

		assertEquals("M E S S A G E T O E N C O D E", cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "M E S S A G E T O E N C O D E");
	}

	@Test
	public void testSetInputStringEncoding_preparedBlank(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, ()  -> {
			cipher.setInputStringEncode("*");
		});

		assertEquals("*", cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", "*");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "*");
	}

	@Test
	public void testSetInputStringEncoding_blank(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputStringEncoding_null(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Original input string '{}'"), anyString());
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testGetPreparedInputStringEncoding(){
		cipher.inputString = decodedString.toUpperCase();

		String preparedInputString = cipher.getPreparedInputStringEncode();

		assertEquals(decodedString.toUpperCase(), cipher.inputString);
		assertEquals(decodedString.toUpperCase().replaceAll("[^A-Z0-9]", ""), preparedInputString);
		verify(logger, times(1)).debug("Preparing input string for encoding");
		verify(logger, times(1)).debug("Prepared input string '{}'", preparedInputString);
	}

	@Test
	public void testSetKeyword(){
		cipher.setKeyword(keyword);

		assertEquals(keywordClean, cipher.keyword);
		assertArrayEquals(grid, cipher.grid);
		verify(logger, times(1)).debug("Original keyword '{}'", keyword);
		verify(logger, times(1)).debug("Cleaned keyword '{}'", keywordClean);
	}

	@Test
	public void testSetKeyword_blank(){
		cipher.setKeyword("");

		assertEquals(keywordBlank, cipher.keyword);
		assertArrayEquals(gridClean, cipher.grid);
		verify(logger, times(1)).debug("Original keyword '{}'", "");
		verify(logger, times(1)).debug("Cleaned keyword '{}'", keywordBlank);
	}

	@Test
	public void testSetKeyword_null(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword(null);
		});

		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[6][6], cipher.grid);
		verify(logger, never()).debug(eq("Original keyword '{}'"), anyString());
		verify(logger, never()).debug(eq("Cleaned keyword '{}'"), anyString());
	}

	@Test
	public void testAddCharactersToCleanStringEncode(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.grid = grid;
		cipher.inputString = decodedString.toUpperCase();

		cipher.addCharactersToCleanStringEncode(encodedString.replaceAll("\\s", "").replaceAll("[^0-9]", ""));

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Formatting output string");
		verify(logger, times(17)).debug(eq("Current character {}"), anyChar());
		verify(logger, times(15)).debug("Appending character");
		verify(logger, times(2)).debug("Appending symbol");
		verify(logger, times(1)).debug("Saving output string {}", encodedString);
	}

	@Test
	public void testAddCharactersToCleanStringDecode(){
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.grid = grid;
		cipher.inputString = encodedString;

		cipher.addCharactersToCleanStringDecode(decodedStringClean);

		verify(logger, times(1)).debug("Formatting output string");
		verify(logger, times(17)).debug(eq("Current character {}"), anyChar());
		verify(logger, times(15)).debug("Appending character");
		verify(logger, times(2)).debug("Appending symbol");
		verify(logger, times(1)).debug("Saving output string {}", decodedString.toUpperCase());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.keyword = keyword;
		cipher.grid = grid;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertArrayEquals(new char[6][6], cipher.grid);
		verify(logger, times(1)).debug("Resetting");
	}


	@Test
	public void testPracticalEncoding(){
		cipher = new LargePolybiusSquare(true, true);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedString.toUpperCase(), cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalEncoding_clean(){
		cipher = new LargePolybiusSquare(false, false);

		String output = cipher.encode(decodedString);

		assertEquals("M E S S A G E T O E N C O D E", cipher.inputString);
		assertEquals(keywordBlank, cipher.keyword);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
	}

	@Test
	public void testPracticalDecoding(){
		cipher = new LargePolybiusSquare(true, true);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(decodedString.toUpperCase(), cipher.outputString);
		assertEquals(decodedString.toUpperCase(), output);
	}

	@Test
	public void testPracticalDecoding_clean(){
		cipher = new LargePolybiusSquare(false, false);

		String output = cipher.decode(encodedStringClean);

		assertEquals(encodedStringClean.replaceAll("\\s", ""), cipher.inputString);
		assertEquals(keywordBlank, cipher.keyword);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}
}
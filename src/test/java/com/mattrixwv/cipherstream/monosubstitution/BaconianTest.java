//CipherStreamJava/src/test/java/com/mattrixwv/CipherStreamJava/BaconianTest.java
//Mattrixwv
// Created: 01-12-22
//Modified: 04-19-24
package com.mattrixwv.cipherstream.monosubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidCharacterException;
import com.mattrixwv.cipherstream.exceptions.InvalidInputException;


@ExtendWith(MockitoExtension.class)
public class BaconianTest{
	@InjectMocks
	private Baconian cipher;
	@Mock
	private Logger logger;
	//Variables
	private static final String decodedString = "Message to-encode";
	private static final String decodedStringClean = "Messagetoencode";
	private static final String decodedStringCleanLower = "messagetoencode";
	private static final String encodedString = "ABABB aabaa baaab baaab aaaaa aabba aabaa baaba abbab aabaa abbaa aaaba abbab aaabb aabaa";


	@Test
	public void testConstructor_default(){
		cipher = new Baconian();

		assertFalse(cipher.preserveCapitals);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testConstructor_preservesCapitals(){
		cipher = new Baconian(true);

		assertTrue(cipher.preserveCapitals);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testSetInputStringEncode(){
		cipher.preserveCapitals = true;

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringClean);
	}

	@Test
	public void testSetInputStringEncode_noCapitals(){
		cipher.preserveCapitals = false;

		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedStringCleanLower, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", decodedString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedStringCleanLower);
	}

	@Test
	public void testSetInputStringEncode_blank(){
		cipher.preserveCapitals = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputStringEncode_null(){
		cipher.preserveCapitals = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug("Setting input string for encoding '{}'", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputStringDecode(){
		cipher.preserveCapitals = true;

		cipher.setInputStringDecode(encodedString);

		assertEquals(encodedString, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", encodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Ensuring all 'letters' contain 5 characters");
		verify(logger, times(15)).debug(eq("Current 'letter' {}"), anyString());
		verify(logger, times(15)).debug("Replacing all non-abAB characters");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedString);
	}

	@Test
	public void testSetInputStringDecode_noCapitals(){
		cipher.preserveCapitals = false;

		cipher.setInputStringDecode(encodedString);

		assertEquals(encodedString.toLowerCase(), cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", encodedString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Ensuring all 'letters' contain 5 characters");
		verify(logger, times(15)).debug(eq("Current 'letter' {}"), anyString());
		verify(logger, times(15)).debug("Replacing all non-abAB characters");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedString.toLowerCase());
	}

	@Test
	public void testSetInputStringDecode_invalidLength(){
		cipher.preserveCapitals = true;

		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setInputStringDecode("a");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", "a");
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Ensuring all 'letters' contain 5 characters");
		verify(logger, times(1)).debug(eq("Current 'letter' {}"), anyString());
		verify(logger, never()).debug("Replacing all non-abAB characters");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputStringDecode_invalidChars(){
		cipher.preserveCapitals = true;

		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setInputStringDecode("ccccc");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", "ccccc");
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Ensuring all 'letters' contain 5 characters");
		verify(logger, times(1)).debug("Current 'letter' {}", "ccccc");
		verify(logger, times(1)).debug("Replacing all non-abAB characters");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputStringDecode_blank(){
		cipher.preserveCapitals = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Setting input string for decoding '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Ensuring all 'letters' contain 5 characters");
		verify(logger, never()).debug(eq("Current 'letter' {}"), anyString());
		verify(logger, never()).debug("Replacing all non-abAB characters");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputStringDecode_null(){
		cipher.preserveCapitals = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Setting input string for decoding '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Ensuring all 'letters' contain 5 characters");
		verify(logger, never()).debug(eq("Current 'letter' {}"), anyString());
		verify(logger, never()).debug("Replacing all non-abAB characters");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testEncode(){
		cipher.preserveCapitals = true;
		cipher.inputString = decodedStringClean;

		cipher.encode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(15)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(1)).debug("Encoding uppercase");
		verify(logger, times(14)).debug("Encoding lowercase");
		verify(logger, times(15)).debug(eq("Output letter {}"), anyString());
		verify(logger, times(1)).debug("Saving output string '{}'", encodedString);
	}

	@Test
	public void testDecode(){
		cipher.preserveCapitals = true;
		cipher.inputString = encodedString;

		cipher.decode();

		assertEquals(decodedStringClean, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(15)).debug(eq("Working letter {}"), anyString());
		verify(logger, times(15)).debug(eq("Location of letter {}"), anyInt());
		verify(logger, times(1)).debug("Decoding uppercase");
		verify(logger, times(14)).debug("Decoding lowercase");
		verify(logger, times(15)).debug(eq("Decoded character {}"), anyChar());
		verify(logger, times(1)).debug("Saving output string '{}'", decodedStringClean);
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(encodedString, cipher.getOutputString());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		verify(logger, times(1)).debug("Resetting fields");
	}

	@Test
	public void testPracticalEncoding(){
		cipher.preserveCapitals = true;

		String output = cipher.encode(decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalEncoding_clean(){
		cipher.preserveCapitals = false;

		String output = cipher.encode(decodedString);

		assertEquals(decodedStringCleanLower, cipher.inputString);
		assertEquals(encodedString.toLowerCase(), cipher.outputString);
		assertEquals(encodedString.toLowerCase(), output);
	}

	@Test
	public void testPracticalDecoding(){
		cipher.preserveCapitals = true;

		String output = cipher.decode(encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}

	@Test
	public void testPracticalDecoding_clean(){
		cipher.preserveCapitals = false;

		String output = cipher.decode(encodedString);

		assertEquals(encodedString.toLowerCase(), cipher.inputString);
		assertEquals(decodedStringCleanLower, cipher.outputString);
		assertEquals(decodedStringCleanLower, output);
	}

	@Test
	public void testPracticalDecoding_upper(){
		cipher.preserveCapitals = false;

		String output = cipher.decode(encodedString.toUpperCase());

		assertEquals(encodedString.toLowerCase(), cipher.inputString);
		assertEquals(decodedStringCleanLower, cipher.outputString);
		assertEquals(decodedStringCleanLower, output);
	}

	@Test
	public void testPracticalDecoding_lower(){
		cipher.preserveCapitals = false;

		String output = cipher.decode(encodedString.toLowerCase());

		assertEquals(encodedString.toLowerCase(), cipher.inputString);
		assertEquals(decodedStringCleanLower, cipher.outputString);
		assertEquals(decodedStringCleanLower, output);
	}
}

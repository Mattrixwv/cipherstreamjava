//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/monosubstitution/PortaTest.java
//Mattrixwv
// Created: 02-28-22
//Modified: 04-19-24
package com.mattrixwv.cipherstream.monosubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;


@ExtendWith(MockitoExtension.class)
public class PortaTest{
	@InjectMocks
	private Porta cipher;
	@Mock
	private Logger logger;
	//Variables
	private static final String decodedString = "Message to^encode";
	private static final String decodedStringClean = "MESSAGETOENCODE";
	private static final String encodedString = "Rtghuos bm^qcwgrw";
	private static final String encodedStringClean = "RTGHUOSBMQCWGRW";
	private static final String keyword = "keyword";
	private static final String keywordDirty = "Ke yw*ord";


	@Test
	public void testConstructor_default(){
		cipher = new Porta();

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
	}

	@Test
	public void testConstructor_preservesCapitals(){
		cipher = new Porta(true, false, false);

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
	}

	@Test
	public void testConstructor_preservesWhitespace(){
		cipher = new Porta(false, true, false);

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
	}

	@Test
	public void testConstructor_preservesSymbols(){
		cipher = new Porta(false, false, true);

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
	}

	@Test
	public void testSetKeyword(){
		cipher.setKeyword(keyword);

		assertEquals(keyword.toUpperCase(), cipher.keyword);
		verify(logger, times(1)).debug("Original keyword '{}'", keyword);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all non-letters");
		verify(logger, times(1)).debug("Cleaned keyword '{}'", keyword.toUpperCase());
	}

	@Test
	public void testSetKeyword_dirty(){
		cipher.setKeyword(keywordDirty);

		assertEquals(keyword.toUpperCase(), cipher.keyword);
		verify(logger, times(1)).debug("Original keyword '{}'", keywordDirty);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all non-letters");
		verify(logger, times(1)).debug("Cleaned keyword '{}'", keyword.toUpperCase());
	}

	@Test
	public void testSetKeyword_blank(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword("");
		});

		assertEquals("", cipher.keyword);
		verify(logger, times(1)).debug("Original keyword '{}'", "");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all non-letters");
		verify(logger, times(1)).debug("Cleaned keyword '{}'", "");
	}

	@Test
	public void testSetKeyword_short(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword("a");
		});

		assertEquals("A", cipher.keyword);
		verify(logger, times(1)).debug("Original keyword '{}'", "a");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all non-letters");
		verify(logger, times(1)).debug("Cleaned keyword '{}'", "A");
	}

	@Test
	public void testSetKeyword_null(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword(null);
		});

		assertEquals("", cipher.keyword);
		verify(logger, never()).debug(eq("Original keyword '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing all non-letters");
		verify(logger, never()).debug(eq("Cleaned keyword '{}'"), anyString());
	}

	@Test
	public void testSetKeyword_symbols(){
		cipher.setKeyword("key*word^");

		assertEquals(keyword.toUpperCase(), cipher.keyword);
		verify(logger, times(1)).debug("Original keyword '{}'", "key*word^");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all non-letters");
		verify(logger, times(1)).debug("Cleaned keyword '{}'", keyword.toUpperCase());
	}

	@Test
	public void testSetInputString(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString, cipher.inputString);
		verify(logger, times(1)).debug("Original input string {}", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString);
	}

	@Test
	public void testSetInputString_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Original input string {}", decodedString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.toUpperCase());
	}

	@Test
	public void testSetInputString_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string {}", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputString_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string {}", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("[^a-zA-Z\\s]", ""));
	}

	@Test
	public void testSetInputString_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Original input string {}", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputString_null(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Original input string {}"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testGetReplacer_0(){
		int keywordCnt = 0;
		char letter = 'A';
		char expected = 'N';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_1(){
		int keywordCnt = 1;
		char letter = 'B';
		char expected = 'O';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_2(){
		int keywordCnt = 2;
		char letter = 'C';
		char expected = 'Q';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_3(){
		int keywordCnt = 3;
		char letter = 'D';
		char expected = 'R';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_4(){
		int keywordCnt = 4;
		char letter = 'E';
		char expected = 'T';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_5(){
		int keywordCnt = 5;
		char letter = 'F';
		char expected = 'U';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_6(){
		int keywordCnt = 6;
		char letter = 'G';
		char expected = 'W';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_7(){
		int keywordCnt = 7;
		char letter = 'H';
		char expected = 'X';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_8(){
		int keywordCnt = 8;
		char letter = 'I';
		char expected = 'Z';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_9(){
		int keywordCnt = 9;
		char letter = 'J';
		char expected = 'N';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_10(){
		int keywordCnt = 10;
		char letter = 'K';
		char expected = 'P';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_11(){
		int keywordCnt = 11;
		char letter = 'L';
		char expected = 'Q';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_12(){
		int keywordCnt = 12;
		char letter = 'M';
		char expected = 'S';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_13(){
		int keywordCnt = 13;
		char letter = 'N';
		char expected = 'H';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_14(){
		int keywordCnt = 14;
		char letter = 'O';
		char expected = 'H';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_15(){
		int keywordCnt = 15;
		char letter = 'P';
		char expected = 'I';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_16(){
		int keywordCnt = 16;
		char letter = 'Q';
		char expected = 'I';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_17(){
		int keywordCnt = 17;
		char letter = 'R';
		char expected = 'J';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_18(){
		int keywordCnt = 18;
		char letter = 'S';
		char expected = 'J';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_19(){
		int keywordCnt = 19;
		char letter = 'T';
		char expected = 'K';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_20(){
		int keywordCnt = 20;
		char letter = 'U';
		char expected = 'K';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_21(){
		int keywordCnt = 21;
		char letter = 'V';
		char expected = 'L';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_22(){
		int keywordCnt = 22;
		char letter = 'W';
		char expected = 'L';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_23(){
		int keywordCnt = 23;
		char letter = 'X';
		char expected = 'M';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_24(){
		int keywordCnt = 24;
		char letter = 'Y';
		char expected = 'M';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_25(){
		int keywordCnt = 25;
		char letter = 'Z';
		char expected = 'A';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testGetReplacer_symbol(){
		//?This scenario requires something to be wrong with setKeyword() but sonarqube wants it
		int keywordCnt = 26;
		char letter = 'A';
		char expected = 'A';
		cipher.keyword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ*";

		char replacer = cipher.getReplacer(keywordCnt, letter);

		assertEquals(expected, replacer);
		verify(logger, times(1)).debug("Getting letter that replaces {} at {}", letter, keywordCnt);
		verify(logger, times(1)).debug("Replacer {}", replacer);
	}

	@Test
	public void testEncode(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keyword.toUpperCase();
		cipher.inputString = decodedString;

		cipher.encode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(17)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(1)).debug("Encoding uppercase");
		verify(logger, times(14)).debug("Encoding lowercase");
		verify(logger, times(17)).debug(eq("Encoded letter {}"), anyChar());
		verify(logger, times(1)).debug("Saving output string '{}'", encodedString);
	}

	@Test
	public void testDecode(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keyword.toUpperCase();
		cipher.inputString = encodedString;

		cipher.decode();

		assertEquals(decodedString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(17)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(1)).debug("Encoding uppercase");
		verify(logger, times(14)).debug("Encoding lowercase");
		verify(logger, times(17)).debug(eq("Encoded letter {}"), anyChar());
		verify(logger, times(1)).debug("Saving output string '{}'", decodedString);
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.keyword = keyword;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(encodedString, cipher.getOutputString());
		assertEquals(keyword, cipher.getKeyword());
	}

	@Test
	public void testRest(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.keyword = keyword;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
	}

	@Test
	public void testPracticalEncoding(){
		cipher = new Porta(true, true, true);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(keyword.toUpperCase(), cipher.keyword);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalEncoding_clean(){
		cipher = new Porta(false, false, false);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(keyword.toUpperCase(), cipher.keyword);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
	}

	@Test
	public void testPracticalDecoding(){
		cipher = new Porta(true, true, true);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(keyword.toUpperCase(), cipher.keyword);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testPracticalDecoding_clean(){
		cipher = new Porta(false, false, false);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedStringClean, cipher.inputString);
		assertEquals(keyword.toUpperCase(), cipher.keyword);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}
}

//CipherStreamJava/src/test/java/com/mattrixwv/CipherStreamJava/CaesarTest.java
//Matthew Ellison
// Created: 07-25-21
//Modified: 04-19-24
package com.mattrixwv.cipherstream.monosubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidInputException;


@ExtendWith(MockitoExtension.class)
public class CaesarTest{
	@InjectMocks
	private Caesar cipher;
	@Mock
	private Logger logger;
	//Variables
	private static final String decodedString = "The quick brown fox jumps over - the lAzy dog";
	private static final String decodedStringClean = "thequickbrownfoxjumpsoverthelazydog";
	private static final String encodedString = "Qeb nrfzh yoltk clu grjmp lsbo - qeb iXwv ald";
	private static final String encodedStringClean = "qebnrfzhyoltkclugrjmplsboqebixwvald";
	private static final int shift = 23;


	@Test
	public void testConstructor_default(){
		cipher = new Caesar();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals(0, cipher.shift);
		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
	}

	@Test
	public void testConstructor_preservesCapitals(){
		cipher = new Caesar(true, false, false);

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals(0, cipher.shift);
		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
	}

	@Test
	public void testConstructor_preservesSymbols(){
		cipher = new Caesar(false, true, false);

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals(0, cipher.shift);
		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
	}

	@Test
	public void testConstructor_preservesWhitespace(){
		cipher = new Caesar(false, false, true);

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals(0, cipher.shift);
		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
	}

	@Test
	public void testSetShift(){
		cipher.setShift(shift);

		assertEquals(shift, cipher.shift);
		verify(logger, times(1)).debug("Setting shift {}", shift);
		verify(logger, times(1)).debug("Cleaned shift {}", shift);
	}

	@Test
	public void testSetShift_large(){
		cipher.setShift(shift + 26);

		assertEquals(shift, cipher.shift);
		verify(logger, times(1)).debug("Setting shift {}", shift + 26);
		verify(logger, times(1)).debug("Cleaned shift {}", shift);
	}

	@Test
	public void testSetShift_negative(){
		cipher.setShift(shift - 26);

		assertEquals(shift - 26, cipher.shift);
		verify(logger, times(1)).debug("Setting shift {}", shift - 26);
		verify(logger, times(1)).debug("Cleaned shift {}", shift - 26);
	}

	@Test
	public void testSetInputString(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString, cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString);
	}

	@Test
	public void testSetInputString_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.toLowerCase(), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.toLowerCase());
	}

	@Test
	public void testSetInputString_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputString_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("[^a-zA-Z\\s]", ""));
	}

	@Test
	public void testSetInputString_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputString_null(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testEncode(){
		cipher.inputString = decodedString;
		cipher.shift = shift;

		cipher.encode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(45)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(2)).debug("Encoding uppercase");
		verify(logger, never()).debug("Wrapping arround to Z");
		verify(logger, times(1)).debug("Wrapping around to A");
		verify(logger, times(33)).debug("Encoding lowercase");
		verify(logger, never()).debug("Wrapping around to z");
		verify(logger, times(31)).debug("Wrapping around to a");
		verify(logger, times(45)).debug(eq("Encoded character {}"), anyChar());
		verify(logger, times(1)).debug("Saving encoded string '{}'", encodedString);
	}

	@Test
	public void testEncode_negative(){
		cipher.inputString = decodedString;
		cipher.shift = shift - 26;

		cipher.encode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(45)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(2)).debug("Encoding uppercase");
		verify(logger, times(1)).debug("Wrapping around to Z");
		verify(logger, never()).debug("Wrapping around to A");
		verify(logger, times(33)).debug("Encoding lowercase");
		verify(logger, times(2)).debug("Wrapping around to z");
		verify(logger, never()).debug("Wrapping around to a");
		verify(logger, times(45)).debug(eq("Encoded character {}"), anyChar());
		verify(logger, times(1)).debug("Saving encoded string '{}'", encodedString);
	}

	@Test
	public void testDecode(){
		cipher.inputString = encodedString;
		cipher.shift = shift;

		cipher.decode();

		assertEquals(decodedString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(45)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(2)).debug("Decoding uppercase");
		verify(logger, times(1)).debug("Wrapping around to Z");
		verify(logger, never()).debug("Wrapping around to A");
		verify(logger, times(33)).debug("Decoding lowercase");
		verify(logger, times(31)).debug("Wrapping around to z");
		verify(logger, never()).debug("Wrapping around to a");
		verify(logger, times(45)).debug(eq("Decoded character {}"), anyChar());
		verify(logger, times(1)).debug("Saving decoded string '{}'", decodedString);
	}

	@Test
	public void testDecode_negative(){
		cipher.inputString = encodedString;
		cipher.shift = shift - 26;

		cipher.decode();

		assertEquals(decodedString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(45)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(2)).debug("Decoding uppercase");
		verify(logger, never()).debug("Wrapping around to Z");
		verify(logger, times(1)).debug("Wrapping around to A");
		verify(logger, times(33)).debug("Decoding lowercase");
		verify(logger, never()).debug("Wrapping around to z");
		verify(logger, times(2)).debug("Wrapping around to a");
		verify(logger, times(45)).debug(eq("Decoded character {}"), anyChar());
		verify(logger, times(1)).debug("Saving decoded string '{}'", decodedString);
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.shift = shift;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(encodedString, cipher.getOutputString());
		assertEquals(shift, cipher.getShift());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.shift = shift;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals(0, cipher.shift);
		verify(logger, times(1)).debug("Resetting fields");
	}

	@Test
	public void testPracticalEncoding(){
		cipher = new Caesar(true, true, true);

		String output = cipher.encode(shift, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(shift, cipher.shift);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalEncoding_clean(){
		cipher = new Caesar(false, false, false);

		String output = cipher.encode(shift, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(shift, cipher.shift);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
	}

	@Test
	public void testPracticalEncoding_negative(){
		cipher = new Caesar(true, true, true);

		String output = cipher.encode(shift, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(shift, cipher.shift);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalDecoding(){
		cipher = new Caesar(true, true, true);

		String output = cipher.decode(shift, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(shift, cipher.shift);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testPracticalDecoding_clean(){
		cipher = new Caesar(false, false, false);

		String output = cipher.decode(shift, encodedString);

		assertEquals(encodedStringClean, cipher.inputString);
		assertEquals(shift, cipher.shift);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}

	@Test
	public void testPracticalDecoding_negative(){
		cipher = new Caesar(true, true, true);
		String output = cipher.decode(shift - 26, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(shift - 26, cipher.shift);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}
}

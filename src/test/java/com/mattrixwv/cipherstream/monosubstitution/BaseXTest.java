//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/monosubstitution/BaseXTest.java
//Mattrixwv
// Created: 01-08-22
//Modified: 04-19-24
package com.mattrixwv.cipherstream.monosubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidBaseException;
import com.mattrixwv.cipherstream.exceptions.InvalidCharacterException;
import com.mattrixwv.cipherstream.exceptions.InvalidInputException;


@ExtendWith(MockitoExtension.class)
public class BaseXTest{
	@InjectMocks
	private BaseX cipher;
	@Mock
	private Logger logger;
	//Variables
	private static final String decodedString = "A+B@C d\te\nf";
	private static final String encodedString_2 = "1000001 101011 1000010 1000000 1000011 100000 1100100 1001 1100101 1010 1100110";
	private static final String encodedString_8 = "101 53 102 100 103 40 144 11 145 12 146";
	private static final String encodedString_10 = "65 43 66 64 67 32 100 9 101 10 102";
	private static final String encodedString_16 = "41 2B 42 40 43 20 64 9 65 A 66";


	@Test
	public void testConstructor_default(){
		cipher = new BaseX();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals(2, cipher.base);
	}

	@Test
	public void testConstructor_base(){
		cipher = new BaseX(8);

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals(8, cipher.base);
	}

	@Test
	public void testSetInputStringEncode(){
		cipher.setInputStringEncode(decodedString);

		assertEquals(decodedString, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", decodedString);
	}

	@Test
	public void testSetInputStringEncode_blank(){
		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for encoding '{}'", "");
	}

	@Test
	public void testSetInputStringEncode_null(){
		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringEncode(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug("Setting input string for encoding '{}'", decodedString);
		verify(logger, never()).debug(anyString(), anyString());
		verify(logger, never()).debug(anyString());
	}

	@Test
	public void testSetInputStringDecode(){
		cipher.base = 16;

		cipher.setInputStringDecode(encodedString_16);

		assertEquals(encodedString_16, cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", encodedString_16);
		verify(logger, times(1)).debug("Creating string of valid 'numbers'");
		verify(logger, times(16)).debug(eq("Current number {}, converted {}"), anyInt(), anyString());
		verify(logger, times(1)).debug("Checking for invalid characters");
		verify(logger, times(1)).debug("Cleaned input string '{}'", encodedString_16);
	}

	@Test
	public void testSetInputStringDecode_invalid(){
		cipher.base = 16;

		assertThrows(InvalidCharacterException.class, () -> {
			cipher.setInputStringDecode("G");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", "G");
		verify(logger, times(1)).debug("Creating string of valid 'numbers'");
		verify(logger, times(16)).debug(eq("Current number {}, converted {}"), anyInt(), anyString());
		verify(logger, times(1)).debug("Checking for invalid characters");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputStringDecode_blank(){
		cipher.base = 16;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Setting input string for decoding '{}'", "");
		verify(logger, times(1)).debug("Creating string of valid 'numbers'");
		verify(logger, times(16)).debug(eq("Current number {}, converted {}"), anyInt(), anyString());
		verify(logger, times(1)).debug("Checking for invalid characters");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputStringDecode_null(){
		cipher.base = 16;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputStringDecode(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Setting input string for decoding '{}'"), anyString());
		verify(logger, never()).debug("Creating string of valid 'numbers'");
		verify(logger, never()).debug(eq("Current number {}, converted {}"), anyInt(), anyString());
		verify(logger, never()).debug("Checking for invalid characters");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetBase(){
		cipher.setBase(16);

		assertEquals(16, cipher.base);
		verify(logger, times(1)).debug("Setting base {}", 16);
	}

	@Test
	public void testSetBase_min(){
		assertThrows(InvalidBaseException.class, () -> {
			cipher.setBase(Character.MIN_RADIX - 1);
		});

		assertEquals(2, cipher.base);
		verify(logger, never()).debug(eq("Setting base {}"), anyInt());
	}

	@Test
	public void testSetBase_max(){
		assertThrows(InvalidBaseException.class, () -> {
			cipher.setBase(Character.MAX_RADIX + 1);
		});

		assertEquals(2, cipher.base);
		verify(logger, never()).debug(eq("Setting base {}"), anyInt());
	}

	@Test
	public void testEncode(){
		cipher.base = 16;
		cipher.inputString = decodedString;

		cipher.encode();

		assertEquals(encodedString_16, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(11)).debug(eq("Working number {}"), anyChar());
		verify(logger, times(11)).debug(eq("Converted number {}"), anyString());
		verify(logger, times(1)).debug("Saving output string '{}'", encodedString_16);
	}

	@Test
	public void testDecode(){
		cipher.base = 16;
		cipher.inputString = encodedString_16;

		cipher.decode();

		assertEquals(decodedString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(11)).debug(eq("Current number {}"), anyString());
		verify(logger, times(11)).debug(eq("Decoded number {}"), anyInt());
		verify(logger, times(1)).debug("Saving output string '{}'", decodedString);
	}

	@Test
	public void testDecode_invalidCharacter(){
		cipher.base = 16;
		cipher.inputString = "FFF";

		assertThrows(InvalidCharacterException.class, () -> {
			cipher.decode();
		});

		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(1)).debug("Current number {}", "FFF");
		verify(logger, times(1)).debug("Decoded number {}", 4095);
		verify(logger, never()).debug(eq("Saving output string '{}'"), anyString());
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString_2;
		cipher.base = 8;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(encodedString_2, cipher.getOutputString());
		assertEquals(8, cipher.getBase());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString_2;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		verify(logger, times(1)).debug("Resetting fields");
	}

	@Test
	public void testPracticalEncoding_2(){
		String output = cipher.encode(2, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(2, cipher.base);
		assertEquals(encodedString_2, cipher.outputString);
		assertEquals(encodedString_2, output);
	}

	@Test
	public void testPracticalEncoding_8(){
		String output = cipher.encode(8, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(8, cipher.base);
		assertEquals(encodedString_8, cipher.outputString);
		assertEquals(encodedString_8, output);
	}

	@Test
	public void testPracticalEncoding_10(){
		String output = cipher.encode(10, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(10, cipher.base);
		assertEquals(encodedString_10, cipher.outputString);
		assertEquals(encodedString_10, output);
	}

	@Test
	public void testPracticalEncoding_16(){
		String output = cipher.encode(16, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(16, cipher.base);
		assertEquals(encodedString_16, cipher.outputString);
		assertEquals(encodedString_16, output);
	}

	@Test
	public void testPracticalEncoding_inputOnly(){
		String output = cipher.encode(decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(2, cipher.base);
		assertEquals(encodedString_2, cipher.outputString);
		assertEquals(encodedString_2, output);
	}

	@Test
	public void testPracticalDecoding_2(){
		String output = cipher.decode(2, encodedString_2);

		assertEquals(encodedString_2, cipher.inputString);
		assertEquals(2, cipher.base);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testPracticalDecoding_8(){
		String output = cipher.decode(8, encodedString_8);

		assertEquals(encodedString_8, cipher.inputString);
		assertEquals(8, cipher.base);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testPracticalDecoding_10(){
		String output = cipher.decode(10, encodedString_10);

		assertEquals(encodedString_10, cipher.inputString);
		assertEquals(10, cipher.base);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testPracticalDecoding_16(){
		String output = cipher.decode(16, encodedString_16);

		assertEquals(encodedString_16, cipher.inputString);
		assertEquals(16, cipher.base);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testPracticalDecoding_inputOnly(){
		String output = cipher.decode(encodedString_2);

		assertEquals(encodedString_2, cipher.inputString);
		assertEquals(2, cipher.base);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}
}

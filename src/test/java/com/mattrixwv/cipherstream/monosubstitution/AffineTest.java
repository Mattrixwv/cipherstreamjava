//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/polysubstitution/AffineTest.java
//Mattrixwv
// Created: 01-26-22
//Modified: 04-19-24
package com.mattrixwv.cipherstream.monosubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;


@ExtendWith(MockitoExtension.class)
public class AffineTest{
	@InjectMocks
	private Affine cipher;
	@Mock
	private Logger logger;
	//Variables
	private static final String decodedString = "MEssage to^encode";
	private static final String decodedStringClean = "messagetoencode";
	private static final String encodedString = "PBtthlb yz^burzwb";
	private static final String encodedStringClean = "pbtthlbyzburzwb";
	private static final int key1 = 5;
	private static final int key2 = 7;


	@Test
	public void testConstructor_default(){
		cipher = new Affine();

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveSymbols);
		assertFalse(cipher.preserveWhitespace);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals(0, cipher.key1);
		assertEquals(0, cipher.key2);
	}

	@Test
	public void testConstructor_preservesCapitals(){
		cipher = new Affine(true, false, false);

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveSymbols);
		assertFalse(cipher.preserveWhitespace);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals(0, cipher.key1);
		assertEquals(0, cipher.key2);
	}

	@Test
	public void testConstructor_preservesWhitespace(){
		cipher = new Affine(false, true, false);

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveSymbols);
		assertTrue(cipher.preserveWhitespace);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals(0, cipher.key1);
		assertEquals(0, cipher.key2);
	}

	@Test
	public void testConstructor_preservesSymbols(){
		cipher = new Affine(false, false, true);

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveSymbols);
		assertFalse(cipher.preserveWhitespace);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals(0, cipher.key1);
		assertEquals(0, cipher.key2);
	}

	@Test
	public void testKey1(){
		cipher.setKey1(key1);

		assertEquals(key1, cipher.key1);
		verify(logger, times(1)).debug("Setting key1 {}", key1);
		verify(logger, times(1)).debug("Cleaned key1 {}", key1);
	}

	@Test
	public void testSetKey1_notPrime(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKey1(2);
		});

		verify(logger, times(1)).debug("Setting key1 {}", 2);
		verify(logger, never()).debug("Cleaned key1 {}", 2);
	}

	@Test
	public void testSetKey1_negative(){
		cipher.setKey1(-27);

		assertEquals(25, cipher.key1);
		verify(logger, times(1)).debug("Setting key1 {}", -27);
		verify(logger, times(1)).debug("Cleaned key1 {}", 25);
	}

	@Test
	public void testSetKey1_large(){
		cipher.setKey1(key1 + 26);

		assertEquals(key1, cipher.key1);
		verify(logger, times(1)).debug("Setting key1 {}", key1 + 26);
		verify(logger, times(1)).debug("Cleaned key1 {}", key1);
	}

	@Test
	public void testSetKey2(){
		cipher.setKey2(key2);

		assertEquals(key2, cipher.key2);
		verify(logger, times(1)).debug("Setting key2 {}", key2);
		verify(logger, times(1)).debug("Cleaned key2 {}", key2);
	}

	@Test
	public void testSetKey2_negative(){
		cipher.setKey2(-27);

		assertEquals(25, cipher.key2);
		verify(logger, times(1)).debug("Setting key2 {}", -27);
		verify(logger, times(1)).debug("Cleaned key2 {}", cipher.key2);
	}

	@Test
	public void testSetKey2_large(){
		cipher.setKey2(key2 + 26);

		assertEquals(key2, cipher.key2);
		verify(logger, times(1)).debug("Setting key2 {}", key2 + 26);
		verify(logger, times(1)).debug("Cleaned key2 {}", key2);
	}

	@Test
	public void testSetInputString(){
		cipher.preserveCapitals = true;
		cipher.preserveSymbols = true;
		cipher.preserveWhitespace = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString, cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString);
	}

	@Test
	public void testSetInputString_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveSymbols = true;
		cipher.preserveWhitespace = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.toLowerCase(), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.toLowerCase());
	}

	@Test
	public void testSetInputString_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveSymbols = true;
		cipher.preserveWhitespace = false;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing sybols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputString_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveSymbols = false;
		cipher.preserveWhitespace = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("[^a-zA-Z\\s]", ""));
	}

	@Test
	public void testSetInputString_null(){
		cipher.preserveCapitals = true;
		cipher.preserveSymbols = true;
		cipher.preserveWhitespace = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Original input string '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputString_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveSymbols = true;
		cipher.preserveWhitespace = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testEncode(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.inputString = decodedString;
		cipher.key1 = key1;
		cipher.key2 = key2;

		cipher.encode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(17)).debug(eq("Current char {}"), anyChar());
		verify(logger, times(15)).debug(eq("Encoded char {}"), anyChar());
		verify(logger, times(1)).debug("Saving output string '{}'", encodedString);
	}

	@Test
	public void testDecode(){
		cipher.preserveCapitals = true;
		cipher.preserveSymbols = true;
		cipher.preserveWhitespace = true;
		cipher.inputString = encodedString;
		cipher.key1 = key1;
		cipher.key2 = key2;

		cipher.decode();

		assertEquals(decodedString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(1)).debug("Key1 inverse {}", 21);
		verify(logger, times(17)).debug(eq("Current char {}"), anyChar());
		verify(logger, times(15)).debug(eq("Decoded char {}"), anyChar());
		verify(logger, times(1)).debug("Saving output string '{}'", decodedString);
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.key1 = key1;
		cipher.key2 = key2;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(encodedString, cipher.getOutputString());
		assertEquals(key1, cipher.getKey1());
		assertEquals(key2, cipher.getKey2());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.key1 = key1;
		cipher.key2 = key2;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals(0, cipher.key1);
		assertEquals(0, cipher.key2);
		verify(logger, times(1)).debug("Resetting fields");
	}

	@Test
	public void testPracticalEncoding(){
		cipher = new Affine(true, true, true);

		String output = cipher.encode(key1, key2, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(key1, cipher.key1);
		assertEquals(key2, cipher.key2);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalEncoding_clean(){
		cipher = new Affine(false, false, false);

		String output = cipher.encode(key1, key2, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(key1, cipher.key1);
		assertEquals(key2, cipher.key2);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
	}

	@Test
	public void testPracticalDecoding(){
		cipher = new Affine(true, true, true);

		String output = cipher.decode(key1, key2, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(key1, cipher.key1);
		assertEquals(key2, cipher.key2);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testpracticalDecoding_clean(){
		cipher = new Affine(false, false, false);

		String output = cipher.decode(key1, key2, encodedString);

		assertEquals(encodedStringClean, cipher.inputString);
		assertEquals(key1, cipher.key1);
		assertEquals(key2, cipher.key2);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}
}

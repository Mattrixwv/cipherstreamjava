//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/monosubstitution/OneTimePadTest.java
//Mattrixwv
// Created: 02-23-22
//Modified: 05-04-23
package com.mattrixwv.cipherstream.monosubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;


@ExtendWith(MockitoExtension.class)
public class OneTimePadTest{
	@InjectMocks
	private OneTimePad cipher;
	@Mock(name = "com.mattrixwv.cipherstream.monosubstitution.OneTimePad")
	private Logger logger;
	//Variables
	private static final String decodedString = "Message to^encode";
	private static final String decodedStringClean = "MESSAGETOENCODE";
	private static final String encodedString = "Wiqooxh mv^egkgws";
	private static final String encodedStringClean = "WIQOOXHMVEGKGWS";
	private static final String keyword = "keywordThatIsTotallyRandom";
	private static final ArrayList<Integer> offset = new ArrayList<>(Arrays.asList(10, 4, 24, 22, 14, 17, 3, 19, 7, 0, 19, 8, 18, 19, 14, 19, 0, 11, 11, 24, 17, 0, 13, 3, 14, 12));


	@Test
	public void testConstructor_default(){
		cipher = new OneTimePad();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		assertEquals(new ArrayList<>(), cipher.offset);
		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
	}

	@Test
	public void testConstructor_preservesCapitals(){
		cipher = new OneTimePad(true, false, false);

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		assertEquals(new ArrayList<>(), cipher.offset);
		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
	}

	@Test
	public void testConstructor_preservesWhitespace(){
		cipher = new OneTimePad(false, true, false);

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		assertEquals(new ArrayList<>(), cipher.offset);
		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
	}

	@Test
	public void testConstructor_preservesSymbols(){
		cipher = new OneTimePad(false, false, true);

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		assertEquals(new ArrayList<>(), cipher.offset);
		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
	}

	@Test
	public void testEncode(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(keyword.toUpperCase(), cipher.keyword);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
		assertEquals(offset, cipher.offset);
		verify(logger, times(1)).debug("Encoding");
	}

	@Test
	public void testEncode_clean(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = false;

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(keyword.toUpperCase(), cipher.keyword);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
		assertEquals(offset, cipher.offset);
		verify(logger, times(1)).debug("Encoding");
	}

	@Test
	public void testEncode_short(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidKeywordException.class, () -> {
			cipher.encode("keyword", decodedString);
		});

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		assertEquals(new ArrayList<>(), cipher.offset);
		verify(logger, never()).debug(anyString());
	}

	@Test
	public void testDecode(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(keyword.toUpperCase(), cipher.keyword);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
		assertEquals(offset, cipher.offset);
		verify(logger, times(1)).debug("Decoding");
	}

	@Test
	public void testDecode_clean(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = false;

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedStringClean, cipher.inputString);
		assertEquals(keyword.toUpperCase(), cipher.keyword);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
		assertEquals(offset, cipher.offset);
		verify(logger, times(1)).debug("Decoding");
	}

	@Test
	public void testDecode_short(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.decode("keyword", encodedString);
		});

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.keyword);
		assertEquals("", cipher.outputString);
		assertEquals(new ArrayList<>(), cipher.offset);
		verify(logger, never()).debug(anyString());
	}

	@Test
	public void testPracticalEncoding(){
		cipher = new OneTimePad(true, true, true);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(keyword.toUpperCase(), cipher.keyword);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalEncoding_clean(){
		cipher = new OneTimePad(false, false, false);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(keyword.toUpperCase(), cipher.keyword);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
	}

	@Test
	public void testPracticalDecoding(){
		cipher = new OneTimePad(true, true, true);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(keyword.toUpperCase(), cipher.keyword);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testPracticalDecoding_clean(){
		cipher = new OneTimePad(false, false, false);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedStringClean, cipher.inputString);
		assertEquals(keyword.toUpperCase(), cipher.keyword);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}
}

//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/monosubstitution/AtbashTest.java
//Mattrixwv
// Created: 07-25-21
//Modified: 04-19-24
package com.mattrixwv.cipherstream.monosubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidInputException;


@ExtendWith(MockitoExtension.class)
public class AtbashTest{
	@InjectMocks
	private Atbash cipher;
	@Mock
	private Logger logger;
	//Variables
	private static final String decodedString = "Message to^encode";
	private static final String decodedStringClean = "MESSAGETOENCODE";
	private static final String encodedString = "Nvhhztv gl^vmxlwv";
	private static final String encodedStringClean = "NVHHZTVGLVMXLWV";


	@Test
	public void testConstructor_default(){
		cipher = new Atbash();

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testConstructor_preservesCapitals(){
		cipher = new Atbash(true, false, false);

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testConstructor_preservesWhitespace(){
		cipher = new Atbash(false, true, false);

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testConstructor_preservesSymbols(){
		cipher = new Atbash(false, false, true);

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
	}

	@Test
	public void testEncode(){
		cipher.inputString = decodedString;

		cipher.encode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(17)).debug(eq("Encoding char {}"), anyChar());
		verify(logger, times(1)).debug("Encoding uppercase");
		verify(logger, times(14)).debug("Encoding lowercase");
		verify(logger, times(2)).debug("Appending symbol");
		verify(logger, times(1)).debug("Saving output string '{}'", encodedString);
	}

	@Test
	public void testSetInputString(){
		cipher.preserveCapitals = true;
		cipher.preserveSymbols = true;
		cipher.preserveWhitespace = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString, cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString);
	}

	@Test
	public void testSetInputString_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveSymbols = true;
		cipher.preserveWhitespace = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.toUpperCase());
	}

	@Test
	public void testSetInputString_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveSymbols = true;
		cipher.preserveWhitespace = false;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputString_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveSymbols = false;
		cipher.preserveWhitespace = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("[^a-zA-Z\\s]", ""));
	}

	@Test
	public void testSetInputString_null(){
		cipher.preserveCapitals = true;
		cipher.preserveSymbols = true;
		cipher.preserveWhitespace = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Original input string '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetInputString_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveSymbols = true;
		cipher.preserveWhitespace = true;
		cipher.inputString = decodedString;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(encodedString, cipher.getOutputString());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		verify(logger, times(1)).debug("Resetting fields");
	}

	@Test
	public void testPracticalEncoding(){
		cipher = new Atbash(true, true, true);

		String output = cipher.encode(decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalEncoding_clean(){
		cipher = new Atbash(false, false, false);

		String output = cipher.encode(decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
	}

	@Test
	public void testPracticalDecoding(){
		cipher = new Atbash(true, true, true);

		String output = cipher.decode(encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testPracticalDecoding_clean(){
		cipher = new Atbash(false, false, false);

		String output = cipher.decode(encodedString);

		assertEquals(encodedStringClean, cipher.inputString);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}
}

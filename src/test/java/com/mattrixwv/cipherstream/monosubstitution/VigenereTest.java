//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/monosubstitution/VigenereTest.java
//Mattrixwv
// Created: 07-25-21
//Modified: 04-19-24
package com.mattrixwv.cipherstream.monosubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;


@ExtendWith(MockitoExtension.class)
public class VigenereTest{
	@InjectMocks
	private Vigenere cipher;
	@Mock
	private Logger logger;
	//Variables
	private static final String inputString = "MeSsage to^encode";
	private static final String inputStringClean = "MESSAGETOENCODE";
	private static final String outputString = "WiQooxh ds^cjqfgo";
	private static final String outputStringClean = "WIQOOXHDSCJQFGO";
	private static final String keyword = "ke yw*ord";
	private static final String keywordClean = "KEYWORD";
	private ArrayList<Integer> offset = new ArrayList<>(List.of(10, 4, 24, 22, 14, 17, 3));


	@Test
	public void testConstructor_default(){
		cipher = new Vigenere();

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.offset);
	}

	@Test
	public void testConstructor_preservesCapitals(){
		cipher = new Vigenere(true, false, false);

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.offset);
	}

	@Test
	public void testConstructor_preservesWhitespace(){
		cipher = new Vigenere(false, true, false);

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.offset);
	}

	@Test
	public void testConstructor_preservesSymbols(){
		cipher = new Vigenere(false, false, true);

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.offset);
	}

	@Test
	public void testSetOffset(){
		cipher.keyword = keywordClean;

		cipher.setOffset();

		assertEquals(keywordClean, cipher.keyword);
		assertEquals(offset, cipher.offset);
		verify(logger, times(1)).debug("Setting offset array from keyword");
		verify(logger, times(1)).debug("Offset {}", offset);
	}

	@Test
	public void testSetInputString(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.offset = offset;

		cipher.setInputString(inputString);

		assertEquals(inputString, cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", inputString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", inputString);
	}

	@Test
	public void testSetInputString_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.offset = offset;

		cipher.setInputString(inputString);

		assertEquals(inputString.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", inputString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", inputString.toUpperCase());
	}

	@Test
	public void testSetInputString_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.offset = offset;

		cipher.setInputString(inputString);

		assertEquals(inputString.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", inputString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", inputString.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputString_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;
		cipher.keyword = keywordClean;
		cipher.offset = offset;

		cipher.setInputString(inputString);

		assertEquals(inputString.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", inputString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", inputString.replaceAll("[^a-zA-Z\\s]", ""));
	}

	@Test
	public void testSetInputString_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.offset = offset;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputString_null(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.keyword = keywordClean;
		cipher.offset = offset;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Original input string '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetKeyword(){
		cipher.setKeyword(keyword);

		assertEquals(keywordClean, cipher.keyword);
		assertEquals(offset, cipher.offset);
		verify(logger, times(1)).debug("Original keyword '{}'", keyword);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all non-letter characters");
		verify(logger, times(1)).debug("Clean keyword '{}'", keywordClean);
	}

	@Test
	public void testSetKeyword_blank(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword("");
		});

		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.offset);
		verify(logger, times(1)).debug("Original keyword '{}'", "");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all non-letter characters");
		verify(logger, times(1)).debug("Clean keyword '{}'", "");
	}

	@Test
	public void testSetKeyword_null(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword(null);
		});

		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.offset);
		verify(logger, never()).debug(eq("Original keyword '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing all non-letter characters");
		verify(logger, never()).debug(eq("Clean keyword '{}'"), anyString());
	}

	@Test
	public void testEncode(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.inputString = inputString;
		cipher.keyword = keywordClean;
		cipher.offset = offset;

		cipher.encode();

		assertEquals(outputString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(17)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(2)).debug("Encoding uppercase");
		verify(logger, times(1)).debug("Wrapping around to A");
		verify(logger, times(13)).debug("Encoding lowercase");
		verify(logger, times(5)).debug("Wrapping around to a");
		verify(logger, times(17)).debug(eq("Encoded character {}"), anyChar());
		verify(logger, times(1)).debug("Encoded message '{}'", outputString);
	}

	@Test
	public void testDecode(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.inputString = outputString;
		cipher.keyword = keywordClean;
		cipher.offset = offset;

		cipher.decode();

		assertEquals(inputString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(17)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(2)).debug("Decoding uppercase");
		verify(logger, times(1)).debug("Wrapping around to Z");
		verify(logger, times(13)).debug("Decoding lowercase");
		verify(logger, times(5)).debug("Wrapping around to z");
		verify(logger, times(17)).debug(eq("Decoded character {}"), anyChar());
		verify(logger, times(1)).debug("Decoded message '{}'", inputString);
	}

	@Test
	public void testGetters(){
		cipher.inputString = inputString;
		cipher.outputString = outputString;
		cipher.keyword = keyword;
		cipher.offset = offset;

		assertEquals(inputString, cipher.getInputString());
		assertEquals(outputString, cipher.getOutputString());
		assertEquals(keyword, cipher.getKeyword());
		assertEquals(offset, cipher.getOffsets());
	}

	@Test
	public void testReset(){
		cipher.inputString = inputString;
		cipher.outputString = outputString;
		cipher.keyword = keyword;
		cipher.offset = offset;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(new ArrayList<>(), cipher.offset);
		verify(logger, times(1)).debug("Resetting fields");
	}

	@Test
	public void testPracticalEncoding(){
		cipher = new Vigenere(true, true, true);

		String output = cipher.encode(keyword, inputString);

		assertEquals(inputString, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(offset, cipher.offset);
		assertEquals(outputString, cipher.outputString);
		assertEquals(outputString, output);
	}

	@Test
	public void testPracticalEncoding_clean(){
		cipher = new Vigenere(false, false, false);

		String output = cipher.encode(keyword, inputString);

		assertEquals(inputStringClean, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(offset, cipher.offset);
		assertEquals(outputStringClean, cipher.outputString);
		assertEquals(outputStringClean, output);
	}

	@Test
	public void testPracticalDecoding(){
		cipher = new Vigenere(true, true, true);

		String output = cipher.decode(keyword, outputString);

		assertEquals(outputString, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(offset, cipher.offset);
		assertEquals(inputString, cipher.outputString);
		assertEquals(inputString, output);
	}

	@Test
	public void testPracticalDecoding_clean(){
		cipher = new Vigenere(false, false, false);

		String output = cipher.decode(keyword, outputString);

		assertEquals(outputStringClean, cipher.inputString);
		assertEquals(keywordClean, cipher.keyword);
		assertEquals(offset, cipher.offset);
		assertEquals(inputStringClean, cipher.outputString);
		assertEquals(inputStringClean, output);
	}
}

//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/monosubstitution/BeaufortTest.java
//Mattrixwv
// Created: 02-23-22
//Modified: 04-19-24
package com.mattrixwv.cipherstream.monosubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;

import com.mattrixwv.cipherstream.exceptions.InvalidInputException;
import com.mattrixwv.cipherstream.exceptions.InvalidKeywordException;


@ExtendWith(MockitoExtension.class)
public class BeaufortTest{
	@InjectMocks
	private Beaufort cipher;
	@Mock
	private Logger logger;
	//Variables
	private static final String decodedString = "Message to^encode";
	private static final String decodedStringClean = "MESSAGETOENCODE";
	private static final String encodedString = "Yageolz rq^ujmdag";
	private static final String encodedStringClean = "YAGEOLZRQUJMDAG";
	private static final String keyword = "Ke*y word";
	private static final String keywordClean = "KEYWORD";


	@Test
	public void testConstructor_default(){
		cipher = new Beaufort();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertFalse(cipher.atbash.preserveCapitals);
		assertFalse(cipher.atbash.preserveWhitespace);
		assertFalse(cipher.atbash.preserveSymbols);
		assertFalse(cipher.caesar.preserveCapitals);
		assertFalse(cipher.caesar.preserveWhitespace);
		assertFalse(cipher.caesar.preserveSymbols);
		assertFalse(cipher.vigenere.preserveCapitals);
		assertFalse(cipher.vigenere.preserveWhitespace);
		assertFalse(cipher.vigenere.preserveSymbols);
	}

	@Test
	public void testConstructor_preservesCapitals(){
		cipher = new Beaufort(true, false, false);

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertTrue(cipher.atbash.preserveCapitals);
		assertFalse(cipher.atbash.preserveWhitespace);
		assertFalse(cipher.atbash.preserveSymbols);
		assertTrue(cipher.caesar.preserveCapitals);
		assertFalse(cipher.caesar.preserveWhitespace);
		assertFalse(cipher.caesar.preserveSymbols);
		assertTrue(cipher.vigenere.preserveCapitals);
		assertFalse(cipher.vigenere.preserveWhitespace);
		assertFalse(cipher.vigenere.preserveSymbols);
	}

	@Test
	public void testConstructor_preservesWhitespace(){
		cipher = new Beaufort(false, true, false);

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertFalse(cipher.atbash.preserveCapitals);
		assertTrue(cipher.atbash.preserveWhitespace);
		assertFalse(cipher.atbash.preserveSymbols);
		assertFalse(cipher.caesar.preserveCapitals);
		assertTrue(cipher.caesar.preserveWhitespace);
		assertFalse(cipher.caesar.preserveSymbols);
		assertFalse(cipher.vigenere.preserveCapitals);
		assertTrue(cipher.vigenere.preserveWhitespace);
		assertFalse(cipher.vigenere.preserveSymbols);
	}

	@Test
	public void testConstructor_preservesSymbols(){
		cipher = new Beaufort(false, false, true);

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertFalse(cipher.atbash.preserveCapitals);
		assertFalse(cipher.atbash.preserveWhitespace);
		assertTrue(cipher.atbash.preserveSymbols);
		assertFalse(cipher.caesar.preserveCapitals);
		assertFalse(cipher.caesar.preserveWhitespace);
		assertTrue(cipher.caesar.preserveSymbols);
		assertFalse(cipher.vigenere.preserveCapitals);
		assertFalse(cipher.vigenere.preserveWhitespace);
		assertTrue(cipher.vigenere.preserveSymbols);
	}

	@Test
	public void testSetInputString(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString, cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString);
	}

	@Test
	public void testSetInputString_noCapitals(){
		cipher.preserveCapitals = false;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.toUpperCase(), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.toUpperCase());
	}

	@Test
	public void testSetInputString_noWhitespace(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = false;
		cipher.preserveSymbols = true;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("\\s", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, times(1)).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("\\s", ""));
	}

	@Test
	public void testSetInputString_noSymbols(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = false;

		cipher.setInputString(decodedString);

		assertEquals(decodedString.replaceAll("[^a-zA-Z\\s]", ""), cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", decodedString);
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, times(1)).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", decodedString.replaceAll("[^a-zA-Z\\s]", ""));
	}

	@Test
	public void testSetInputString_blank(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString("");
		});

		assertEquals("", cipher.inputString);
		verify(logger, times(1)).debug("Original input string '{}'", "");
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, times(1)).debug("Cleaned input string '{}'", "");
	}

	@Test
	public void testSetInputString_null(){
		assertThrows(InvalidInputException.class, () -> {
			cipher.setInputString(null);
		});

		assertEquals("", cipher.inputString);
		verify(logger, never()).debug(eq("Original input string '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing whitespace");
		verify(logger, never()).debug("Removing symbols");
		verify(logger, never()).debug(eq("Cleaned input string '{}'"), anyString());
	}

	@Test
	public void testSetKeyword(){
		cipher.setKeyword(keyword);

		assertEquals(keywordClean, cipher.keyword);
		verify(logger, times(1)).debug("Original keyword '{}'", keyword);
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all non-letters");
		verify(logger, times(1)).debug("Cleaned keyword '{}'", keywordClean);
	}

	@Test
	public void testSetKeyword_blank(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword("");
		});

		assertEquals("", cipher.keyword);
		verify(logger, times(1)).debug("Original keyword '{}'", "");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all non-letters");
		verify(logger, times(1)).debug("Cleaned keyword '{}'", "");
	}

	@Test
	public void testSetKeyword_short(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword("A");
		});

		assertEquals("A", cipher.keyword);
		verify(logger, times(1)).debug("Original keyword '{}'", "A");
		verify(logger, times(1)).debug("Removing case");
		verify(logger, times(1)).debug("Removing all non-letters");
		verify(logger, times(1)).debug("Cleaned keyword '{}'", "A");
	}

	@Test
	public void testSetKeyword_null(){
		assertThrows(InvalidKeywordException.class, () -> {
			cipher.setKeyword(null);
		});

		assertEquals("", cipher.keyword);
		verify(logger, never()).debug(eq("Original keyword '{}'"), anyString());
		verify(logger, never()).debug("Removing case");
		verify(logger, never()).debug("Removing all non-letters");
		verify(logger, never()).debug(eq("Cleaned keyword '{}'"), anyString());
	}

	@Test
	public void testEncode(){
		cipher = new Beaufort(true, true, true);
		cipher.keyword = keywordClean;
		cipher.inputString = decodedString;

		cipher.encode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Encoding");
		verify(logger, times(1)).debug("Encoding with Atbash");
		verify(logger, times(1)).debug("Shifting all letters by 1");
		verify(logger, times(1)).debug("Encoding with Vigenere");
		verify(logger, times(1)).debug("Saving output string '{}'", encodedString);
	}

	@Test
	public void testDecode(){
		cipher = new Beaufort(true, true, true);
		cipher.keyword = keywordClean;
		cipher.inputString = decodedString;

		cipher.decode();

		assertEquals(encodedString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(1)).debug("Encoding with Atbash");
		verify(logger, times(1)).debug("Shifting all letters by 1");
		verify(logger, times(1)).debug("Encoding with Vigenere");
		verify(logger, times(1)).debug("Saving output string '{}'", encodedString);
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.keyword = keyword;

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(encodedString, cipher.getOutputString());
		assertEquals(keyword, cipher.getKeyword());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.keyword = keyword;

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
	}

	@Test
	public void testPracticalEncoding(){
		cipher = new Beaufort(true, true, true);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalEncoding_clean(){
		cipher = new Beaufort(false, false, false);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
	}

	@Test
	public void testPracticalDecoding(){
		cipher = new Beaufort(true, true, true);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testPracticalDecoding_clean(){
		cipher = new Beaufort(false, false, false);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedStringClean, cipher.inputString);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}
}

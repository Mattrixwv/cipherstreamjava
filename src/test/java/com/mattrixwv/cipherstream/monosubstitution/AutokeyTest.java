//CipherStreamJava/src/test/java/com/mattrixwv/cipherstream/monosubstitution/AutokeyTest.java
//Mattrixwv
// Created: 07-26-21
//Modified: 04-19-24
package com.mattrixwv.cipherstream.monosubstitution;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;


@ExtendWith(MockitoExtension.class)
public class AutokeyTest{
	@InjectMocks
	private Autokey cipher;
	@Mock(name = "com.mattrixwv.cipherstream.monosubstitution.Autokey")
	private Logger logger;
	//Variables
	private static final String decodedString = "MeSsage to^encode";
	private static final String decodedStringClean = "MESSAGETOENCODE";
	private static final String encodedString = "WiQooxh fs^wfcuhx";
	private static final String encodedStringClean = "WIQOOXHFSWFCUHX";
	private static final String keyword = "keyword";
	private static final ArrayList<Integer> offset = new ArrayList<>(List.of(10, 4, 24, 22, 14, 17, 3, 12, 4, 18, 18, 0, 6, 4, 19));


	@Test
	public void testConstructor_default(){
		cipher = new Autokey();

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(0, cipher.offset.size());
	}

	@Test
	public void testConstructor_preservesCapitals(){
		cipher = new Autokey(true, false, false);

		assertTrue(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(0, cipher.offset.size());
	}

	@Test
	public void testConstructor_preservesWhitespace(){
		cipher = new Autokey(false, true, false);

		assertFalse(cipher.preserveCapitals);
		assertTrue(cipher.preserveWhitespace);
		assertFalse(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(0, cipher.offset.size());
	}

	@Test
	public void testConstructor_preservesSymbols(){
		cipher = new Autokey(false, false, true);

		assertFalse(cipher.preserveCapitals);
		assertFalse(cipher.preserveWhitespace);
		assertTrue(cipher.preserveSymbols);
		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(0, cipher.offset.size());
	}

	@Test
	public void testEncodeSet(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.encodeSet(keyword, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals((keyword + decodedString.replaceAll("\\s", "").replaceAll("[^a-zA-Z\\s]", "").substring(0, 8)).toUpperCase(), cipher.keyword);
		assertEquals(offset, cipher.offset);
		verify(logger, times(1)).debug("Setting fields for encoding");
		verify(logger, times(1)).debug("Setting keyword");
		verify(logger, times(1)).debug("Adding input to keyword");
		verify(logger, times(1)).debug("Removing last letters in the keyword");
	}

	@Test
	public void testDecodeSet(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;

		cipher.decodeSet(keyword, decodedString);

		assertEquals(keyword.toUpperCase(), cipher.keyword);
		assertEquals(decodedString, cipher.inputString);
		verify(logger, times(1)).debug("Setting fields for decoding");
		verify(logger, times(1)).debug("Setting keyword");
		verify(logger, times(1)).debug("Setting input string");
	}

	@Test
	public void testDecode(){
		cipher.preserveCapitals = true;
		cipher.preserveWhitespace = true;
		cipher.preserveSymbols = true;
		cipher.decodeSet(keyword, encodedString);

		cipher.decode();

		assertEquals(decodedString, cipher.outputString);
		verify(logger, times(1)).debug("Decoding");
		verify(logger, times(2)).debug("Appending partial output to keyword");
		verify(logger, times(17)).debug(eq("Working character {}"), anyChar());
		verify(logger, times(2)).debug("Appending uppercase");
		verify(logger, times(1)).debug("Wrapping around to Z");
		verify(logger, times(13)).debug("Appending lowercase");
		verify(logger, times(3)).debug("Wrapping around to z");
		verify(logger, times(17)).debug(eq("Decoded letter {}"), anyChar());
		verify(logger, times(1)).debug("Saving output string '{}'", decodedString);
	}

	@Test
	public void testGetters(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.keyword = keyword;
		cipher.offset.add(1);

		assertEquals(decodedString, cipher.getInputString());
		assertEquals(encodedString, cipher.getOutputString());
		assertEquals(keyword, cipher.getKeyword());
		assertEquals(new ArrayList<>(List.of(1)), cipher.getOffsets());
	}

	@Test
	public void testReset(){
		cipher.inputString = decodedString;
		cipher.outputString = encodedString;
		cipher.keyword = keyword;
		cipher.offset.add(1);

		cipher.reset();

		assertEquals("", cipher.inputString);
		assertEquals("", cipher.outputString);
		assertEquals("", cipher.keyword);
		assertEquals(0, cipher.offset.size());
	}

	@Test
	public void testPracticalEncoding(){
		cipher = new Autokey(true, true, true);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedString, cipher.inputString);
		assertEquals((keyword + decodedStringClean.substring(0, 8)).toUpperCase(), cipher.keyword);
		assertEquals(encodedString, cipher.outputString);
		assertEquals(encodedString, output);
	}

	@Test
	public void testPracticalEncoding_clean(){
		cipher = new Autokey(false, false, false);

		String output = cipher.encode(keyword, decodedString);

		assertEquals(decodedStringClean, cipher.inputString);
		assertEquals((keyword + decodedStringClean.substring(0, 8)).toUpperCase(), cipher.keyword);
		assertEquals(encodedStringClean, cipher.outputString);
		assertEquals(encodedStringClean, output);
	}

	@Test
	public void testPracticalDecoding(){
		cipher = new Autokey(true, true, true);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedString, cipher.inputString);
		assertEquals((keyword + decodedStringClean.substring(0, 14)).toUpperCase(), cipher.keyword);
		assertEquals(decodedString, cipher.outputString);
		assertEquals(decodedString, output);
	}

	@Test
	public void testpracticalDecoding_clean(){
		cipher = new Autokey(false, false, false);

		String output = cipher.decode(keyword, encodedString);

		assertEquals(encodedStringClean, cipher.inputString);
		assertEquals((keyword + decodedStringClean.substring(0, 14)).toUpperCase(), cipher.keyword);
		assertEquals(decodedStringClean, cipher.outputString);
		assertEquals(decodedStringClean, output);
	}
}

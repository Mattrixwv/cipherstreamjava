# Cipher Stream Java
This is a Java port of the C++ library [Cipher Stream](https://bitbucket.org/Mattrixwv/CipherStream)


## Combination
TODO:

### ADFGX
TODO:

Example:
```
Square Keyword = SQUAREKEYWORD
Keyword = KEYWORD
Message = Message to encode
AAGAGADFAGAXXDAXDXADAFAFXDDGDF
```

### ADFGVX
TODO:

Example:
```
Square Keyword = SQUAREKEYWORD
Keyword = KEYWORD
Message = Message to encode
AXGVDAVFXGAGFAAFAGAAXDXFGDAGDA
```


## Mono-Substitution
These are simple ciphers where a single character is transformed into a single or multiple characters the same way each time it is encountered in the input. These are simple ciphers and easy to break.

### BaseX
This is not technically a cipher, it simply converts ASCII characters to their numeric values at a given base. The most common use for this is to transform text to binary, octal, or hexadecimal. Strictly speaking it does not require a key, however you do need to include the base that you wish to use.

Example:
```
Message to encode ->
1001101 1100101 1110011 1110011 1100001 1100111 1100101 100000 1110100 1101111 100000 1100101 1101110 1100011 1101111 1100100 1100101
```

### Baconian
The Baconian cipher is similar to Base2 encoding (binary) except that the alphabet starts at A = 0, instead of 0's and 1's it uses a's and b's, and I and J share an encoding space, as do U and V. It does not require a key.

Example:
```
Message to encode ->
ababb aabaa baaab baaab aaaaa aabba aabaa baaba abbab aabaa abbaa aaaba abbab aaabb aabaa
```

### Caesar
The Caesar cipher offsets the letters in the alphabet by a given amount. It does require a "key" of sorts in that you must set how far the message needs to be shifted.

Example:
```
offset = 3
Message to encode ->
Phvvdjh wr hqfrgh
```

### Atbash
The Atbash cipher reverses the alphabet for encryption. i.e. a = z, b = y, .... It does not require a key.

Example:
```
Message to enocde ->
Nvhhztv gl vmxlwv
```

### Vigenere
TODO: The Vigenere cipher shifts each letter in the input a varied amount based on the letters in the key, with 'a' shifting the letter 0 places, 'b' shifting the letter 1 place, etc. If the message is longer than the key you simply start back over at the beginning of the key. It does require a key word/phrase.

Example:
```
key = keyword
Message to encode ->
Wiqooxh ds cjqfgo
```

### Autokey
The Autokey cipher works in a similar way to the Vigenere cipher except that instead of reusing the key when you reach the end of it, you tack on the beginning of the message to the key. This way the key does not repeat, making it harder to crack. It does require a key word/phrase.

Example:
```
key = keyword
Message to encode ->
Wiqooxh fs wfcuhx
```


## Poly-Substitution
These ciphers are slightly more complex, encoding multiple letters at the same time making the cipher harder to crack. While many of these keep one letter from always being encoded as another single letter it often does so in such a way that if multiple letters are often seen together they will be encoded the same way. This is offset somewhat because they have to appear in the same location in the cipher text % the number of letters you are encoding by. i.e. If you are using a cipher that encodes with pairs the 'es' in 'mess' and 'these' would not encode to the same thing because 'me' and 'ss' are encoded in the first example while 'th', 'es', 'e_' are encoded in the second, unless there were an odd number of characters before one of these words, shifting one of the words by one place.

### Columnar
TODO:

Example:
```
key = keyword
Message to encode ->
Edxeoxm te acxgoxsnxsex
```

### Morse
This is technically not a cipher, at least any more than writing is encoded speach, as it is meant to enocde information for transmission in a different media. It has been around for a long time and used to be widely known and used, though in recent years it is mainly used by hobbyists. Using this code a letter or number is converted to 1 or more .'s and -'s (dots and dashes or dits and dahs). It does not require a key.

Example:
```
Message to encode ->
-- . ... ... .- --. . - --- . -. -.-. --- -.. .
```

### Playfair
TODO:

Example:
```
Message to encode ->
NKQZPCNDZKDULCGD
```

### PolybiusSquare
TODO:

Example:
```
Message to encode ->
41124545233212 5115 124225152212
```
